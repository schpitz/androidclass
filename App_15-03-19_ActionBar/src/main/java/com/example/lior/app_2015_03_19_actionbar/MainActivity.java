package com.example.lior.app_2015_03_19_actionbar;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
//import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends Activity {

    public static boolean copyState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // get the menu_main.xml and attach to the menu
        getMenuInflater().inflate(R.menu.menu_main , menu);

        if ( copyState)
            menu.add("paste");


        return true;
    }



    // When selecting an item -> View object -> popup menu -> menu_item.xml
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case (R.id.action_settings):
                Toast.makeText(getApplicationContext() , "Settings" , Toast.LENGTH_SHORT).show();

                copyState = true;
                invalidateOptionsMenu();
                break;

            case (R.id.item2):
                Toast.makeText(getApplicationContext() , "Menu Item :)" , Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }
}
