package com.example.hackeru.app_2015_02_26_settingsactivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        TextView tv = new TextView(this);
        SharedPreferences settingsPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        boolean wifi = settingsPrefs.getBoolean("wifi" , false);
        String name = settingsPrefs.getString("name" , "");
        tv.setText("Wifi: " + wifi + "\n + Name: " + name);

        tv.setTextSize(25);
        setContentView(tv);

    }


}
