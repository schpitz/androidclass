package com.example.hackeru.app_2015_02_26_settingsactivity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;


public class MainActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);


        addPreferencesFromResource(R.xml.settings);
        startActivity(new Intent(getApplicationContext() , SecondActivity.class));

    }


}
