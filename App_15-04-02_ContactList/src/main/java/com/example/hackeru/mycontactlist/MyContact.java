package com.example.hackeru.mycontactlist;

/**
 * Created by hackeru on 02/04/2015.
 */
public class MyContact {

    String name;                    // Contact name
    String[] phones;                // phone numbers (could be more than 1 number)
    String displayPhones;



    // Alternative version
    public  void addPhone(String phone){
        displayPhones += phone+"\n";
    }


    @Override
    public String toString()
    {

        if(displayPhones == null)
        {
            StringBuilder builder = new StringBuilder();

            //fill builder
            if(phones != null && phones.length>0){
                for (String phone : phones) {
                    builder.append(phone+"\n");
                }
            }

            displayPhones = builder.toString();
        }

        return name+"\n"+displayPhones;
    }
}
