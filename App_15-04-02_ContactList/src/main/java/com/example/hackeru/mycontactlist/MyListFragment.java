package com.example.hackeru.mycontactlist;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by hackeru on 02/04/2015.
 * This script will take the device's contact-list and present it as a list
 * (using SQLite database)
 */
public class MyListFragment extends ListFragment {

    ArrayList<MyContact> list = new ArrayList<>();                   // a list of ype MyContact
    ArrayAdapter<MyContact> adapter;                                 // it's still null for now...



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // initialize the adapter (we needed the context/activity
        adapter = new ArrayAdapter<MyContact>(getActivity(),
                                                android.R.layout.simple_list_item_1,
                                                list);
        setListAdapter(adapter);                                    // Set the list into the adapter
        readContact();                                              // Read from contact (external function)
    }



    // Function that reads the contact from the database
    private void readContact()
    {
        // New a-sync task, use another thread for reading the contacts
        new AsyncTask<Void, MyContact, Void>(){

            @Override
            protected Void doInBackground(Void... params)                       //background thread
            {
                // Resolver -> general object that can get info from different PROVIDERS (as URI/contacts/etc.)
                ContentResolver resolver = getActivity().getContentResolver();
                // it's a wrapper for the contacts
                Uri contactUri = ContactsContract.Contacts.CONTENT_URI;
                // get everything (null -> without filtering) into CURSOR object
                Cursor contactCursor = resolver.query(contactUri, null, null, null, null);

                if(contactCursor.moveToFirst())
                {
                    // check only the column of "Names"
                    int nameIndex = contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                    // get the ID's column
                    int idIndex = contactCursor.getColumnIndex(ContactsContract.Contacts._ID);
                    // get the index of where the phone number is
                    int hasPhoneIndex =  contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                    // the name of the column
                    String columnIdName = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;


                    do {
                        MyContact contact = new MyContact();                            // new contact
                        contact.name = contactCursor.getString(nameIndex);              //get name for row
                        boolean isPhone = contactCursor.getInt(hasPhoneIndex) > 0;      //add phones contact
                        // check if the contact has any phone number stored
                        if(isPhone)
                        {
                            String id = contactCursor.getString(idIndex);               // gets the id of the contact we want to check

                            Cursor phoneCursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, // access the inner class
                                                                null,
                                                                columnIdName+ " = ? ",//selection (send the selection without filtering/order)
                                                                new String[]{id}      //arguments
                                                                ,null);

                            if(phoneCursor.moveToFirst())
                            {
                                int columnNumberIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);      // get the index of columns
                                String[] phones = new String[phoneCursor.getCount()];                   // get the number of ROWS (=number of contacts)
                                int counter = 0;                                                        // internal counter (initialized to 0)
                                do{
                                    phones[counter] = phoneCursor.getString(columnNumberIndex);         // cover all rows in the phone list (using counter)
                                    counter++;                                                          // increase counter
                                } while (phoneCursor.moveToNext());
                                contact.phones = phones;
                            }
                            phoneCursor.close();
                        }

                        publishProgress(contact);
                    } while (contactCursor.moveToNext());
                }
                contactCursor.close();                                      // Close cursor connect to SQLite database
                return null;                                                //onPostExecute
            }


            @Override
            protected void onProgressUpdate(MyContact... values) {           // UI THREAD
//              super.onProgressUpdate(values);
                list.add(values[0]);                                         // add the current contact
                adapter.notifyDataSetChanged();                              // notify to refresh screen
            }


            @Override
            protected void onPostExecute(Void aVoid)
            {
//              super.onPostExecute(aVoid);
                Toast.makeText(getActivity().getApplicationContext(), "Finish", Toast.LENGTH_SHORT).show();
            }
        }.execute();

    }
}
