package com.example.app_2014_29_12_a;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	Random random = new Random();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		//New button object
		Button button1 = (Button) findViewById(R.id.button1);
		
		View myTextView = findViewById(R.id.textView1);
		button1.setTag(myTextView);
	}
	
	public void click(View v)
	{
		switch (v.getId())
		{
		case R.id.button1:
			changeCount(v);
			break;
		case R.id.button2:
			startActivity(new Intent(this , second_activity.class)); 	//Remember by heart!
			//finish();	//disable "back" button
			break;
		case R.id.button3:
			translateButton(v);
			break;
		default:
			break;
		}
	}
	
	
	//v -> button3 
	private void translateButton(View v)
	{
		View parentView = (View) v.getParent();	//linear layout around the button
		int layoutWidth = parentView.getWidth();
		int layoutHeight = parentView.getHeight();
		
		int vWidth = v.getWidth();		//button's width
		int vHeight = v.getHeight(); 	//button's height
		
		int randomX = random.nextInt(layoutWidth-vWidth); 	//get screen (view) width of the screen
		int randomY = random.nextInt(layoutHeight-vHeight);	//the same, but with height
		
		
		//translation button
		v.setX(randomX);
		v.setY(randomY);
	}
	
	
	int counter = 0;
	private void changeCount(View v)
	{
			TextView tv = (TextView) v.getTag();
			tv.setText("" + (++counter));
	}
}
