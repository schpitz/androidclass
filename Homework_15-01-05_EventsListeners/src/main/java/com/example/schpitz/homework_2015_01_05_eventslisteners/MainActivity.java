package com.example.schpitz.homework_2015_01_05_eventslisteners;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

    //New arraylist of countries
    ArrayList<Country> countries = new ArrayList<>();
    //ArrayAdapter of countries
    ArrayAdapter<Country> countryAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        set6countries();                //Initialize countries + cities

        ListView lv1 = (ListView) findViewById(R.id.listViewCountry);   //Creating a listview object (from XML)
        lv1.setOnItemClickListener(this);                               //add a listener to each item -> implement onItemClick() function

        // Initialize the adapter with context and layout
        countryAdapter = new ArrayAdapter<Country>(
                this,                                       //Context -> this activity
                android.R.layout.simple_list_item_1,        //Layout -> simple_list_item_1
                countries);                                 //Content -> countries ArrayList

        lv1.setAdapter(countryAdapter);         //Apply the listview with the adapter


    }



    //Set the countries (as external function
    private void set6countries()
    {
        //Creating countries (using their instructors of (country + cities))
        Country country1 = new Country("France","Paris" , "Baurdox" , "Lyon");
        Country country2 = new Country("Israel" , "Tel Aviv" , "Jerusalem", "Haifa");
        Country country3 = new Country("UK" , "London" , "Newcastle" , "Liverpool");
        Country country4 = new Country("US" , "Washington D.C." , "New York" , "Las Vegas");

        //Adding instructors to countries-arraylist
        countries.add(country1);
        countries.add(country2);
        countries.add(country3);
        countries.add(country4);

        //You can add as many countries as you like
        //I'm lazy...
    }



    // Must implements onItemClick() because of the listener (for each item in the arrayadapter)
    // Each click will do the following:
    // 1. Save the clicked object into a static variable (created on the SecondActivity.class)
    // 2. send the user to the 2nd activity screen
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ArrayAdapter<Country> countriesList = (ArrayAdapter<Country>) parent.getAdapter();  //get the adapter from parent
        SecondActivity.countryX = countriesList.getItem(position);          //get the country from the selected position

        //Load the next activity
        startActivity(new Intent(this , SecondActivity.class));
    }
}
