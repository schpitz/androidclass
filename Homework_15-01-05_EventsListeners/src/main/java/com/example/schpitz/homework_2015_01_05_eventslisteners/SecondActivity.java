package com.example.schpitz.homework_2015_01_05_eventslisteners;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class SecondActivity extends Activity {

    public static Country countryX;                 //a new static Country object, shared to all classes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        ListView CitylistView = (ListView) findViewById(R.id.listViewCities);       //set a new ListView

        // countryX is a Country object.
        //Each "countryX" has "countryName" string + "cities" arraylist (contain 3 strings of cities)
        //Because "cities" is an arraylist, we can send it into the adapter (without more actions)
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(
                this,                                                       //Context -> This ListView
                android.R.layout.simple_list_item_1,                        //Layout
                countryX.cities                                             //Content -> arraylist
        );
        CitylistView.setAdapter(cityAdapter);                               //Apply the adapter on ListView

    }

}
