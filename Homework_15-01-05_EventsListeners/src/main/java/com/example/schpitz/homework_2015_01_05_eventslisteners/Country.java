package com.example.schpitz.homework_2015_01_05_eventslisteners;

import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Schpitz on 20/01/2015.
 */
public class Country {

    //Each COUNTRY object will have the name of the country + 3 cities (
    String countryName;
    ArrayList<String> cities = new ArrayList<>();


    //Constructors that gets: Name of country + 3*cities
    public Country(String countryName, String city1, String city2, String city3) {
        this.countryName = countryName;
        this.cities.add(city1);
        this.cities.add(city2);
        this.cities.add(city3);
    }


    @Override
    public String toString() {
        return countryName;
    }
}


