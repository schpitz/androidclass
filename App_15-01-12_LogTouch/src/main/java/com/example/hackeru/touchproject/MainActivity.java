package com.example.hackeru.touchproject;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    // Use Ctrl+O
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //Check if the action is ACTION_Down, and just then print the X
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            Log.d("on touch", " x = " + event.getX());
        }
        return super.onTouchEvent(event);
    }
}
