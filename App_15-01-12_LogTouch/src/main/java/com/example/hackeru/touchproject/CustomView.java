package com.example.hackeru.touchproject;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by hackeru on 12/01/2015.
 */
public class CustomView extends View{



    //4 Different constructors for View (called CustomView)
    public CustomView(Context context) {
        super(context);
        init();
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    // Runs only on Lollipop android version
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    //Function init()
    private void init()
    {
        setBackgroundColor(Color.RED);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        Log.d("on touch" , " View y = " + event.getY());
        return true;
        //Return false - the father activity will handle it (from the down and later)
        //Return true - this activity/view will handle anything after the DOWN EVENT
    }


    //Draw a
    @Override
    protected void onDraw(Canvas canvas)
    {
        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setStrokeWidth(3.0f);
        //Now it's implemented on the canvas (draw a line)
        canvas.drawLine(10,10,100,100,p);

        RectF rect = new RectF();
        rect.set(100,100,200,200);  //it's still not connected to the CANVAS, it's just a new object
        //using rect.set() is exactly as using
        //rect.left = 100;
        //rect.top = 100;
        //...

        p.setColor(Color.BLUE);
        canvas.drawRect(rect,p);    //draw rect on the canvas

        canvas.drawCircle(canvas.getHeight()/2 , canvas.getWidth()/2,100,p);


    }
}
