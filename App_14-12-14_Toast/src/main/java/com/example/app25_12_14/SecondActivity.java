package com.example.app25_12_14;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		//if i write new  setContentView the previous one is not relevant anymore
		setContentView(R.layout.layout_margin);
	}
}
