package com.example.app25_12_14;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText name, lastName;
	TextView output;

	
	//The functions job is to connect between the xml to the code. 
	//onCreate - we ran over this function.we chance the basic function so it will fit our needs. 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);// onCcreate is a basic function in android - like main
		setContentView(R.layout.activity_main);
		//setContentView- is asistem function of android. 
		// activity_main- is the activity name 
		name = (EditText) findViewById(R.id.editText1); // We write in parenthesis the type of the variable.we use casting to make sure we receive the right info.
		lastName = (EditText) findViewById(R.id.editText2);
		output = (TextView)findViewById(R.id.textView1);
	}
	
	public void click(View v){
		if(v.getId()==R.id.button1){
			String sName = name.getText().toString();
			String lastName = this.lastName.getText().toString();
			
			//this gives a system notification incase one of the names are empty
			if(sName.equals("")||lastName.equals("")){
				Toast.makeText(this, "name and last must be implemented", Toast.LENGTH_LONG).show();//activity inherit from context
				return;
			}
			//print name and last
			//output.setText("Hello "+sName +" "+lastName);
			output.setText(R.string.title_activity_second);
			
			//to start new activity
			startActivity(new Intent(this, SecondActivity.class));
			// to finish the previous activity 
			finish();
		}
	}
}
