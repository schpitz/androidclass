package com.example.hackeru.mymonitor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by hackeru on 19/02/2015.
 */
public class MonitorView extends View {

    ArrayList<Integer> points = new ArrayList<>();
    Random random  = new Random();
    Paint paint = new Paint();

    public MonitorView(Context context) {
        super(context);

        new Thread(){
            @Override
            public void run() {//SECOND THREAD
                while (true){
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    int ran = getHeight() - random.nextInt(300);
                    points.add(ran);
                    postInvalidate();//UPDATE VIEW ON UI THREAD

                }
            }
        }.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(points.size()>1){
            for (int i = 1; i < points.size(); i++) {
                canvas.drawLine((i-1)*25, points.get(i-1), i*25, points.get(i), paint);

            }
        }
    }
}
