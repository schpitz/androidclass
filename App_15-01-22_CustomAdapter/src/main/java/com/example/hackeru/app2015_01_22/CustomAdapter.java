package com.example.hackeru.app2015_01_22;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hackeru on 22/01/2015.
 */
public class CustomAdapter extends ArrayAdapter{

    ArrayList<String> arrayList;
    Context context;


    //The default constructor uses List -> we change it to Arraylis

    public CustomAdapter(MainActivity context,  ArrayList<String> arrayList) {
        super(context, 0);                  //resource -> how each row will look like
        this.arrayList = arrayList;
        this.context = context;
    }


    //The adapter doesn't know how many rows the ArrayList has
    @Override
    public int getCount() {
        return arrayList.size();
    }


    //Until now we presented the same type of rows
    //This function will deal with how many types of rows we have
    @Override
    public int getViewTypeCount() {
        return 2;                   // 2 types of rows
    }

    //Each 5th position return the a different type (row2)
    @Override
    public int getItemViewType(int position) {
        //Short-IF
        return position % 5 == 0 ? 0 : 1;

        //The same as the following:
        //if (position % 5 == 0){
        //      return 0;                   //LinearLayout type
        //else
        //      return 1;                   //Textview type
    }

    //Manual change of GetView() ---> uses GetViewCount()
    //If there's no new cell, we create one
    //If we can recycle, we just change its content (into an existing context)
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        if (convertView == null)        //there's no view to return
        {
            //Create a new layout inflater which deals layouts (using OS services)
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);    //uses system services
            convertView = inflater.inflate(R.layout.row , null);    //if view=null
        }

        if (position % 5 == 0)
        {
            convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.row2 , null);
        } else {
        //Only if there's no NULL or 5th
            TextView tv = (TextView) convertView.findViewById(R.id.textView);   //Connect Textview object
            tv.setText(arrayList.get(position));
        }

        return convertView;                 //Return back to the view
    }
}
