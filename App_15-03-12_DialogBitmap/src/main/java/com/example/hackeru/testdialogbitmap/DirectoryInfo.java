package com.example.hackeru.testdialogbitmap;

import android.graphics.Bitmap;

/**
 * Created by hackeru on 12/03/2015.
 */
public class DirectoryInfo {

    private  boolean isEmpty;
    Bitmap icon;

    public DirectoryInfo() {
        if(isEmpty){
            icon = MainActivity.emptyFolderIcon;
        }else
            icon = MainActivity.folderIcon;
    }
}
