package com.example.hackeru.app2015_01_22_gestures;

import android.content.Context;
import android.os.Handler;
import android.view.ScaleGestureDetector;


//ScaleGestureDetector class -> for gestures
public class MyClass extends ScaleGestureDetector implements ScaleGestureDetector.OnScaleGestureListener {

    //Constructors!!!
    public MyClass(Context context, OnScaleGestureListener listener) {
        super(context, listener);
    }

    public MyClass(Context context, OnScaleGestureListener listener, Handler handler) {
        super(context, listener, handler);
    }


    //Implementation of "OnScaleGestureListener"
    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        return false;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }
}
