package com.example.app_2015_01_05_2;

import java.util.ArrayList;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class MyListener implements OnItemClickListener{

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		//
		ArrayAdapter<String> mainActivity_ListViewAdapter = (ArrayAdapter<String>) parent.getAdapter();
		
		String removed = mainActivity_ListViewAdapter.getItem(position);	//Get the item position
		mainActivity_ListViewAdapter.remove(removed);			//remove current string from adapterlist
		mainActivity_ListViewAdapter.notifyDataSetChanged();	//refresh list view data
		
		//test - remove by position instead of by string
		ArrayList<String> names = (ArrayList<String>) parent.getTag();
		names.remove(position);
		
	}

}
