package com.example.app_2015_01_05_2;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener {

	//Create new ArrayList & ArrayAdapter
	ArrayList<String> names = new ArrayList<>();
	ArrayAdapter<String> listViewAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		fillNamesList();	//fill the ArrayList
		Button b = (Button) findViewById(R.id.button1);		//create button from XML
		b.setOnClickListener(this);			//add listener
		
		ListView listView = (ListView) findViewById(R.id.listView1);	//create TextView from XML
		listView.setOnItemClickListener(new MyListener());		//add MyListeren object (which implements OnItemClickListener INTERFACE)
		
		//Set the listViewAdapter object as list of Names
		listViewAdapter = new ArrayAdapter<>(this,
											android.R.layout.simple_list_item_1,
											names);
		//Execute the adapter (aka listViewAdapter)
		listView.setAdapter(listViewAdapter);
		

	}

	
	//Function the adds names to the arraylist
	private void fillNamesList() {
		for (int i=1; i<12; i++)
		{
			names.add("Person " + i);	//String of name and its number as one string
		}		
	}



	//Implementation of INTERFACE functions
	@Override
	public void onClick(View v) 
	{
		//Add a manual new person into the arraylist
		names.add(2, "Person X " + names.size());
		//We cant list to the listView because it doesn't have a listener
		listViewAdapter.notifyDataSetChanged();	//refresh list-view data
	}
}
