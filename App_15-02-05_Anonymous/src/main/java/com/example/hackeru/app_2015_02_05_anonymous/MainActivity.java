package com.example.hackeru.app_2015_02_05_anonymous;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final int i=10;             //test with a final variable (final only here!)

        Button button1 = new Button(getApplicationContext());
        setContentView(button1);
        button1.setText("Click Me!");

        //Add click-lisetener and activate another function
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMyDialog();
            }
        });

    }

    //Create the anonymous function at the mainActivity
    private void showMyDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Title").create();
        //We created 2 objects (with no identifier):
        //1. AlertDialog
        //2. Builder
        //the "dialog" object has what was returned from ".create()"

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                     @Override
                     public void onDismiss(DialogInterface dialog) {
                         Toast.makeText(MainActivity.this , "On Dismis!" , Toast.LENGTH_SHORT).show();

                     }
        });
        dialog.show();
    }


    //even if "i" is final in another function, it's not outisde
    private void test(int i) {
        i++;
    }


}
