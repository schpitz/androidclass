package com.example.hackeru.app_2015_03_02_datastorage;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.IOException;


public class MainActivity extends Activity {

    EditText name;

    // Summary of functions
    // getFilesDir() -> root internal storage
    // Environment.getExternalStorageDirectory() -> root external storage folder
    // getExternalFilesDir(String) -> external data storage fo


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set name from Editext
        name = (EditText) findViewById(R.id.editText);
    }

    // Check if we have write permission (Mounted is read+write)
    private boolean isWrite()
    {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    // Check if we have a read permission
    private boolean isRead()
    {
        String state = Environment.getExternalStorageState();
        return (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY) || isWrite());
    }





//    public void addFile(View view)
//    {
//        addExternalFile();
//    }

    // Add file in external storage
    // <root> sdcard/name.type
    private void addExternalFile()
    {
        File externalRoot = Environment.getExternalStorageDirectory();              // Environment = access to the main directory
        File newFile = new File(externalRoot , name.getText().toString());

        if (!newFile.exists())
            try{
                newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }


    // add folder in external storage
    private void addExternalFolder()
    {
        File externalRoot = Environment.getExternalStorageDirectory();
        File newFolder = new File (externalRoot , name.getText().toString());

        if (!newFolder.exists())
            newFolder.mkdir();                  // create a new directory (dir)
    }





    public void addFolder (View view) {
        if (isWrite())
            addExternalFolder();

        addExternalDataFolder();
    }

    public void addFile (View view) {
        if (isWrite())
            addExternalFile();
        addExternalDataPictureFile();           // also write a png into the external directory
    }






    // Write data into the external SD-Card
    private void addExternalDataFolder()
    {
        File externalDataRoot = getExternalFilesDir(null);          // get the address in the memory of the device
        File newFolder = new File (externalDataRoot , name.getText().toString());

        if (!newFolder.exists())
            newFolder.mkdir();
    }


    // Add picture to a folder on the SD card
    // <root>/storage/sdcard/Android/data/<Package Name>/files
    private void addExternalDataPictureFile()
    {
        File externalDataRoot = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File picture = new File(externalDataRoot , name.getText().toString() + ".png");

        if (!picture.exists())
            try {
                picture.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}
