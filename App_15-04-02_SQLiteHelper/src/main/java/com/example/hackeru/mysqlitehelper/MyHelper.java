package com.example.hackeru.mysqlitehelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by hackeru on 02/04/2015.
 */
public class MyHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "file_name.sqlite";

    public MyHelper(Context context, int version) {
        super(context, DB_NAME, null, version);
        Log.d("helper", "Constructor");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("helper", "onCreate");
        db.execSQL("CREATE TABLE contact_table (" +
                "name VARCHAR," +
                " last VARCHAR," +
                " created INTEGER," +
                " phone VARCHAR)");
        db.execSQL("INSERT INTO contact_table VALUES ('Izik', 'Algrisi', 1, '0548197615')");

        db.execSQL("CREATE TABLE phone_table (contact_id INTEGER, phone VARCHAR)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("helper", "onUpgrade");
        if(oldVersion == 1)
            db.execSQL("CREATE TABLE phone_table (contact_id INTEGER, phone VARCHAR)");
        if(oldVersion<2){

        }
    }

}
