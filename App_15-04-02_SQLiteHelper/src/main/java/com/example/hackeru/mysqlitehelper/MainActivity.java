package com.example.hackeru.mysqlitehelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            rootView.findViewById(R.id.button).setOnClickListener(this);
            rootView.findViewById(R.id.button2).setOnClickListener(this);
            return rootView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button:
                    EditText name = (EditText) getView().findViewById(R.id.editText);
                    EditText last = (EditText) getView().findViewById(R.id.editText2);

                    ContentValues values = new ContentValues();
                    values.put("created", System.currentTimeMillis());
                    values.put("name", name.getText().toString());
                    values.put("last", last.getText().toString());

                    SQLiteDatabase database = new MyHelper(getActivity().getApplicationContext(),
                                                getResources().getInteger(R.integer.db_contact_version))
                                                   .getWritableDatabase();
                    long row = database.insert("contact_table", null, values);
                    if(row == -1)
                        Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT)
                            .show();
                    database.close();

                    break;
                case R.id.button2:

                    SQLiteDatabase database1 = new MyHelper(getActivity(),
                            getResources().getInteger(R.integer.db_contact_version))
                                .getWritableDatabase();
                    Cursor cursor = database1.rawQuery("SELECT * FROM contact_table", null);
                    if(cursor.moveToFirst()){
                        do{

                            String name1 = cursor.getString(cursor.getColumnIndex("name"));
                            String last1 = cursor.getString(cursor.getColumnIndex("last"));
                            Log.d("db contact", "name: "+ name1+", last: "+last1);


                        }while (cursor.moveToNext());
                    }
                    cursor.close();
                    database1.close();

            }
        }
    }
}
