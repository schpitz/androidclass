package com.example.hackeru.fileexplorer;

import android.graphics.Bitmap;
import android.os.Environment;
import java.io.File;

/**
 * Created by hackeru on 12/03/2015.
 */
public class FileInfo extends File {

    private Bitmap icon;
    protected   DirectoryInfo parent;
    protected  static  DirectoryInfo rootDirectory = new DirectoryInfo(Environment.getExternalStorageDirectory(),null);





    public FileInfo(File file) {
        this(file.getPath());
        parent = new DirectoryInfo(file.getParentFile());
        if(isDirectory())
            throw new IllegalArgumentException();
    }

    public FileInfo(File file, DirectoryInfo parent) {
        this(file.getPath());
        this.parent = parent;
        if(isDirectory())
            throw new IllegalArgumentException();
    }


    public FileInfo(String path) {
        super(path);
        parent = new DirectoryInfo(getParentFile());
        if(isDirectory())
            throw new IllegalArgumentException();
    }

    protected FileInfo(File file, DirectoryInfo parent, boolean dir) {
        this(file.getPath(), true);
        this.parent = parent;
    }


    protected FileInfo(String dirPath, boolean directory) {
        super(dirPath);
    }
    protected FileInfo(File file, boolean directory) {
        super(file.getPath());
    }

    public Bitmap getIcon() {
        return icon;
    }

    public  DirectoryInfo getParentDirectory() {
        return parent;
    }

    @Override
    public String toString() {
        return  getName();
    }
}
