package com.example.hackeru.fileexplorer;

import android.app.ListActivity;
import android.os.Environment;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;


public class MainActivity extends ListActivity {


    DirectoryInfo current;
    ArrayAdapter<FileInfo> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File file = Environment.getExternalStorageDirectory();

        if(savedInstanceState != null)
            current = new DirectoryInfo(savedInstanceState.getString("path"));
        else
            current = new DirectoryInfo(file);
        adapter = new ArrayAdapter<FileInfo>(this,
                    android.R.layout.simple_list_item_1,
                    current.getInnerFiles());//File Info Array

        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if(adapter.getItem(position) instanceof  DirectoryInfo){
            current = (DirectoryInfo) adapter.getItem(position);
            adapter = new ArrayAdapter<FileInfo>(this,
                    android.R.layout.simple_list_item_1,
                    current.getInnerFiles());

            setListAdapter(adapter);
        }
    }



    @Override
    public void onBackPressed() {
        if(current.getParentDirectory() == null)
            super.onBackPressed();
        else {
            current = current.getParentDirectory();
            adapter = new ArrayAdapter<FileInfo>(this,
                    android.R.layout.simple_list_item_1,
                    current.getInnerFiles());

            setListAdapter(adapter);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("path", current.getPath());
    }
}
