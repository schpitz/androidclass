package com.example.hackeru.fileexplorer;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hackeru on 12/03/2015.
 */
public class DirectoryInfo extends FileInfo {

    public DirectoryInfo(File file, DirectoryInfo parent) {
        super(file, parent, true);
    }

    public DirectoryInfo(String dirPath) {
        super(dirPath, true);
        if(! getPath().equals(rootDirectory.getPath()))
            parent = new DirectoryInfo(getParentFile());
    }

    public DirectoryInfo(File file) {
        super(file, true);
        if(! file.getPath().equals(rootDirectory.getPath()))
            parent = new DirectoryInfo(getParentFile());
    }




    public ArrayList<FileInfo> getInnerFiles(){
        ArrayList<FileInfo> list = new ArrayList<>();

        for (File file : listFiles()) {
            if(file.isDirectory())
                list.add(new DirectoryInfo(file, this));
            else
                list.add(new FileInfo(file, this));
        }
        return  list;
    }

    public ArrayList<DirectoryInfo> parents(){
        ArrayList<DirectoryInfo> list = new ArrayList<>();
        DirectoryInfo current = parent;
        list.add(current);
        while (parent.getParentDirectory() != null){
            list.add( current = parent.getParentDirectory());

        }
        return  list;
    }
}
