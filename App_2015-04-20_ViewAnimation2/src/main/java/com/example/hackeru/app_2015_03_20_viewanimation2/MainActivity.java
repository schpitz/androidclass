package com.example.hackeru.app_2015_03_20_viewanimation2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;


public class MainActivity extends Activity {

    /*
        With 'Animation' we only moving the view display,
        but not the object itself.
        This is a serious downside (con) and not a perfect way to apply animation
    */



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Animation
        // Apply animation on object from View
        Animation anim = AnimationUtils.loadAnimation(
                getApplicationContext(),
                R.anim.swing);
        findViewById(R.id.imageView).startAnimation(anim);


        // Use a new thread for loading a new activity with animation
        // Using an animation for moving out, and another animation for moving in activity
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), SecondActivity.class));
                overridePendingTransition(R.anim.in , R.anim.out);              // use 'in' and 'out' xml animation (could also use build-in animation of android)
            }
        }, 5000);

    }




    // Animations
    public void move(final View view)
    {
        // We can set the animation via code (and not XML)
        // Animation Set -> a plate (set) of different animation, that will be played together
        AnimationSet set = new AnimationSet(false);      // each animation has its own interpulator

        // 1st Animation - transform (translate position)
        TranslateAnimation animation = new TranslateAnimation(0 , 500 , 0 , 0); // fromX, toX, fromX, toY
        animation.setDuration(2000);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setInterpolator(new LinearInterpolator());

        //animation.setFillAfter(true);
        //view.startAnimation(animation);             // apply to view-object -> only at the end

        // 2nd Animation - Scale
        ScaleAnimation scaleAnim = new ScaleAnimation(1,0,1,0);
        scaleAnim.setDuration(5000);
        scaleAnim.setInterpolator(new BounceInterpolator());
        scaleAnim.setRepeatCount(Animation.INFINITE);
        scaleAnim.setRepeatMode(Animation.REVERSE);



        // Set Listener
        // Adds some inner functions (must have -> interface)
        // Could notice when the animation start/end/repeat itself, and add code to it
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setX(view.getX()+100);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // Combine both animations -> into 'set' variable
        set.addAnimation(animation);
        set.addAnimation(scaleAnim);

        // Play main animation
        view.startAnimation(set);
    }
}
