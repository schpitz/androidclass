package com.example.app_2014_12_25_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	EditText name, lastName;
	TextView output;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		name = (EditText) findViewById(R.id.editText1);
		lastName = (EditText) findViewById(R.id.editText2);
		output = (TextView) findViewById(R.id.textView1);
	}
	
	public void click(View v)
	{
		if (v.getId() == R.id.button1)
		{
			String sName = this.name.getText().toString();
			String lastName = this.lastName.getText().toString();
			
			if (sName.equals("") || lastName.equals("")){
				Toast.makeText(this, "Name & last Name must be implemented", Toast.LENGTH_LONG).show(); //activity inherits from ...
				return;
			}
			
			//print name and last name
			output.setText("Hello " + sName + " " + lastName);
			
			
			//startActivity() -> Load the next activity (script+layout)
			startActivity(new Intent(this , SecondActivity.class));
			//Kill the current screen (no "Back" button available)
			finish();	//kill the 1st screen
		}
	}
}
