package com.example.hackeru.app_2015_02_09;

/**
 * Created by hackeru on 09/02/2015.
 */
public class MyDate {

    int day,month,year;     //3 integers


    //Getters & Setters
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) throws  MonthExceptions{
        if (month>0 && month<13)
            this.month = month;
        else
            throw new MonthExceptions(month);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
