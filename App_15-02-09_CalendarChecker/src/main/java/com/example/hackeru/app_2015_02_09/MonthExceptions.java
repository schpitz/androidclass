package com.example.hackeru.app_2015_02_09;

/**
 * Created by hackeru on 09/02/2015.
 */
public class MonthExceptions extends Exception {
    //this class will use "Exception" class


    public MonthExceptions (int badMonth)
    {
        super(badMonth + ", invalid month");
    }
}
