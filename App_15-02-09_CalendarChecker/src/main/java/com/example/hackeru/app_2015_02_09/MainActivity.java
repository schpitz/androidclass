package com.example.hackeru.app_2015_02_09;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            // will cause failed in builds -> don't use context
            new AlertDialog.Builder(getApplicationContext()).setTitle("New title :)").create().show();

        } catch (Exception e) {
            //will print the type of the class of the exception
            Toast.makeText(getApplicationContext() , "It crashed... "+e.getClass().getName() , Toast.LENGTH_LONG).show();
        }



        //using "this" instead of "getApplicationContext" will make it work
        //new AlertDialog.Builder(this).setTitle("New title :)").create().show();
    }



    ////////////////////////////////////
    ///////// New stuff/////////////////
    ////////////////////////////////////

    ArrayList<MyDate> dates = new ArrayList<>();        //Array of dates using the MyDate class

    public void addDate(View view) {
        //Find the edit-text from XML
        EditText editText = (EditText) findViewById(R.id.editText);
        
        createDate(editText.getText().toString());      //call another functions
    }


    private void createDate(String s) {
        String[] date = s.split("/");
        MyDate myDateX = new MyDate();      //new Date

        //Now we will suggest us to use try/catch
        try {
            myDateX.setMonth(Integer.parseInt(date[1]));    //set the months value (parse from string to int)
            dates.add(myDateX);                 //add to the arraylist
            Toast.makeText(getApplicationContext() , "success, array size is: "+dates.size() , Toast.LENGTH_LONG).show();
        } catch (MonthExceptions e) {
            e.printStackTrace();            //show all tracks
        }



    }
}
