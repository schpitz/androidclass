package classes.statics;

import java.util.ArrayList;
import java.util.Vector;

public class MyClass {
	static int shared;
	Class test = MyClass.class;
	
	public static void main(String[] args) {
		MyClass c1 = new MyClass();
		MyClass c2 = new MyClass();
		
		System.out.println(c1 == c2);//false
		System.out.println(c1.getClass() == c2.getClass()); //true
		System.out.println(c1.getClass() == MyClass.class);//true
		
		c1.shared = 0 ;
		c2.shared = 2;
		MyClass.shared = 4;
		ArrayList<String> list = new ArrayList<>();
		list.add("");
		Vector<String> vec = new Vector<>();
		vec.add("");
	}
	
	static synchronized void test1(){
		
	}
	
	static synchronized void test2(){
		
	}


}
