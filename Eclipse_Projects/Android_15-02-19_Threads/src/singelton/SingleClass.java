package singelton;

public class SingleClass {
	
	private SingleClass() {
		// TODO Auto-generated constructor stub
	}
	
	private static SingleClass instance;//null
	
	public synchronized static SingleClass getInstance(){
		
		if(instance == null)
			instance = new SingleClass();
		return instance;
	}

}
