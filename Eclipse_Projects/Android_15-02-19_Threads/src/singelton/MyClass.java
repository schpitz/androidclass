package singelton;

public class MyClass {
	
	static 	 SingleClass s1, s2;

	public static void main(String[] args) {
		
		Thread t = new Thread(){
			public void run() {
				s2 = SingleClass.getInstance();
			};
		};//.start();
		t.start();
		s1 = SingleClass.getInstance();

	}

}
