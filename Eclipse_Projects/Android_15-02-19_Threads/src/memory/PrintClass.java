package memory;

public class PrintClass {

	int counter = 0;
	Object lock = new Object();

	public void print(){
		System.out.println(Thread.currentThread().getName()+" start print function");
		
		synchronized (this) {
			for (; this.counter < 1000; this.counter++) {
				String name = Thread.currentThread().getName();
				System.out.println(name+" , counter = "+counter);
			}

		}		
		System.out.println(Thread.currentThread().getName()+" start print function");

	}

	public void print2(){
		System.out.println(Thread.currentThread().getName()+" start print2 function");
		
		synchronized (this) {
			for (; this.counter < 1000; this.counter++) {
				String name = Thread.currentThread().getName();
				System.out.println(name+" , counter = "+counter);
			}
			System.out.println("Thread 2 inside synchronized");

		}		
		System.out.println(Thread.currentThread().getName()+" start print2 function");

	}

	
	public synchronized void syncPrint(){
		synchronized (this) {
			for (int i = 0; i < 1000; i++) {
				
			}
		}
	}
	
	public static synchronized void testStatic(){
		
		Class mClass = PrintClass.class;
		synchronized (mClass) {
			
		}
	}
	
	
	
	
	
	
	
	
	
	public void lockPrint(Object lock){
		System.out.println("start lockPrint");
		while(true){
			System.out.println("start eteration");
			synchronized(this){
				if(counter<1000){
					String name = Thread.currentThread().getName();
					System.out.println(name+" , counter = "+counter);
					counter++;
				}	
			}
			if(counter>999)
				return;

		}

	}

}
