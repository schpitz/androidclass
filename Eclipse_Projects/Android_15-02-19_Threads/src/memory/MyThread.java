package memory;

public class MyThread extends Thread{

	PrintClass pClass;
	
	
	public MyThread(PrintClass pClass) {
		this.pClass = pClass;
	}


	@Override
	public void run() {
		pClass.lockPrint("MyString");
	}
}
