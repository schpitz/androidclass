package hackeru.arrays;

import java.util.Arrays;

public class arrays {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Different ways to create ARRAY
		//1st way
		//when we know length, but now values
		int[] intArray = new int[5];			//new integer array
		String[] stringArray = new String[5];	//new string array
		
		
		//2nd way
		//when we know the values we need
		int[] intArray2 = {1,8,3,7};			//new integer array
		//syso(intArray2[0]) -> 1
		intArray[0] = 7;
		//syso(intArray2[0]) -> 7;
		String[] stringArray2 = {"a", "2", intArray2[1]+""};		
		//syso(stringArray[3]) -> intArray2[1]+"" -> "8"; 

		
		//3rd way
		//When we don't know either length nor values
		int[] intArray3;
		intArray3 = new int[]{1,8,10,2};
		intArray2 = new int[]{2,9};		//will replace the previous intArray2 (old arrays+values)
		String[] strArray3;
		strArray3 = new String[]{"a","b","8"};
		
		
		//Call the function
		int[] arrayAnswer = getArray(95);
		System.out.println(Arrays.toString(arrayAnswer));
	}
	
	
	
	//Return int array from the character of the number
	static int[] getArray(int i){

		//Create an empty array
		int[] arr = null;		// int[] arr;
		
		//check the size of i 
		if(i>99)					// i>=100 					
		{							//3 digits
			arr = new int[3];		
			arr[0] = i/100;			//get hundreds
			arr[1] = (i/10)%10;		//get tens
			arr[2] = i%10;			//get ones
		} else if (i>9) {			// 10<=i<100
			arr = new int[2];		//2 digits
			arr[0] = i/10;
			arr[1] = i%10;
		} else {					//	i<10
			arr = new int[1];		//1 digit
			arr[0] = i;
		}
		
		return arr;				//must return something!
	}

}
