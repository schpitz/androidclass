package hackeru.arrayList;
import java.util.ArrayList;

public class ArrayLists {


	static ArrayList<String> names = new ArrayList<>();

	public static void main(String[] args) {

		int size = names.size();		//just like array.length with normal arrays;
		System.out.println(names.size());	//print size of array
		
		//Add values
		names.add("Itzik");				//adds a value to the array
		names.add("Avi");
		//names -> ("Avi","Itzik")
		
		//Print values
		String itzik = names.get(0);	//get integer for index, return a string
		System.out.println("Itzik");
		System.out.println(names.get(1));
		
		//Remove Values
		String str = names.remove(0);	//remove cell with index 0
		//System.out.println(names.remove(0) + "removed");		//-> "Itzik removed"
		
		//Find value
		int findValue = names.indexOf("Itzik");	//-1 is "not found"
		
	}

}
