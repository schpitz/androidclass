package ArrayEX;
import java.util.ArrayList;
import java.util.Scanner;

public class ArrayEx {
	
	static ArrayList<String> firstNames = new ArrayList<>();
	static ArrayList<String> lastNames = new ArrayList<>();
	static String[] lastPerson = {"",""};	//empty 2 cells array (not null)
	static Scanner scan = new Scanner(System.in);	//enable input
	
	
	
	public static void main(String[] args) {
		
		boolean keepLooping = true;
		boolean isDeleted = false;
		
		while (keepLooping)
		{
			System.out.println("Welcome!");
			System.out.println("Please choose how to contiune");
			System.out.println("a - to add person");
			System.out.println("d - to delete");
			System.out.println("p - to print all persons");
			if (isDeleted)
			{
				System.out.println("sd - show last perseon to delete");
				System.out.println("u - to return person from delete");
			}
			System.out.println("q - to quit");

			
			//Enter next action
			String x = scan.nextLine();	
			
			//Call the right function with SWITCH
			switch (x)
			{
			case "a":
				addPerson();
				break;
			case "d":
				System.out.println("Which person to delete (ID)?");
				int i = scan.nextInt();
				scan.nextLine();	//clean the buffer
				deletePerson(i);
				isDeleted = true;	//delete array is filled -> activate new option
				break;
			case "p":
				printAll();	
				break;
			case "sd":
				System.out.println(getPerson());
				break;
			case "u":
				undo();
				isDeleted = false;			//Array gets empty -> deactivate new options
				break;
			case "q":
				keepLooping = false;		//stop looping (in case the program is not closed)
				return;						//exit the program
			default:
				break;
			}
			
		}
		
		
		
		
	}
	
	
	//Get name from the user and add it to the arrays
	static void addPerson()
	{
		//Get input from the user
		System.out.println("Enter first name");
		firstNames.add(scan.nextLine());
		//scan.nextLine();
		
		System.out.println("Enter Last name");
		lastNames.add(scan.nextLine());
		//scan.nextLine();	//clean the buffer
	}
	
	
	//Delete person from arrayLists and save them into LastPerson array
	static void deletePerson(int i)
	{
		if ((i<0) || (i>firstNames.size()) || (i>lastNames.size()))
		{
			System.out.println("ERROR: List is empty or wrong ID");
			return;
		}
		//Delete
		lastPerson[0] = firstNames.remove(i-1);		//
		lastPerson[1] = lastNames.remove(i-1);
	}
	
	
	//return the name from the arrayLists (using index)
	static String getPerson(int i)
	{
		return (firstNames.get(i) +" "+ lastNames.get(i));
	}
	
	
	//Return  the name from "last person" array
	static String getPerson()
	{
		return (lastPerson[0]+ " " + lastPerson[1]);
	}
	
	
	//Print all persons in the lists
	static void printAll()
	{
		if (firstNames.isEmpty()){
			System.out.println("ERROR: List is empty");
			return;
		}
		for (int i=0; i<firstNames.size(); i++)
		{
			System.out.println((i+1) + " "+ getPerson(i));
		}
	}
	
	
	//Undo function
	static void undo()
	{
		//Add back from lastPerson
		firstNames.add(lastPerson[0]);
		lastNames.add(lastPerson[1]);
		//Reset "lastPerson" array
		lastPerson[0] = "";
		lastPerson[1] = "";
	}
	
	
	
	
	
	
	
	
}
