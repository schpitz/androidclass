package hackeru.implement;

import hackeru.widget.OnClickEvent;

public class CleanPerson  implements OnClickEvent{

	Person p;

	public CleanPerson(Person p) {
		this.p = p;
	}

	
	public void onClick() {
		p.name = "";
		p.age = 0;
	}
	

}
