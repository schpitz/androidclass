package hackeru.implement;

import hackeru.widget.Button;

public class MyClass {

	public static void main(String[] args) {
		Person p = new Person();
		
		Button addYear = new Button();
		Button clean = new Button();
		
		addYear.event = p;
		clean.event = new CleanPerson(p);
		
		addYear.click();
		addYear.click();
		addYear.click();
		addYear.click();
		clean.click();
		System.out.println(p);

	}

}
