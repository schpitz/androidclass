package hackeru.implement;

import hackeru.widget.OnClickEvent;

public class Person implements OnClickEvent{

	String name;
	int age;
	
	@Override
	public void onClick() {
		
		age++;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
	
	

}
