package hackeru.abstracts;

public abstract class Animal {

	private String name;
	protected int age;
	
	public Animal(String name) {
		this.name = name;
		age = 1;
	}
	
	public String getName() {
		return name;
	}
	
	
	public abstract void setAge(int age);
	

	
	public Animal(String name, int age) {
		this.name = name;
		this.age = age;
	}




	@Override
	public String toString() {
		return "Animal [name=" + name + ", age=" + age + "]";
	}
	
	

}
