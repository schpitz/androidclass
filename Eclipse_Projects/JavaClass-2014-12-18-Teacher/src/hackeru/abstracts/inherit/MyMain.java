package hackeru.abstracts.inherit;

import java.awt.geom.Point2D;
import java.util.Deque;

public class MyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
	}

}

abstract class Base{
	public abstract int getInt();
	public abstract String getString();
	
	public void Print(String str){
		System.out.println(str);
	}
}

class Sub extends Base{

	@Override
	public int getInt() {
		return 0;
	}

	@Override
	public String getString() {
		return "My String";
	}
	
}


abstract class SubAbstract extends Base{
	
	@Override
	public String getString() {
		return "Sub Abstract";
	}
}

class Tset extends SubAbstract{

	@Override
	public int getInt() {
		return 0;
	}
	
}









