package hackeru.abstracts;

public  class Cat extends Animal{

	public Cat() {
		super("Cat");
	}

	@Override
	public void setAge(int age) {
		if(age>0&& age<50)
			this.age = age;
	}

	public int getAge(){
		return age;
	}
	

}
