package hackeru.abstracts;

public class MyMain {

	public static void main(String[] args) {
		
		Animal animalPointer;
//		animalPointer = new Animal();
		animalPointer = new Dog();
		
		Animal[] animals ={new Dog(), new Cat()};
		
		animals[0].setAge(30);
		animals[1].setAge(30);
		
		System.out.println(animals[0]);
		System.out.println(animals[1]);
		
		System.out.println("Cat name is "+animals[1].getName());
		Cat c  = (Cat) animals[1];
		System.out.println("Cat age is "+c.getAge());
		c.getAge();

	}

}
