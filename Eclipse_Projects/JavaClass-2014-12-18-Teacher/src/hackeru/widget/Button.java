package hackeru.widget;

public class Button {

	int width;
	int height;
	//pointer = null
	public OnClickEvent event;
	
	//getters and setters
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	
	public void click(){
		System.out.println("button");
		if(event != null)
			event.onClick();
		
	}
	
	

}
