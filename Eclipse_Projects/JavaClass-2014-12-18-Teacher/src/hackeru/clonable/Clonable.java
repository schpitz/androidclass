package hackeru.clonable;

public interface Clonable {

	public Object onClone();
}
