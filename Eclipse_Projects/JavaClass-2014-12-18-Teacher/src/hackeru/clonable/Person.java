package hackeru.clonable;

public class Person implements Clonable{

	private String name;
	private int id;
	
	public Person(String name, int id) {
		this.name = name;
		this.id = id;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", id=" + id + "]";
	}

	@Override
	public Object onClone() {
		Person temp = new Person(name, id);
		
		return temp;
	}
	
	
	

}
