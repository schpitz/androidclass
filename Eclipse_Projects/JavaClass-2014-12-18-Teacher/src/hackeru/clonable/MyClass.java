package hackeru.clonable;

public class MyClass {

	public static void main(String[] args) {
		
		Person p = new Person("Person 1", 123456789);
		Person p2 = p;
//		p2 = new Person(p.name, p.id); private variables
		p2 = (Person) p.onClone();
		System.out.println(p2);

	}

}
