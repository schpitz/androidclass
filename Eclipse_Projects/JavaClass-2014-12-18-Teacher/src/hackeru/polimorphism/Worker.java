package hackeru.polimorphism;

public class Worker extends Person{
	
	Person manager;
	
	@Override
	public void print() {
		super.print();
		System.out.print(" manager = ");
		manager.print();
	}
	
}
