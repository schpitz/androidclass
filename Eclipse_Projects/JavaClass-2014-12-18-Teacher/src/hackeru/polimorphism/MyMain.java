package hackeru.polimorphism;

import java.util.Scanner;

public class MyMain {

	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		Person[] persons = {new Person(), new Worker(), new Student()};
	
		
		for (Person item : persons) {
			System.out.println("Enter name");
			item.name = scan.nextLine();
			System.out.println("Enter last");
			item.last = scan.nextLine();
			System.out.println();
		}
		
		Person teacher = new Person();
		Person manager = new Person();
		
		Object test = persons[0];
		test.toString();
//		test.name 
		
		if(persons[1] instanceof Worker){
			Worker w = (Worker) persons[1];
			w.manager = manager;
		}
		
		if(persons[2] instanceof Student){
			((Student)persons[2]).teacher = teacher;
		}
		
		//init array
		Worker w = new Worker();
		w.name = "my name";
		w.last = "my last";
		w.manager = manager;
		persons[0] = w;
		
		for (Person person : persons) {
			//System.out.println(person.name +" "+person.last);
			person.print();
			System.out.println();
		}

	}

}
