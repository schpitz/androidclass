package hackeru.implement;

import hackeru.widget.OnClickEvent;

public class CleanPerson implements OnClickEvent {
	
	Person p;
	
	
	//alt+shift+s+o
	public CleanPerson(Person p) {
		this.p = p;
	}

	
	//Must add this function :) (implements stuff)
	@Override
	public void OnClick() {
		p.name = "";
		p.age = 0;	
	}

}
