package hackeru.implement;

import hackeru.widget.OnClickEvent;

//Every class that implements "OnClickEvent" interface
//must execute "OnClick()" function
public class Person implements OnClickEvent{
	
	String name;
	int age;
	
	
	//Must be re-written
	@Override
	public void OnClick() {
		//when someone click on the button -> age++ 
		age++;	
	}

	
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
	
	
	

}
