package hackeru.implement;
import hackeru.widget.Button;

public class MyClass {
	
	public static void main(String[] args) {
		
		Person p = new Person();
		
		Button addYear = new Button();	//add 1 to current year
		Button clean = new Button();	//reset the fields

		
		addYear.event = p;			//Button=Person -> it's pointint on it, not null anymore
		//clean.event = null;
		clean.event = new CleanPerson(p);
		
		addYear.click();
		addYear.click();
		addYear.click();
		addYear.click();
		System.out.println(p);		//Test before clean.event
		clean.click();				//use the clean.click() button/function
		System.out.println(p);
	}
}
