package hackeru.clonable;

public class Person implements Clonable{
	
	private String name;
	private int id;

	
	//Constructor
	public Person(String name, int id) {
		this.name = name;
		this.id = id;
	}

	//ToString
	@Override
	public String toString() {
		return "Person [name=" + name + ", id=" + id + "]";
	}

	
	//Must add onClone() -> implements
	@Override
	public Object onClone() {
		Person temp = new Person(name,id);
		return temp;
	}
	
	
	
	

}
