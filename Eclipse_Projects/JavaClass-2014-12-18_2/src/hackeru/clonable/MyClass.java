package hackeru.clonable;

public class MyClass {
	
	public static void main(String[] args) {
		
		Person p1 = new Person("Person 1" , 123456789);
		Person p2 = p1;		//still got 1 person in memory (with 2 pointers)
		
		//p2 = new Person(p.name , p.id);	//p.name & p.id privates!
		Person p3 = (Person) p1.onClone();
		//we need casting because p.onClone() in Person class -> working only with Object type

	}
}
