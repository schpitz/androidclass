package hackeru.widget;

public class Button {
	
	int width;
	int height;
	// pointer = null
	public OnClickEvent event;
	
	
	//Getters & Setters
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	
	
	
	public OnClickEvent getEvent() {
		return event;
	}

	
	
	public void setEvent(OnClickEvent event) {
		this.event = event;
	}
	
	
	
	//Testing the button
	public Button()
	{
		System.out.println("New button!");
	}
	
	
	public void click(){
		System.out.println("Click");
		if(event != null)
			event.OnClick();
	}

}
