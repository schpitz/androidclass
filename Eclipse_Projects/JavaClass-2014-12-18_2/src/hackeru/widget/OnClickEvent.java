package hackeru.widget;

//This is INTERFACE
public interface OnClickEvent {
	
	//All of the functions are abstracts
	//You cannot create objects, just announce
	//(also it means you must create them somewhere)
	public void OnClick();

}
