package hackeru.strings;

public class MyStringManipulation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String myString = "first string";
		
		int length = myString.length();
		System.out.println(myString + " : length => " + length);
		
		char first = myString.charAt(0);
		char last = myString.charAt(length-1);
		//without the (-1) there will be a crash, because it will look for a char that doesn't exist
		
		System.out.println("First letter = " + first + " | Last letter = " + last);
		
		//Find space char (aka " ")
		int spacePos = myString.indexOf(" ");
		System.out.println("space (\" \") index of = " + spacePos);
		
		//Find "st" chars combination in the string
		//If not found -> result = -1
		System.out.println("1st st index = " + myString.indexOf("st"));
		System.out.println("2nd st index = " + myString.indexOf("st",4));
		
		
		//REPLACE in string
		//Replace a char
		String replace1 = myString.replace('t','k');
		System.out.println("replace1 = " + replace1);
		//Delete chars
		String replace2 = myString.replace("st", "");
		System.out.println("replace2 = " + replace2);
		
		//substring
		String word = myString.substring(spacePos + 1);
		System.out.println(word);
		System.out.println("test = " + myString.substring(8,9));
		
		//Boolean in strings
		boolean b = myString.isEmpty();					//b=>false
		System.out.println(myString.startsWith("a")); 	//false
		System.out.println(myString.endsWith("ng"));	//true
		
	}

}
