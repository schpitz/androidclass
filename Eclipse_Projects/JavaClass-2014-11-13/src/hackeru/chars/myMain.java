package hackeru.chars;

import java.util.Scanner;

public class myMain {
							
	public static void main(String[] args) {
				
	
			
			char c = 'a';
			System.out.println("c = " +c);
			System.out.println(c + " index of " + (int)c);
			
			
			// number to char
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter Number");	
			int input = scan.nextInt();
			System.out.println((char)input);
			
			
			// cast char to Int
			System.out.println("Enter Char");
			scan.nextLine();
			String sInput = scan.nextLine();
			
			//cast string to char
			char cast = sInput.charAt(0); //find the first char of the string "sInput"
			System.out.println(cast + " index = "+ (int)cast);
	}		
}
