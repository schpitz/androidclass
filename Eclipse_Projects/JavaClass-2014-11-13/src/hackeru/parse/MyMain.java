package hackeru.parse;

public class MyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Declare a new variable and print it
		String str = "258";
		System.out.println("str = " + str);
		
		//convert (cast) string to long
		long l = Long.parseLong(str);
		System.out.println("srt to long = " + l);
		
		//convert string to int
		int i = Integer.parseInt("258"); //it could be "str" or a new string
		System.out.println("str to int = " + i);
		
		//cast to double	(adds .0)
		double d = Double.parseDouble(str);
		System.out.println("str => double = " + d);
		
		//cast to byte
		/*		ERROR ------ str("258") is too large for byte
		byte b = Byte.parseByte(str);
		System.out.println("str => byte = " + b);
		*/
		
		
	}

}
