package hackeru.overflow;

public class myMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte b = Byte.MAX_VALUE;
		System.out.println("byte max value = "+b);
		
		long l1 = Long.MIN_VALUE;
		System.out.println("long min value is = " + l1);
		
		int max = Integer.MAX_VALUE;
		System.out.println("max = " + max);
		
		
		//Overflow
		//(max+1) => overflow of int => add a minus('-')
		long myLong = (max +1);	
		System.out.println("my Long =" + myLong);
		
		//first flow
		//Convert (cast) max into LONG type
		myLong = (long)max +1;
		System.out.println("first solution, myLong = "+ myLong);
		
		//second way
		//Create the number initially as LONG (numbers are int by default)
		myLong = max +1L;
		System.out.println("second solution, myLong= "+myLong);
		
		
		
		byte myByte = 120;
		//myByte = 120+1;  //  error -> myByte is saved as byte and the number 1 is saved as int
		//myByte = myByte + (byte)1;	//works -> because 1 will be converted to byte and just then added to byte (so no errors)
		myByte = (byte) (myByte + 1);
		System.out.println("myByte is: "+myByte);
		
	}

}
