package hackeru.abstracts;

public abstract class Animal {

	private String name;
	protected int age;

	public Animal(String Name) {
		this.name = Name;
		this.age = 1;
	}

	public Animal(String Name, int Age) {
		this.name = Name;
		this.age = Age;
	}

	public String getName() {
		return name;
	}

	
	// Announced function -> setAge
	//once we announce of abstract function - it must be mentioned in all other extensions!
	public abstract void setAge(int age);
	

	
	@Override
	public String toString() {
		return "Animal [name=" + name + ", age=" + age + "]";
	}

}
