package hackeru.abstracts;

public class MyMain {
	
	public static void main(String[] args) {
					
			Animal animalPointer;
			//animalPointer = new Animal(); 	//Error!
			//Animal x = new Animal();		//BAD
			//Animal x = new Cat();			//GOOD!
			// It's possible to create a pointer, but not an object
			
			animalPointer = new Dog();		//Possible
			Animal animalPointer2 = new Cat();
			
			//1. Abstract class can't be an object, but can be an extension of it
			//2. Abstract functions must be mentioned in the class' extensions
			//3. Abstract functions can be written only in abstract class
			
			
			//Array of animals
			Animal[] animals = {new Dog(), new Cat()};
			animals[0].setAge(30);	//should give error
			animals[1].setAge(30);
			//test
			System.out.println(animals[0]);
			System.out.println(animals[1]);
			
			
			System.out.println("Cat name is "+ animals[1].getName());
			//Must cast animals[1] into "Cat" object for using the .getAge() function
			System.out.println("Cat age is " + ((Cat)animals[1]).getAge());
			
	}

}
