package hackeru.abstracts;

public class Dog extends Animal {
	
	public Dog() {
		super("Dog");		//construct with the Animal() constructor
	}

	//Must implement the abstract function "setAge()"
	@Override
	public void setAge(int age) {		
		if(age>0 && age<7)
			this.age = age;
	}
}
