package hackeru.abstracts.inherit;

public class MyMain {
	public static void main(String[] args) {

	}
}




abstract class Base{
	
	public abstract int getInt();	//returns int
	public abstract String getString(); //returns string
	
	public void Print(String str){
		System.out.println(str);
	}
}


//New extended class must execute the abstracts functions
class Sub extends Base{

	@Override
	public int getInt() {
		return 0;
	}

	@Override
	public String getString() {
		return null;
	}	
}


//Abstract extension of an Abstract class 
//(doesn't have to execute anything - because it can't be an object)
abstract class SubAbstract extends Base{
	@Override
	public String getString() {
		return "Sub-Abstract";
	}
}




class Test extends SubAbstract{
	@Override
	public int getInt() {
		return 0;
	}
}