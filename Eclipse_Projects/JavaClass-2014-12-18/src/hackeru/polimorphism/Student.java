package hackeru.polimorphism;

public class Student extends Person{

	Person teacher;
	
	public void print(boolean b)
	{
		super.print();
		System.out.println(teacher.name + " " + teacher.last);
	}
}
