package hackeru.polimorphism;

import java.util.Scanner;

public class MyMain {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Person[] persons = {new Person(),
							new Worker(),
							new Student()};
		
		for (Person item : persons)
		{
			System.out.println("Enter first name");
			item.name = scan.nextLine();
			System.out.println("Enter last name");
			item.last = scan.nextLine();
			System.out.println();
		}
		
		Person teacher = new Person();
		Person manager = new Person();
		
		Object test = persons[0];
		test.toString();		
		
		if (persons[1] instanceof Worker)
		{
			Worker w = (Worker)persons[1];	//casting
			
		}
		
		
	}

}
