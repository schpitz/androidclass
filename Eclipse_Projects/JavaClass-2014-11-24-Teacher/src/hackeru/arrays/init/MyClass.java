package hackeru.arrays.init;

import java.util.Arrays;

public class MyClass {

	
	
	public static void main(String[] args) {
		//1
		int[] intArray1 = new int[5];
		String[] stringArray = new String[5];
				
		//2
		int[] intArray2 = {1, 8, 3, 7};
		intArray2[0] = 7;
		String[] stringArray2 = {"a", "2", intArray2[1]+""};//"8"
		
		//3
		int[] intArray3;
//		intArray3 ={1,2,3};
		intArray3 = new int[]{1,8, 10,2};
		intArray2 = new int[]{2,9};
		String[] strArray3;
		strArray3 = new String[]{"a", "b","8"};
		
		

		int[] array = getArray(978);
		System.out.println(Arrays.toString(array));
	}
	
	//return int array
	static int[] getArray(int i){
		int[] arr = null;
		
		if(i>99){
			arr = new int[3];
			arr[0] = i /100;
			arr[1] = i % 100 / 10;
			arr[2] = i % 10;
			
		}else if(i>9){
			arr = new int[2];
			arr[0] = i/10;
			arr[1] = i%10;
		}else{
			arr = new int[1];
			
			arr[0] = i;
		}
		
		
		return arr;
	}

	
	
	
	
	
	
	
	
	
	
	
	
}
