package hackeru.arraylist;

import java.util.ArrayList;


public class MyArray {

	static ArrayList<String> names = new ArrayList<>();
	
	public static void main(String[] args) {
		int size = names.size();//0
		
		names.add("Izik");
		names.add("Avi");
		System.out.println(names.size());//print size array
		
		String izik = names.get(0);
		System.out.println(izik);
		System.out.println(names.get(1));
		
		//
		//names.remove(0);
		String removed = names.remove(0);
		System.out.println(removed + " removed"); // Izik removed
		//System.out.println(names.remove(0) + "removed");
		
	}

}
