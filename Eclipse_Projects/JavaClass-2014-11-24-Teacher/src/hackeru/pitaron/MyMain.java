package hackeru.pitaron;

import java.util.ArrayList;
import java.util.Scanner;

public class MyMain {
	
	static Scanner scan = new Scanner(System.in);
	static ArrayList<String> names = new ArrayList<>();
	static ArrayList<String> last  = new ArrayList<>();
	static String[] lastPerson = {"",""};
	
	public static void main(String[] args) {
		
		while(true){
			System.out.println("Choose:");
			System.out.println("a - to add person");
			System.out.println("d - to delete");
			System.out.println("p - to print all persons");
			
			if( ! lastPerson[0].isEmpty() ){ // String str = ""; => this is empty
				System.out.println("sd - to show last person to deleted");
				System.out.println("u - to return person to deleted");
			}
			System.out.println("q - to exit");
			
			String selected = scan.nextLine();
			switch (selected) {
			case "a":
				addPerson();
				break;
			case "d":
				System.out.println("enter the number of the person to delete");
				int number = scan.nextInt()-1;
				scan.nextLine();//clean buffer
				deletePerson(number);
				break;
			case "p":
				printAll();
				break;
			case "q":
				return;
			case "sd":
				System.out.println(getPerson());
				break;
			case "u":
				undo();
				break;
			default:
				break;
			}
			
			System.out.println();
			System.out.println();
		}
		
	}
	
	 
	static void undo() {
		names.add(lastPerson[0]);
		last.add(lastPerson[1]);
		lastPerson[0] = "";
		lastPerson[1] = "";
	}


	static void printAll() {
		if(names.size()>0){
			for (int i = 0; i < names.size(); i++) {
				System.out.println(i+1+" "+ getPerson(i));
			}
		}
	}

	static String getPerson(int index){
		return names.get(index)+ " " + last.get(index);
	}
	
	static String getPerson(){
		return lastPerson[0] + " " + lastPerson[1];
	}

	static void deletePerson(int index) {
		if(index <0 || names.size() <= index){
			System.out.println("not number of index "+index);
			return;
		}
		
		lastPerson[0] = names.remove(index);
		lastPerson[1] = last.remove(index);
		System.out.println("deleted");
		
	}


	static void addPerson(){
		//add first name
		System.out.println("Enter name");
		String name = scan.nextLine();
		names.add(name);
		
		//add last name
		System.out.println("Enter last name");
		last.add(scan.nextLine());//�� ����� ���� ���� ��� ����� �� �� � 2 �����
		
	}

}
