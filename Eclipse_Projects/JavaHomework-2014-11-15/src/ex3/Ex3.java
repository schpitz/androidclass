package ex3;

import java.util.Scanner;

public class Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter departure time (hh:mm:ss)");
		String flightD = scan.nextLine();
		
		System.out.println("Enter flight time (hh:mm:ss)");
		String flightT = scan.nextLine();

		String flightL;
		int sec = 0;
		int min = 0;
		int hours = 0;
		
		sec = Integer.parseInt(flightD.substring(6,8)) + Integer.parseInt(flightT.substring(6,8));
		if (sec>59){
			min++;
			sec = sec-60;
		}
		
		min = min + Integer.parseInt(flightD.substring(3,5)) + Integer.parseInt(flightT.substring(3,5));
		if (min>59)
		{
			hours++;
			min = min - 60;
		}
		
		hours = hours + Integer.parseInt(flightD.substring(0,2)) + Integer.parseInt(flightT.substring(0,2));
			
		
		System.out.println("The landing time is: " + hours + ":" + min + ":" + sec);
		
		
	}

}
