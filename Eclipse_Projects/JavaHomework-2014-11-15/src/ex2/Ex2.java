package ex2;

import java.util.Scanner;

public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		byte day;
		byte year;
		byte month;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter a date at the following formar: dd/mm/yy");
		String aDate = scan.next();
		
		//Convert date format to Bytes
		day = Byte.parseByte(aDate.substring(0,2));
		month = Byte.parseByte(aDate.substring(3,5));
		year = Byte.parseByte(aDate.substring(6,8));
		
		//Print numbers
		System.out.println("Day = " + day);
		System.out.println("Month = " + month);
		System.out.println("Year = " + year);
	}

}
