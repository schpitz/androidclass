package ex4;

import java.util.Scanner;

public class Ex4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a number (<100)");
		int num = scan.nextInt();
		
		int ones = num % 10;
		int dozens = num / 10;
		//System.out.println(ones);
		//System.out.println(dozens);
		
		if ((ones%2!=0) && (dozens%2!=0))
		{
			System.out.println("The number includes odd digits only");
		} else if ((ones%2==0) && (dozens%2==0))
		{
			System.out.println("The number includes even digits only");
		} else {
			System.out.println("Mixed Numbers");
		}
		
		
	}

}
