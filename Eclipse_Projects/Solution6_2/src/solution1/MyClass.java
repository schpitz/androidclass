package solution1;

import java.util.Scanner;

public class MyClass {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Enter yor statement");
		String s = scan.nextLine();
		
		//in line 1 way
		print(s);
		//2 way
		char[] chars = s.toCharArray();
		print(chars, true);
		
		//all word new line
		//String[] array = s.split(" ");
		print(s.split(" "));
		
		//all char in new line
		print(s.toCharArray(), false);
	}
	
	static void print(String str){
		System.out.println(str);
	}
	
	static void print(String[] str){
		for (int i = 0; i < str.length; i++) {
			System.out.println(str[i]);
		}
	}
	
	static void print(char[] chars, boolean inLine){
		if(inLine){
			for (int i = 0; i < chars.length; i++) {
				System.out.print(chars[i]);
			}
			System.out.println();
		}
		else{
			for (int i = 0; i < chars.length; i++) {
				System.out.println(chars[i]);
			}
			System.out.println();
		}
	}

}
