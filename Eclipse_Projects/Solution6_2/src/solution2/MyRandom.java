package solution2;

import java.util.Random;

public class MyRandom {
	
	static Random rand = new Random();
	public static void main(String[] args) {
		
		System.out.println(getRandom());
		System.out.println(getRandom(-20, -10));
		
	}
	
	static int getRandom(){
		return rand.nextInt(1000)+500;
	}

	static int getRandom(int min, int max){
		if(max <= min)
			return getRandom();
		else {
			return rand.nextInt(Math.abs(max-min)) + min;
		}
	}
}
