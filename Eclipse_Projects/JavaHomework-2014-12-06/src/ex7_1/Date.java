package ex7_1;

public class Date {

	private int day;
	private int month;
	private int year;

	// Getters & Setters
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day <= 31)
			this.day = day;
		else
			System.out.println("Error - Day is wrong");

	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month > 0 && month <= 12)
			this.month = month;
		else
			System.out.println("ERROR - Month is error");

	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	
	// TO-STRING
	@Override
	public String toString() {
		return ("Date: " + this.day + "/" + this.month + "/" + this.year);
	}

}
