package ex7_1;

import java.util.Scanner;

public class MyMain {

	static Date[] dates = new Date[5];
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		for (int i = 0; i < dates.length; i++) {
			dates[i] = new Date();
			System.out.println((i + 1) + "TIME:");
			System.out.println("Enter day");
			dates[i].setDay(scan.nextInt());
			System.out.println("Enter month");
			dates[i].setMonth(scan.nextInt());
			System.out.println("Enter year");
			dates[i].setYear(scan.nextInt());
		}
		
		
		//Now pring all the years
		for (int i = 0; i < dates.length; i++) {
			System.out.println(dates[i].toString());
		}

	}

}
