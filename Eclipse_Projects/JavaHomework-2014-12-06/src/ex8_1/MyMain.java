package ex8_1;

public class MyMain {

	public static void main(String[] args) {
		Car car1 = new Car(123,"The Schpitz",99);
		car1.CarRentValue(1);
		System.out.println(car1);
	
		Car car2 = new Car(321,"Rocks",77);
		car1.CarRentValue(31);
		System.out.println(car2);
		
		Car car3 = new Car(321,"Bettys");
		car3.setPricePerDay(999);
		car3.CarRentValue(13);
		car3.CarRentValue(5);
		System.out.println(car3);
		
		Car car4 = new Car(007);
		car4.setPricePerDay(888);
		car4.setType("Rocks");
		car4.CarRentValue(12);
		car4.CarRentValue(12);	//Add more renting days
		car4.CarRentValue(12);	//More renting days  
		System.out.println(car4);
		
	}
}
