package ex7_3;

public class Point{
	
	private int x = 0;
	private int y = 0;
	
	

	//Default constructor
	public Point() 
	{
		this.x = 0;
		this.y = 0;
	}
	
	public Point(int x1,int y1)
	{
		this.x = x1;
		this.y = y1;
	}

	
	//ToString -> in order to print the x&y positions
	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + "]";
	}
	
	
	

}
