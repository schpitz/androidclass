package ex7_3;

public class MyMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Line 1 - with auto-constructors
		System.out.println("Line 1:");
		Line line1 = new Line();
		System.out.println(line1);
		
		//Line 2 - with OVI (line strength) and one manual constructors
		System.out.println("Line 2");
		Point x = new Point();
		Point y = new Point(2,5);
		Line line2 = new Line(x,y,3);
		System.out.println(line2);
		
		//Line 3 - with all manual constructors
		System.out.println("Line 3");
		x = new Point(3,10);
		Line line3 = new Line(x,y,2);
		System.out.println(line3);
		
	}

}
