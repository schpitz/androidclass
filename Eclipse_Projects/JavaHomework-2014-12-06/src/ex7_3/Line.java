package ex7_3;

public class Line {
	private Point startLine = new Point();
	private Point endLine = new Point();
	private double OVI = 0;
	
	
	public Line() {
		// TODO Auto-generated constructor stub
		this.startLine = new Point();
		this.endLine = new Point();
		this.OVI = 0;
	}
	
	public Line(Point x, Point y, double z){
		this.startLine = x;
		this.endLine = y;
		this.OVI = z;
	}

	@Override
	public String toString() {
		return "Line [startLine=" + startLine + ", endLine=" + endLine
				+ ", OVI=" + OVI + "]";
	}
	
	

}
