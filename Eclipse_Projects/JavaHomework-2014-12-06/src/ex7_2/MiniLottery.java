package ex7_2;

import java.util.ArrayList;
import java.util.Random;


public class MiniLottery {
	
	private int maxMembers;		//Max members of the lottery system
	private ArrayList<String> arr = new ArrayList<>();		//String array-list
	static Random rand = new Random();
	
	//Constructor
	public MiniLottery(int num) {
		this.maxMembers = num;
	}
	
	//Add a name to the arrayList
	void add(String nameX){
		if (arr.size()<maxMembers)			//if the arrayList size is small than the maxMembers
			arr.add(nameX);					//Add the string to the arrayList
		else
			System.out.println("Members are full");
	}
	
	//Get a winner from the lotter
	void lottery(){
		int tempX = rand.nextInt(arr.size());		//get a random number from 0 to the length of the array
		System.out.println(arr.get(tempX));
	}
	
	

}
