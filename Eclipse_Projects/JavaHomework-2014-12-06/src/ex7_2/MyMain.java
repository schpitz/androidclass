package ex7_2;

import java.util.Random;
import java.util.Scanner;

public class MyMain {

	static String[] PotentialNames = {"Lior","Noa","Betty","Michael","Sason","Hana","Becky","Yoela","Ohad","Levi","Rockstar","Jack","John"};
	static Random rand = new Random();
	
	public static void main(String[] args) {
		boolean keepLooping = true;
		int numMembers = 8;			//Manually decide the Max Members
		
		
		//Run only 5 times! (of the contest)
		for (int i=0; i<5; i++){
			
			MiniLottery newLot = new MiniLottery(numMembers);		//New max members
			//Give automatic names while creating a new type in the array
			for (int j = 0; j < numMembers; j++) {
				String tempName = PotentialNames[rand.nextInt(PotentialNames.length)];
				newLot.add(tempName);			//Add the name
			}			
			System.out.print("The winner is: ");
			newLot.lottery();
		}
		
//		//NOT USING WHILE LOOP!!!
//		while (keepLooping){
//			MiniLottery newLot = new MiniLottery(8);
//		}
		
		
	}
	
}
