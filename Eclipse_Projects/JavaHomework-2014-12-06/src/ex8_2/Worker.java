package ex8_2;

import java.util.Random;

public class Worker {
	private String privateName;		//readable - with getter
	private String lastName;		//readable - with getter
	private int year;				//readable - with getter
	private int id;					//not readable!
	private int salary;				//Changeable
	static Random rand = new Random();
	
	//Getters & Setters
	public String getPrivateName()
	{
		return this.privateName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public int getBirthYear()
	{
		return this.year;
	}
	
	public int getSalary()
	{
		return this.salary;
	}
	
	
	
	void setSalary(int newSalary)
	{
		if (newSalary>=0 && newSalary<=4900)
			this.salary = newSalary;
		else
			System.out.println("Salary shoulw be up to 4900!");
	}

	
	
	//Constructor
	public Worker() {
		this.privateName = "John";
		this.lastName = "Smith";
		this.salary = rand.nextInt(4900);
		this.id = rand.nextInt()+1;
		this.year = 1985;
	}
	
	public int LastSalary()
	{
		//return random between (salary-1000) up to (salary+1000)
		int newSalary = rand.nextInt(2000) + (this.salary-1000);
		setSalary(newSalary);		//Update it in the object
		return newSalary;
		
	}
	
	@Override
	public String toString() {
		return "Worker [First Name:" + privateName 
				+ ", Last Name:" + lastName
				+ ", Birth Year:" + year 
				+ ", ID:" + id 
				+ ", Basic Salary:" + salary 
				+ ", Last Salary: " + LastSalary() + "]";
	}
	
	
	
	
	
}
