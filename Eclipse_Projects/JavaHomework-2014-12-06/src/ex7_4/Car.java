package ex7_4;

public class Car {
	
	private String model;
	private int price;
	Color carColor = new Color();
	
	
	//Setters
	public void setModel(String model)
	{
		this.model = model;
	}
	
	public void setPrice(int price)
	{
		this.price = price;
	}
	
	
	@Override
	public String toString() {
		return "Car [model=" + model + ", price=" + price + ", Color="
				+ carColor + "]";
	}
	
	

}
