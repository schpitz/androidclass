package ex7_4;

public class MyMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//Set with manual setters
		Cloth cloth1 = new Cloth();
		cloth1.size = 2;
		cloth1.brand = "Rosenspitz";
		System.out.println(cloth1.toString());
		
		Cloth cloth2 = new Cloth();
		cloth2.size = 21;
		cloth2.brand = "Kelvin Klein";		
		cloth2.ClothColor.setB(255);
		cloth2.ClothColor.setG(255);
		cloth2.ClothColor.setR(255);
		System.out.println(cloth2);
		
		Cloth cloth3 = new Cloth();
		cloth3.size = 4;
		cloth3.brand = "Bettys";
		cloth3.ClothColor.setG(100);
		System.out.println(cloth3);
		
		//Set with class setters (functions)
		Car car1 = new Car();
		car1.setModel("The Schpitz");
		car1.setPrice(2999);
		System.out.println(car1);
		
		Car car2 = new Car();
		car2.setModel("Bettys");
		car2.setPrice(9999);
		car2.carColor.setB(255);
		System.out.println(car2);
		
		Car car3 = new Car();
		car3.setModel("Thr Rockstar");
		car3.setPrice(1);
		car3.carColor.setB(255);
		car3.carColor.setG(255);
		car3.carColor.setR(255);
		System.out.println(car3);
		
	}

}
