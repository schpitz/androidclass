package ex7_4;

public class Color {

	private int R;
	private int G;
	private int B;

	// Getters & Setters
	//RED
	public int getR() {
		return R;
	}

	public void setR(int r) {
		if (r >= 0 && r <= 255)
			R = r;
		else {
			System.out.println("Error with R");
			R = 0;
		}
	}

	//GREEN
	public int getG() {
		return G;
	}

	public void setG(int g) {
		if (g >= 0 && g <= 255)
			G = g;
		else {
			System.out.println("ERROR with G");
			G = 0;
		}
	}

	//BLUE
	public int getB() {
		return B;
	}

	public void setB(int b) {
		if (b >= 0 && b <= 255)
			B = b;
		else {
			System.out.println("ERROR with B");
			B = 0;
		}
	}

	@Override
	public String toString() {
		if (B == 0 && G == 0 && R == 0) {
			return ("Black [R=" + R + ", G=" + G + ", B=" + B + "]");
		} else if (B == 255 && G == 255 && R == 255) {
			return ("White [R=" + R + ", G=" + G + ", B=" + B + "]");
		} else {
			return "MIX Color [R=" + R + ", G=" + G + ", B=" + B + "]";
		}
	}

}
