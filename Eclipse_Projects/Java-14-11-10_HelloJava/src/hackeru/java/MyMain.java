package hackeru.java;

public class MyMain {

	public static void main(String[] args) {
		// This is the main function, appear only once in any package!
		//	Ctrl + Space (autocomplete)
		
		
		String str = "Hello World";
		
		System.out.println(str);	//system library -> out -> print srt
		System.out.println(str);	//SYSO Ctrl+Space

		str = "my test string";		//new content
		System.out.println(str);
		
		str = "my \ntest string";	//breakline
		System.out.println(str);
		
		str = "my \"string\"";		//how to print " with 
		System.out.println(str);
		
		str = "my string \\";		//print a backslash (/)
		System.out.println(str);
		
		
	}
	
}
