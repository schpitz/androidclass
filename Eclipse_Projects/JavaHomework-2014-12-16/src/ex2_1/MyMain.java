package ex2_1;

import java.util.Scanner;

import ex2.Car;
import ex2.air;
import ex2.ground;
import ex2.water;

public class MyMain {

	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		Car[] carList = new Car[5];
		for (int i = 0; i < carList.length; i++) {
			System.out.println("Enter:");
			System.out.println("1 - Ground Vhiecle");
			System.out.println("2 - Air vheicle");
			System.out.println("3 - Underwater vhiecle");

			int tempChoose = scan.nextInt();
			scan.nextLine();
			
			switch (tempChoose){
			case 1:
				System.out.println("What is the color?");
				String colorX = scan.nextLine();
				System.out.println("Where is it? (type city or location)");
				String location = scan.nextLine();
				ground newGroundCar = new ground();
				newGroundCar.wheels = 4;
				newGroundCar.color = colorX;
				newGroundCar.Point = location;
				newGroundCar.capacity = 5;
				carList[i] = newGroundCar;
				break;
			case 2:
				System.out.println("What is the color?");
				String colorX1 = scan.nextLine();
				System.out.println("Where is it? (type city or location)");
				String location1 = scan.nextLine();
				air aircraft = new air();
				aircraft.Point = location1;
				aircraft.color = colorX1;
				aircraft.maxHeight = 195.1;
				aircraft.capacity = 2;
				carList[i] = aircraft;
				break;
			case 3:
				System.out.println("What is the color?");
				String colorX2 = scan.nextLine();
				System.out.println("Where is it? (type city or location)");
				String location2 = scan.nextLine();
				water sub = new water();
				sub.Point = location2;
				sub.color = colorX2;
				sub.isDiving = true;
				sub.capacity = 2;
				carList[i] = sub;
				break;
			default:
				System.out.println("error!");
			}
		}
		
		//Print array
		for (int i = 0; i < carList.length; i++) {
			System.out.println(carList[i].toString());
		}
		//Could also do it with ForEach loop (and not for loop)

	}
}
