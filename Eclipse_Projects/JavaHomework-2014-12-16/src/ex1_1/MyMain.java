package ex1_1;

import java.util.Scanner;

import ex1.Circle;
import ex1.Square;
import ex1.rectangle;
import ex1.shape;

public class MyMain {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		shape[] shapesList = new shape[5];				//create array of 5 basic shapes
		for (int i = 0; i < shapesList.length; i++) 
		{
			System.out.println((i + 1) + "---Choose shape:");
			System.out.println("1-Circle | 2-Square | 3-Rectangle");
			int temp = scan.nextInt();

			if (temp == 1) {							//user chose circle
				Circle newCircle = new Circle();		//create a temp circle 
				System.out.println("Enter Radius:");
				int r = scan.nextInt();
				newCircle.getShape(r);					//use getShape() with new data (radius)
				shapesList[i] = newCircle;				//cast circle into the object
			} else if (temp == 2) {						//user chose square
				Square newSquare = new Square();		//create a temp square
				System.out.println("Enter one side length");
				int x = scan.nextInt();
				newSquare.getShape(x);
				shapesList[i] = newSquare;				//cast square into the shape
			} else {									//User chose rectangle
				rectangle newRect = new rectangle();
				System.out.println("Enter length:");
				int x = scan.nextInt();
				System.out.println("Enter width");
				int y = scan.nextInt();
				newRect.getShape(x, y);
				shapesList[i] = newRect;
			}
		}

		//Print all objects in the array (using the printShape() function)
		for (int i = 0; i < shapesList.length; i++) {
			shapesList[i].printShape();
		}

	}

}
