package ex3;

public class Car {
	
	private int id;
	private String type;
	private int pricePerDay;
	
	private int Rented = 0;	//Rented days so far... will be updated!
	
	
	//Now let's build some constructors
	//Automatic constructor
	public Car() {
	}
	
	//Full manual constructor
	public Car(int id,String type,int price)
	{
		this.id = id;
		this.type = type;
		this.pricePerDay = price;
	}
	
	//Constructor of 1-2
	public Car(int id, String type)
	{
		this.id = id;
		this.type = type;
	}
	
	//Constructor of 1st field
	public Car(int id)
	{
		this.id = id;
	}

	
	static boolean isCarType(Object x)
	{
		boolean isTrue;
		if (x.getClass() == Car.class)
			isTrue = true;
		else
			isTrue = false;
		
		return isTrue;
	}
	
	//Function will get the renting days, and return the price of the renting. Also will update the total rented days of this car
	public int CarRentValue(int daysOfRent){
		
		this.Rented = this.Rented + daysOfRent;	//Increase the rented value with the new days entered
		int x = daysOfRent*pricePerDay;		//days*price = total price
		return x;							//Return the price for renting the car for few days
	}
	
	public int CarMoneyMaking(){
		int x = Rented * pricePerDay;	//Total days * price = total value
		return x;
	}
	
		
	
	
	//Getters & Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPricePerDay() {
		return pricePerDay;
	}

	public void setPricePerDay(int pricePerDay) {
		this.pricePerDay = pricePerDay;
	}

	public int getRented() {
		return Rented;
	}

	public void setRented(int rented) {
		Rented = rented;
	}



	
	
	@Override
	public String toString() {
		return "Car [id=" + id + ", type=" + type + 
				", Price Per Day=" + pricePerDay + 
				", Rented Days=" + Rented + 
				", Total Income= " + CarMoneyMaking() +"$]";
	}
	

}
