package ex3;

import java.util.Scanner;

public class MyMain {
	
	public static void main(String[] args) {
		
		Object[] objectList = new Object[6];
		Scanner scan = new Scanner(System.in);
		
		//Getting animals and cars into object
		for (int i = 0; i < objectList.length; i++) {
			if (i%2==0)
				objectList[i] = new Car();
			else
				objectList[i] = new Animal();
		}
		
		boolean keepShowing = true;
		while (keepShowing)
		{
			System.out.println("WELCOME");
			System.out.println("Please choose what to show:");
			System.out.println("1 - ALL");
			System.out.println("2 - Animals only!");
			System.out.println("3 - Cars only!");
			int x = scan.nextInt();
			
			switch (x)
			{
			case 1:
				for (Object tempObject : objectList)
				{
					System.out.println(tempObject);
				}
				break;
			case 2:
				for (Object tempObject : objectList)
				{
					if (tempObject.getClass()==Animal.class)
						System.out.println(tempObject);
				}
				break;
			case 3:
				for (Object tempObject : objectList)
				{
					if (tempObject.getClass()==Car.class)
						System.out.println(tempObject);
				}
			}
		}
		
		
		
	}

}
