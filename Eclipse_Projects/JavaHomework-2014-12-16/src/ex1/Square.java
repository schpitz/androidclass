package ex1;

public class Square extends shape {

	int sideHeight;

	public Square() {
		this.name = "Square";
	}
	
	public void getShape(int x) {
		this.sideHeight = x;
	}

	public void printShape() {
		System.out.println(this.name + " area -> " + (this.sideHeight*this.sideHeight));
		System.out.println(this.name + " perimeter -> " + (this.sideHeight*4));
	}

}