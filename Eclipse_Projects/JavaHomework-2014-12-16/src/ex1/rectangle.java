package ex1;

public class rectangle extends shape {
	int sideHeight;
	int sideWidth;

	public rectangle() {
		this.name = "Rectangle";
	}
	
	
	public void getShape(int x , int y) {
		this.sideHeight = x;
		this.sideWidth = y;
	}

	public void printShape() {
		System.out.println(this.name + " area -> "
				+ (this.sideHeight * this.sideWidth));
		System.out.println(this.name + " perimeter -> "
				+ ((this.sideHeight * 2) + (this.sideWidth * 2)));
	}
}
