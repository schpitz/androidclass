package ex1;

public class Circle extends shape {

	int radius;

	public Circle() {
		this.name = "Circle";
	}
		
	public void getShape(int r) {
		this.radius = r;
	}

	@Override
	public void printShape() {
		System.out.println(this.name + " area -> " + (radius * radius * Math.PI));
		System.out.println(this.name + " perimeter -> " + (2 * radius));
	}
	

}
