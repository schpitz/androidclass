package hcakeru.classes;

public class MyMain {
	
	public static void main(String[] args) {
	
		//Array of objects
		Object[] objArray = new Object[4];	
		objArray[0] = new Base();
		objArray[1] = new Base();
		objArray[2] = objArray[1];			//so far we have 2 objects on 3 places
		objArray[3] = new Sub();			//extended of Base
		
		for (Object itemX : objArray){
			//check if the object is Base or Extension of it (4 items)
			if (itemX instanceof Base)			
				System.out.println("BASE");
		
			//Check of SUB (1 item)
			if (itemX instanceof Sub)			
				System.out.println("SUB");
			
			//Check only for true BASE, not extensions of if! (3 items)
			if (itemX instanceof Base && !(itemX instanceof Sub))
				System.out.println("TRUE BASE");
		}
		
		
		//Get the objects' classes info
		//Every object has static info of its class (via .class)
		for (Object itemY : objArray){
			System.out.println(itemY.getClass().getName());
		}
	
		
		//alternative filter
		for (Object itemZ : objArray){
			if (itemZ.getClass() == Base.class)
				System.out.println("True Base Class");			
		}
		
		
		System.out.println();
		System.out.println("---Equals---");
		for (Object items : objArray)
		{
			if (items == objArray[1])
				System.out.println("==");
		}
		
		//With objects == is not equals to a content 
		//but to the place in the memory
//		Base b = new Base();
//		Base b2 = new Base();
//		(b2 == b)? -> NO
//		b2=b;
//		(b2==b) ? -> YES
		
	}	
}



class Base{
	@Override
	public String toString() {
		return "Base";
	}
}



class Sub extends Base{
	@Override
	public String toString() {
		return "Sub";
	}
}