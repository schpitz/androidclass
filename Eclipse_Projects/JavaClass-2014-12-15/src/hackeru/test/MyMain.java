package hackeru.test;

import solution2.CTO;
import solution2.Manager;
import solution2.Worker;

public class MyMain {

	
	public static void main(String[] args) {
		
		//array of 2 managers
		Manager[] managers = new Manager[2];
		//Manually fill the managers in the array
		managers[0] = new Manager("Manager 1",																	//name of manager
									10,																			//id
									10000,																		//salary
									new Worker[]{new Worker("Worker1",2,7000) , new Worker("Worker 2",3,5000)} 	//array of workers
		);
		managers[1] = new Manager("Manager 2",
									11,
									9999,
									new Worker[]{new Worker("Worker3",4,7000) , new Worker("Worker 4",5,5000)}
		);
		
		//In the class we called CTO -> CEO2
		//
		CTO ceo = new CTO("CEO2 - THE BIG BOSS" , 100 , 1000000, managers);
		ceo.print();
	}
}
