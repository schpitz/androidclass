package hackeru.polymorphism;

public class MyMain {

	int i = 9; // new var

	public static void main(String[] args) {
		MyMain main = new MyMain();
		main.i = 10;
		System.out.println(main.i);
		
		//C
		Base b = new Base();
		b.print();
		
		System.out.println();
		b = new Sub();
		b.print();
		//b.print(10);
	}
}


// Class inside Class
class Base {
	String name = "base class";

	public void print() {
		System.out.println(name);
	}
}


// Another class inside a class
class Sub extends Base {
	String tag = "tag";

	// this.name = "sub"; //it will change "name"

	public Sub() {
		name = "Sub class";
	}

	// not overriding the old "print()" function -> different signature
	public void print(int i) {
		// super.print(); //calls the 1st print() (of class base)
		print(); // calls the 1st print() (of class base)
		System.out.println(tag);
	}

	// Now print is override (replacing existing function)
	@Override
	public void print() {
		System.out.println("overriding!!!");
	}

}