package hackeru.polymorphism.casting;

public class MyMain {
	public static void main(String[] args) {
		
		//New object
		Base b = new Base();
		System.out.println("Tag -> " + b.TAG);
		
		//New object (extended of base)
		Sub sub = new Sub();
		System.out.println("Tag -> " + sub.TAG + " Sub tag -> " + sub.SubTAG);
		

		//Old convert process
		Base b2 = new Sub();
		System.out.println("Tag b2 -> " + b2.TAG);
		//Sub s2 = new Base();	//ERROE - sub has base, but base doesn't have sub
		Base b3 = new Base();
		b3 = new Sub();			//it's basically like "Base X=new Sub()" (or like b2)
		System.out.println("Tag b3 -> " + b3.TAG);
		
		
		//Mix - we need to cast (convert)
		//We can convert the index (mezahe), not the content 
		//Base bTest = new Base();		//Won't work - because later on it should be converted to SUB
		Base bTest = new Sub();
		Sub testMIX1 = (Sub) bTest;		//we convert b3 from "Base" to "Sub"
		System.out.println("TAG Test -> " + testMIX1.TAG);
		System.out.println("TAG Test -> " + testMIX1.SubTAG);
	
	}
	
}


class Base{
	String TAG = "base";
}
//Base -> has 1 string

class Sub extends Base{
	String SubTAG = "sub";
}
//Sub -> has 2 strings