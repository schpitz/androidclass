package solution2;

public class Manager extends Worker{

	protected String typeArray = "Workers";
	Worker[] workers;//null
	
	public Manager(String name, int id, int salary, Worker[] workers) {
		super(name, id, salary);
		this.workers = workers;
	}

	@Override
	public void print() {
		super.print();
		System.out.println(typeArray + " array length: "+workers.length);
		for (Worker w : workers) {
			System.out.print("\t");
			w.print();
		}
		System.out.println();
	}
}
