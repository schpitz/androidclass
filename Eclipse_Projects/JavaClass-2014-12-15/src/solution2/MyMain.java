package solution2;

import java.util.Random;

public class MyMain{

	static Random r = new Random();
	static int counterWorker;
	public static void main(String[] args) {
		Manager[] managers = new Manager[4];
		
		for (int i = 0; i < managers.length; i++) {
			
			int count = r.nextInt(10);
			Worker[] workers = new Worker[count];
			
			for (int j = 0; j < count; j++) {
				int id =  r.nextInt(999999999)+11111111;
				Worker w = new Worker("worker "+ counterWorker++, id, r.nextInt(5000)+4000);
				workers[j] = w;
			}
			
			
			managers[i] = new Manager("manager "+(i+1), r.nextInt(1111111), r.nextInt(10000)+12000, workers);
		}
		CTO cto = new CTO("Cto", 123456789, 28000, managers);
		cto.print();
	}
}
