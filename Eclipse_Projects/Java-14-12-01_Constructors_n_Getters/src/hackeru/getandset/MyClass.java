package hackeru.getandset;

public class MyClass {

	public static void main(String[] args) {

		Person p1 = new Person("Izik","Algrisi");
		p1.setAge(-200);
		//p1.name  //ERROR - p1.name is private
		
		
		
		
		
		//1st way - ERROR
//		if (p1.name.equals("Izik"))
//		{
//			System.out.println("Hello Izik");
//		}
		//Won't work because p1.name is private!
		
		
		//2nd way
		System.out.println("Hello " + p1.getName());
		System.out.println(p1.getName() + " age is " + p1.getAge());
	}

}
