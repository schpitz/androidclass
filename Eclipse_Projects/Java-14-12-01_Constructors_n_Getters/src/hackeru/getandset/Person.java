package hackeru.getandset;

public class Person {

	private String name;		//Private
	private String last;		//Private
	private int age;			//automatically 0		
	
	
	//Constructor <-> ctrl+space
	//Set "name" and "last" the private variable with external values
	public Person(String n, String l) {
		name = n;
		last = l;
	
	}
	
	//Return a private var to other classes (read, not change)
	public String getName()
	{
		return name;
	}
	
	
	//Set "age" which is also a private variable
	//Filter the age, change it if 
	public void setAge(int x) {
		if (x<130 && x>0)
		this.age = x;
	}
	
	//Get "age"
	public int getAge(){
		return age;
	}
	
}
