package hackeru.getandset;

public class Color {

	//STATIC
	static int test1 = 0;
	//static => can be only once, and everyone can get and set it
	//static => it's everyone's variable now
	//To change it:
	//Color.test1 = 5;
	
	//To static function have no access to normal variables, only to internal or static variables
	
	
	private int red, green, blue;
	private int counterRed = 0;

	// generate get/set functions (alt+shift+'s'+'r')
	// Format (ctrl+shift+f)

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		if (red >= 0 && red <= 255) {
			this.red = red;
			counterRed++;		//count only if RED had updated (inside the 'if')
		}

	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		if (green <= 255 && green >= 0)
			this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		if (blue <= 255 && blue >= 0)
			this.blue = blue;
	}

	public int getCounterRed() {
		return counterRed;
	}

	@Override
	public String toString() {
		return "Color [red=" + red + ", green=" + green + ", blue=" + blue
				+ "]";
	}

}
