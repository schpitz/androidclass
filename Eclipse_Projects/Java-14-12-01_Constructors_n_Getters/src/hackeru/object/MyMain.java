package hackeru.object;

public class MyMain {

	public static void main(String[] args) {
		
		Person p;		//new variable of Person
		//System.out.println(p);		//will print p.toString()
		
		p = new Person();		//initialize new person into 'p'
		p.name = "Lior R.";
		p.year = 1938;
		
		System.out.println(p);
		
		
		//new person
		Person p2 = new Person();
		p2.name = "Anat";
		p2.year = 1990;
		System.out.println(p2);
		
		System.out.println(p.name + " age: " + p.getAge(2014));
		System.out.println(p2.name + " age: " + p2.getAge(2014));
	}
	
}
