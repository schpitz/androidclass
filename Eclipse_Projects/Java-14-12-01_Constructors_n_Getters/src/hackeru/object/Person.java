package hackeru.object;

public class Person {

	String name;			//Person's name
	int year;				//Person's Birth year
	
	//Function gets current year and return the person's age
	int getAge(int now)
	{
		return (now-year);		//age = current year - birth year
	}
	
	
	@Override
	public String toString() {
		return "name=" + name + ", year=" + year;
	}
	
	
	
}
