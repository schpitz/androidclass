package hackeru.constructor;

import java.util.ArrayList;
import java.util.Scanner;

public class MyMain {


	
	static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) {
		
		//Person p1 = new Person; 	//ERROR -> Overriding the default constructor
		Person p1 = new Person("Lior","Rosenspitz");
		System.out.println(p1);
		
		
		//Objects and Arrays
		//Person[] persons = new Person[3];	//Array of person objects
		ArrayList<Person> persons = new ArrayList<>();
				
		//Add persons to the array
		persons.add(p1);
		for (int i=0; i<2; i++)
		{
			System.out.println("Enter first name:");
			String name = scan.nextLine();
			System.out.println("Enter last name:");
			String lastName = scan.nextLine();
			
			Person temp = new Person(name,lastName);
			persons.add(temp);
		}
		
		
		//Print the array
		for (int i=0; i<persons.size(); i++){
			System.out.println(persons.get(i));
		}
		
		
		
	}
}
