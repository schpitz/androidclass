package hackeru.constructor;

public class Person {

	String name,last;		//new NULL string
	int year = 1900;		//default will be 1900
	
	
	//Constructor
	//CTRL+Space -> default constructor for new objects
	//Let's make it impossible to make a new person without entering a first and last name
	public Person(String n, String l) {
		name = n;
		last = l;	
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", last=" + last + ", year=" + year
				+ "]";
	}
	
	
	
}
