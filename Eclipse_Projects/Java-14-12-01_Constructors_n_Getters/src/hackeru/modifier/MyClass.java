package hackeru.modifier;

import hackeru.test.car;

import java.awt.CardLayout;

public class MyClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Person p = new Person();
		p.age = 100;
		//p.name = "Lior Ro"; 	//ERROR - name is private
		System.out.println("Person: " + p.toString());
		
		
		//CAR object example
		car masda = new car();	//new "car" object (from another package)
		masda.year = 1990;		//changeable only because this var is PUBLIC
		System.out.println("Mazda: " + masda.toString());
		
	}

}
