package hackeru.typeVariable;

public class Person {
	
	String name;
	//static int count;
	private static int count;	//not changeable outside
	
	
	//In order to auto-count with Constructor
	public Person(){
		count++;
	}
	
	//get the count
	public static int getCount(){
		return count;
	}

}
