package hackeru.typeVariable;

public class MyClass {

	int test = 10;
	
	public static void main(String[] args) {
		// because main() is static function, it doesn't need to make =new
		// MyClass()

		

		// Check Person.count
		System.out.println("Person.count = " + Person.getCount());
		// Person.name instance variable
		Person p = new Person();

		p.name = "Person 1"; // change person name

		// Person.count = 9; //change person count
		// now count is private & static

		// check Person.count
		System.out.println("Person.count = " + Person.getCount());

		MyClass mClass = new MyClass();
		System.out.println(mClass.test);

	}

}
