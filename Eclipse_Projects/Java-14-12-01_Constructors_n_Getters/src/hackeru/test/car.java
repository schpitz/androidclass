package hackeru.test;

public class car {
	
	public int year;	//public -> known outside the package
	String model;		//default -> inside the package
	private int speed;	//private -> only inside the class
	
	
	@Override
	public String toString() {
		return "car [year=" + year + ", model=" + model + ", speed=" + speed
				+ "]";
	}
		
	

}
