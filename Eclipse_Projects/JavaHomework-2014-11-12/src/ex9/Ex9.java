package ex9;

public class Ex9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double p = 3.1415;
		
		int		 pInt = (int)p;
		float	 pFloat = (float)p;
		String 	stringP = ""+p;
		byte	byteP = (byte)p;
		
		System.out.println("Original P was: " + p);
		System.out.println("Int version = " + pInt);
		System.out.println("Float version = " + pFloat);
		System.out.println("String version = " + stringP);
		System.out.println("Byte version = " + byteP);
	}

}
