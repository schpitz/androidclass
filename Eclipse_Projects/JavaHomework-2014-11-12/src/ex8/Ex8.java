package ex8;

import java.util.Scanner;

public class Ex8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		
		byte num1;		
		System.out.println("Enter a byte number");
		num1 = scan.nextByte();	
		
		int num2;
		System.out.println("Enter an integer number");
		num2 = scan.nextInt();
		
		int sumInt = num1 + num2;
		System.out.println("Integer summary will be: " + sumInt);
		byte sumByte = (byte)(num1 + num2);
		System.out.println("Byte summary will be: " + sumByte);
		
		
	}

}
