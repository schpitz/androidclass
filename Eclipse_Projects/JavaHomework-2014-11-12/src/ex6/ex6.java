package ex6;

import java.util.Scanner;

public class ex6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//enable input
		Scanner scan = new Scanner(System.in);
		
		//Declare and get a new number
		int num;		
		System.out.println("Enter a number between 100-999");
		num = scan.nextInt();	
		
		//breaking the numbers into letters
		int firstNum,SecondNum,ThirdNum;
		firstNum = num%10;
		System.out.println("1st letter is: " + firstNum);
		SecondNum = ((num%100)/10);
		System.out.println("2nd letter is: " + SecondNum);
		ThirdNum = ((num%1000)/100);
		System.out.println("3nd letter is: " + ThirdNum);
		
		//New number (opposite to the original)
		int newNum = firstNum*100 + SecondNum*10 + ThirdNum;
		System.out.println("New number is: " + newNum);
		
	}

}
