package ex2;

import java.util.Scanner;

public class ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//create the input object "scan"
		Scanner scan = new Scanner(System.in);

		int num1,num2;
		System.out.println("Enter a number");
		num1 = scan.nextInt();
		System.out.println("Enter another number");
		num2 = scan.nextInt();
		System.out.println("The first numer is: " + num1 + " and the second number is: " + num2);
		
		//Switch the numbers
		int numTemp = num1;
		num1 = num2;
		num2 = numTemp;
		System.out.println("The first numer is: " + num1 + " and the second number is: " + num2);
		
		
	}

}
