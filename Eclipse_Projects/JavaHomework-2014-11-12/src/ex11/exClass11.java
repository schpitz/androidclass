package ex11;

import java.util.Scanner;

public class exClass11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner (System.in);
		System.out.println("Please enter a number");
		
		String str;
		str = scan.nextLine();
		
		int xInt = Integer.parseInt(str);
		System.out.println("The number in int: "+ xInt);
		
		double xDouble = Double.parseDouble(str);
		System.out.println("The number in double: "+ xDouble);
		
		byte xByte = Byte.parseByte(str);
		System.out.println("The number in byte: "+ xByte);
		
	}

}
