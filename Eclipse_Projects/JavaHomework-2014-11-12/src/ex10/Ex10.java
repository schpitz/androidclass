package ex10;

import java.util.Scanner;

public class Ex10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Get a number into a string
		Scanner scan = new Scanner(System.in);
		System.out.println("Type a number into a string");
		String number1;
		number1 = scan.nextLine();
		
		int numInt = Integer.parseInt(number1);
		double numD = Double.parseDouble(number1);
		byte numB = Byte.parseByte(number1);
		
		System.out.println("Integer version = " + numInt);
		System.out.println("Double version = " + numD);
		System.out.println("Byte version = " + numB);
		
		
		
	}

}
