package ex7;

import java.util.Scanner;

public class Ex7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//enable input
		Scanner scan = new Scanner(System.in);
		
		//Declare and get a new number
		int myInt;		
		System.out.println("Enter a number larger than 100,000");
		myInt = scan.nextInt();	
		
		
		//Cast to Long
		long myLong = myInt;
		System.out.println("Long version of it will be: " + myLong);
		
		
		//Cast to String
		String myString = ""+myInt+"";		
		//Alternative options
//		String myString = String.valueOf(myInt);
//		String myString = Integer.toString(myInt);
		System.out.println("String version will be: " + myString);
		
		
		//Cast to Short
		short myShort = (short)myInt;
		System.out.println("Short version will be: " + myShort);
		
		//Cast to double
		double myD = (double)myInt;
		System.out.println("Double version will be: " + myD);
		
	}

}
