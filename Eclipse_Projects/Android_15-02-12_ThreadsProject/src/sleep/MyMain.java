package sleep;

public class MyMain {

	public static void main(String[] args) {
		System.out.println("Start program");
		
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(i + " second");
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("test");
		
		Thread t1 = new Thread(){
			public void run() {
				print("t1 ");
			};
		};
		Thread t2 = new Thread(){
			public void run() {
				print("t2");
			}
		};
		
		t1.setPriority(Thread.MAX_PRIORITY);
		t2.setPriority(Thread.MIN_PRIORITY);
		
		t1.start(); t2.start();
		
	}
	
	public static void print(String name){
		for (int i = 0; i < 5000; i++) {
			System.out.println(name + " "+i);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
