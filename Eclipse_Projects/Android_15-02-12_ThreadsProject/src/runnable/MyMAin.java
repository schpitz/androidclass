package runnable;

public class MyMAin {
	
	public static void main(String[] args) {
		
		Thread t1 = new Thread(new MyRun());
		t1.start();
		
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				print("Anonymous runnable ");
			}
		});
		t2.start();

		print("Main thread");
		
		
	}
	
	static class MyRun implements Runnable{

		@Override
		public void run() {
			print("Runnable sub class");
		}
		
	}
	
	public static void print(String name){
		for (int i = 0; i < 1000; i++) {
			System.out.println(name + " "+i);
		}
	}

}
