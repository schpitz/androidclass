package priority;

public class MyMAin {
	
	public static void main(String[] args) {
		
		Thread t1 = new Thread(){
			@Override
			public void run() {
				print("t1 ");
			}
		};
		
		Thread t2 = new Thread(new Runnable(){
			@Override
			public void run() {
				print("t2 ");
			}
		});
		
		t1.setPriority(Thread.MAX_PRIORITY);
		t2.setPriority(Thread.MIN_PRIORITY);
		
		t1.start();
		t2.start();
		
	}
	
	
	
	public static void print(String name){
		for (int i = 0; i < 1000; i++) {
			System.out.println(name + " "+i);
		}
	}


}
