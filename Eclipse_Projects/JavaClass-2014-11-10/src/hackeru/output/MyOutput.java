package hackeru.output;

import java.util.Scanner; //using a scanner system utility

public class MyOutput {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		//create the input object "scan"
		String str1 = "Enter Your Name", str2;
		// Create 2 strings variables, but only one has content
		
		System.out.println(str1);
		
		str2 = scan.nextLine();	//Print a line of string
//		str2 = scan.next();		//Print only the first word
		//whatever the user will enter after "next()" function, will be saved into "srt"
		//the content inside "next()" will be saved as a string

		System.out.println("Hello " + str2);		
		
	}
	
}



// Type F11 to RUN