package variables;

import java.util.Scanner;

public class MyClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Byte type
		byte b = 127;
		// b=128 is error; byte maximum is 127
		
		b = Byte.MIN_VALUE;
		System.out.println("Byte min value is " + b);
		b = Byte.MAX_VALUE;
		System.out.println("Byte max value is " + b);
		
		byte input;
		//System.out.println(input); //can't use undefined variable
		System.out.println("Enter a number between -128 to +127");
		
		
		//Most important system function for getting inputs
		Scanner scan = new Scanner(System.in); //load input from system library
				
		input = scan.nextByte();				//use "scan object for input a byte type
		System.out.println("Your input is: " + input);
		
		
		//Short Type
		short maxS = Short.MAX_VALUE;
		short minS = Short.MIN_VALUE;
		System.out.println("Short max is: " + maxS + ", Short min value is: " + minS);
		
		System.out.println("Enter a short number");
		short sInput = scan.nextShort();			//Input of  a Short type
		System.out.println("sInput = " + sInput);	//print sInput
	
	}

}
