package hackeru.whileloop;

import java.util.Scanner;

public class MyLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		
		String name = "";
		//empty string
		
		//While loop - check a char in the string and print its index position (UNCODE table)
		while (true){
			System.out.println("Enter name");
			name = scan.nextLine();
			
			//if (name.length()>0)
			//check if the name string has more than 0 characters
			
			//alternative
			if (!name.isEmpty())	//check if the string is NOT(!) empty
			{
				System.out.println((int)name.charAt(name.length()-1));	
				//print the location of the first char of the string (from the UNICODE table)
				
			}
			
			
		}
		
	}

}
