package hackeru.dowhile;

import java.util.Scanner;

public class Myclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String userPass = "";
		String password = "abc123";
		int counter = 1;
		
		Scanner scan = new Scanner (System.in);
		
		
		//Loop while password is not correct
		//or when the counter is larger than 3
		do{
			System.out.println("Enter password");
			userPass = scan.nextLine();
			counter++;
				
		} while(!(userPass.equals(password)) && counter<=3);
		//Compare strings with string1.equal(string2)
		//Won't work with if(string1==string2)
		
		if (userPass.equals(password))
			System.out.println("Hello");
		else
			System.out.println("You are blocked. Please contact your bank, goodluck!");

	}

}
