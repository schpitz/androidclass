package hackeru.accessories;

import java.util.Scanner;

public class MyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter age");
		int age = scan.nextInt();
		//clean buffer
		scan.nextLine();
		
		System.out.println("Enter your name");
		String name = scan.nextLine();
		
		System.out.println("Hi " + name + ",");
		//In order to print it, otherwise there will be memory leaks
		
		
		
		//Math//
		
		//Math.random -> a number between 0 - 1
		double random1 = Math.random();
		//Remember the Stone-Scissors-Paper game... 
		
		
		//create a new int number between 0 to 10
		int dec = (int)(random1*10);
		//the random will always be 0.x, so int might delete whatever beyond the "."
		
		//Create a new variable of full Pie (3.1415....)
		double pi = Math.PI;
		//there's also Sinus, Tanges, and other known mathematical symbols
		
		//Erech-Muchlat | Absolute Value - The distance from 0 (abs(-5)->5)
		int abs = Math.abs(-90);
		
		//Square (example: Math.sqrt(9)->3;
		Math.sqrt(9);
		

	}

}
