package ex2;

import java.util.Scanner;

public class exClass2 {
	
	public static void main(String[] args) {
		
		//New scanner object
		Scanner scan = new Scanner(System.in);
		
		//new array of int (length of 5)
		int[] numbers = new int[5];
		
		double avg = 0;	//final score (sum than average)
		
		for (int i=0; i<numbers.length; i++) {
			System.out.println("Enter a number");
			numbers[i] = scan.nextInt();
			
			//add the number into avg
			avg = (avg + numbers[i]);
		}
		
		//Calculate the avarage
		//avg = avg/5;
		avg = avg/numbers.length;
		
		System.out.println("The avarage of the array" + avg);
		
		
	}

}
