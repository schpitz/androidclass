package exCPU;

public abstract class CPU {
	
	private String model;
	private int price;
	
	public CPU(String m , int p) {
		this.model = m;
		this.price = p;
	}

	public int getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return "CPU " + model + " - " + price + "NIS";
	}
	
	

}
