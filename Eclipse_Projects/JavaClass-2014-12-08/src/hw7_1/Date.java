package hw7_1;

public class Date {

	private int day;
	private int month;
	private int year;

	// Getters & Setters
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day < 1 || day > 31) {
			this.day = 1;
			System.out.println("Error with DAY");
		} else
			this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month < 1 || month > 12) {
			this.month = 1;
			System.out.println("Error with MONTH");
		} else
			this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	
	@Override
	public String toString() {
		return "Date: " + day + "/" + month + "/" + year;
	}

}
