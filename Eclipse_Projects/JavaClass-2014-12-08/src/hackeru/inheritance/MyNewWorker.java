package hackeru.inheritance;

//extends!!!!
public class MyNewWorker extends Person{
	
	int salary;

	@Override
	public String toString() {
		//"this" has salary, but also name (because it's an expansion of Person)
		return (this.name + " " + this.salary);
	}
	
	

}
