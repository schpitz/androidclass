package hackeru.inheritance;

public class Worker {
	
	int salary;
	
	//Old style
	Person p= new Person();
	
	
	@Override
	public String toString() {
		return "name: " +  p.name + ", salary: " + salary;
	}
	
	

}
