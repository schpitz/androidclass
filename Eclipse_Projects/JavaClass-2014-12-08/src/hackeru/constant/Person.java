package hackeru.constant;

public class Person {
	
	String name;
	static int counter;
	//final int personCount;	//error-because it cannot be initilaized later!
	final int personCount;		//We can set/change the personCount only from the constructor (when object is created)
	final static String president = "PUTIN";
	
	
	//personCount is set only within the constructor!
	public Person() {
		//Make a unique "personCount" for every new person, based on the static counter
		personCount = ++counter;	
	}
	
	public Person(String name){
		this();		//call the default constructor
		this.name = name;
		//we still need to initialize "personCount", that's why we call the default constructor	
	}
	
	public Person(String nameX, int personCountX)
	{
		this.name = nameX;
		this.personCount = personCountX;
	}

	
	@Override
	public String toString() {
		return "Person [name=" + name + ", personCount=" + personCount + "]";
	}
	
	
	
	
	
}
