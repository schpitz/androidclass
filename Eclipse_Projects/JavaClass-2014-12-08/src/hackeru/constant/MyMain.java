package hackeru.constant;

public class MyMain {

	static int test2 = 30;
	
	public static void main(String[] args) {

		int test1 = 10;
		
		System.out.println(Person.president);
		//test
		Person p1 = new Person("IZIK", 777);
		System.out.println(p1);
		//Another test 
		Person p2 = new Person("Betty"); // using a different constructor
		System.out.println(p2);
		
		//Changing the object name
		System.out.println("Before: name = " + p1.name);
		changeName(p1);
		System.out.println("After: name = " + p1.name);

		
		//"test" variable won't be changed, because it only happens inside the function
		System.out.println();
		System.out.println("Before: test = " + test1);
		changeInt(test1);
		System.out.println("After: test = " + test1);
		
		//test2 - with static int (outside functions)
		System.out.println();
		System.out.println("Before: test = " + test2);
		changeInt(test2);
		System.out.println("After: test = " + test2);
		
	}

	static void changeInt(int i) {
		//i = i + 10;
		test2 = i+10;
	}
	
	static void changeName(Person personX)
	{
		personX.name = "Change Value!";
	}

}
