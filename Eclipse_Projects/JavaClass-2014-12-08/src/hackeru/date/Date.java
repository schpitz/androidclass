package hackeru.date;

public class Date {
	
	private int day;
	private int month;
	private int year;
	
	//default constructor (the same as blank)
	public Date() {
		this.day = 0;
		this.month = 0;
		this.year = 0;
	}
	
	public Date(int dayX, int monthX, int yearX)
	{
		this.day = dayX;
		this.month = monthX;
		this.year = yearX;
	}

	@Override
	public String toString() {
		return (day + "/" + month + "/" + year);
	}

	
	//Getters only!
	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}
	
	
	
	

}
