package hackeru.date;

public class Person {

	String name;
	Date d;
	String test;
	int i;

	//Constructor of string+date
	public Person(String name1, Date d1) {
		this.name = name1;
		this.d = d1;
	}

	//Constructor of string only
	public Person(String name1) {
		this.name = name1;		//get name
		this.d = new Date();	//get new date
	}

	//Default constructor -> call constructor of string (with empty string)
	public Person() {
		//this.name = ""; // call constructor Person(String name)
		this("");
	}

	//Constructor that gets a name 
	public Person(String name1, int day, int month, int year) {
		this.name = name1;
		Date d1 = new Date(day,month,year);
		this.d = d1;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", d=" + d + "]";
	}
	
	
	//Check if a person is older than another person
	public boolean isOlder(Person otherP)
	{
		Date temp = otherP.d;
		
		//Check years, then months, than days
		//Check if years are equal
		if (d.getYear() != temp.getYear())
		{
			return (d.getYear()<temp.getYear());	//if years are not equal -> check if current.year is larger the d.year
		} //check if months are equal
		else if (d.getMonth() != temp.getMonth())
		{
			return (d.getMonth()<temp.getMonth());
		}	
		else //check if days are eual
		{
			return (d.getDay()<temp.getDay());
		}
		
	}
	
	
	

}
