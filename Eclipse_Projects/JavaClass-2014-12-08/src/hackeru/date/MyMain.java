package hackeru.date;

public class MyMain {

	public static void main(String[] args) {


		Person p1 = new Person("HackerU",9,10,2000);
		System.out.println(p1);
		
		Date d1 = new Date (4,12,2000);
		Person p2 = new Person ("Liran", d1);	//Using different constructor
		System.out.println(p2);
		
		Person p3 = new Person();
		System.out.println(p3);
		
		System.out.println(p1.isOlder(p2));
		System.out.println(p2.isOlder(p1));

	}

}
