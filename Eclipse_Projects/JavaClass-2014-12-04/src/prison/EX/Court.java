package prison.EX;

import java.util.ArrayList;

public class Court {
	
	static int currentYear = 1990;
	
	//Prisoner[] PrisonerList = new Prisoner[300];
	static ArrayList<Prisoner> PrisonerList = new ArrayList<>();
	//List of potential names
	static String[] NamesList = {"Lior","Betty","Rockstar","Yoeal","Noa","Michael","Becky","Moshe","Yossi"};
	
	
	static void NewTrial(){
		Prisoner p1 = new Prisoner();
		Prisoner p2 = new Prisoner();
		PrisonerList.add(p1);
		PrisonerList.add(p2);
		Judge.Judging(p1, p2);
	}
	
	
	public static void main(String[] args) {

		for (int i = 0; i < 150; i++) {	
			NewTrial();		//Call a new trial
			if (i%20==0)	//Increase year every 20 sentences
			{
				currentYear++;
			}
		}
		
		for (int i = 0; i < 30; i++) {
			Prisoner TempPrisoner = new Prisoner();
			TempPrisoner = PrisonerList.get(Prisoner.rand.nextInt(300));
			
			System.out.println(TempPrisoner);
		}
		
		
	}
	
}
