package prison.EX;

import java.util.Random;

public class Prisoner {

	String name;		//The name of the prisoner
	int JailYears = 0; // Years sentenced for this prisoner
	int JailStartYear; // Year starting his sentence
	int YearAlreadyInJail; // Years in jail
	int YearsLeft; // Years left for sentence
	boolean admit; // Has this prisoner confessed?!

	static Random rand = new Random(); // Use random functions

	
/*	// GET BACK TO IT!
	public int getYearAlreadyInJail() {
		return YearAlreadyInJail;
	}
*/
	
	
	// Return a random boolean
	public boolean isAdmit() {
		return (rand.nextBoolean());
	}

	

	public Prisoner() {
		this.admit = isAdmit();
		this.name = Court.NamesList[rand.nextInt(8)];
	}


	
	//To String
	@Override
	public String toString() {
		return "Prisoner [name=" + name + ", JailYears=" + JailYears
				+ ", JailStartYear=" + JailStartYear + ", YearAlreadyInJail="
				+ YearAlreadyInJail + ", YearsLeft=" + YearsLeft + ", admit="
				+ admit + "]";
	}
	
	

}
