package prison.EX;

public class Judge {

	// Get 2 prisoners and judge them
	static void Judging(Prisoner X, Prisoner Y) {

		// If both prisoners admit
		if (X.admit && Y.admit) {
			X.JailYears = 3;
			Y.JailYears = 3;
			
		// If both don't admit
		} else if (!X.admit && !Y.admit) {
			X.JailYears = 7;
			Y.JailYears = 7;
			
		// If the 1st admit, 2nd isn't
		} else if (X.admit && !Y.admit) {
			X.JailYears = 1;
			Y.JailYears = 10;
	
		// If the 1st doesn't admit, 2nd yes admit
		} else if (!X.admit && Y.admit) {
			X.JailYears = 10;
			Y.JailYears = 1;
		}

	}

}
