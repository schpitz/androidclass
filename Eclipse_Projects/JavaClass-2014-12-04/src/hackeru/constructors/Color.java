package hackeru.constructors;

public class Color {

	int R = 100,G,B;
	
	public Color(int r, int g, int b) {
		R = r;
		G = g;
		B = b;
	}
	
	// Use "this" for using a constructor inside a constructor
	public Color(int colors) {
		this(colors, colors , colors);
//		R = colors;
//		G = colors;
//		B = 100;
	}
	
	
	public Color() {
		this(255, 255, 255);
	}
	
	
	//Get/Set
	public void setR(int R) {
		this.R = R;
	}
	
	public void setB(int b) {
		this.B = b;
	}
	
	
	
	
	
	
	
	
	@Override
	public String toString() {
		return "Color [R=" + R + ", G=" + G + ", B=" + B + "]";
	}

	public static void main(String[] args) {
//		Color color1 = new Color(); 
		Color c = new Color(100, 50, 0);
		System.out.println(c); 		//Test1
		
		Color c1 = new Color();
		System.out.println(c1); 	//Test2
		
		//Changing only part of the parameters
		c.setR(11);
		c1.setB(200);
		System.out.println(c); 		//Test3
		System.out.println(c1); 	//Test4
	}

}
