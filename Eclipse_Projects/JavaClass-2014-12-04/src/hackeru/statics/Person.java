package hackeru.statics;

public class Person {

	String name;			//Local object
	static String king;		//static object
	int id;					//Local ID
	private static int count;	//Static private counter
	int personCount;		//Local person counter
	
	
	//Add to the constructor count=count+1
	public Person() {
		count++;
	}
	
	//Get private variable "count"
	public static int getCount() {
		return count;
	}
	
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", id=" + id + "]";
	}
	
}
