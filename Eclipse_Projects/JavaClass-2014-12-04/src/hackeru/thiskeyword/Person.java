package hackeru.thiskeyword;

public class Person {

	private String name;
	private String last;

	//constructor
	public Person(String name, String last) {
		this.name = name;
		this.last = last;
	}

	//getters and setters
	public String getFullName(){
		return name + " " + last;
	}
	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getName() {
		return name;
	}
	
	public void printPerson(){
		//System.out.println(this);
		MyMain.println(this);
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", last=" + last + "]";
	}
	
	
	
	

}
