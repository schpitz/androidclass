package hackeru.overloading;

public class Overload {

	public static void main(String[] args) {

		//tests
		fun(5);			//test 1st function
		fun(10,5);		//test 2nd function
		fun("5");		//test 3rd function
	
		System.out.println();//break test
		fun(127);			//byte or int?
		
		//Long
		long l = 127;		//create a LONG function
		fun(l);				//call the long function
		fun(127L);

		//Byte
		System.out.println();
		byte b = 127;		//create a BYTE value
		fun(b);				//call the byte function
		fun((byte)127);		//call the byte function
	}
	
	//Different FUNCTIONS may have the same name, but with different signatures
	
	//1st function
	static void fun(int i){
		System.out.println("fun(int) called!");
	}
	
	//4th function
	static void fun(long l){
		System.out.println("fun(long) called!");
	}
	
	//5th function
	static void fun(byte b){
		System.out.println("fun(byte) called!");
	}
	
	
	
	//2st function
	static void fun(int i, int x){
		System.out.println("fun(int,in) called)");
	}
	
	//3rd function
	static void fun(String str){
		System.out.println("fun(String) called");
	}
	

}
