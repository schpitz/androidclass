package hackeru.objects;

public class MyClass {

	//Class with the function MAIN
	public static void main(String[] args) {

		
		//type name=value
		int i=5;		
		
		
		//Create a new object, named "izik", type of Person
		Person izik = new Person();
		izik.name = "Lior";
		izik.last = "Rosenspitz";
		izik.age = i + 24;		//(i=5)+24
		
		
		Person ilya = new Person();	//create another object of type Person
		ilya.name = "ilya";	

				
		//Print check
		System.out.println(izik);		//will print: hackeru.objects.Person@502a3135
		System.out.println(izik.name); 	//will print first name
		
		
		//Test with full object
		System.out.println(izik.name + " " + izik.last + " age=" + izik.age);		
		//prints: Lior Rosenspitz age=29
		
		
		//Test with semi-filled object
		System.out.println(ilya.name + " " + ilya.last + " age=" + ilya.age);
		//prints: ilya null age=0
		ilya.last = "Grishman";			//adds new content
		ilya.age = 24;					//adds new content
		System.out.println(ilya.name + " " + ilya.last + " age=" + ilya.age);
		//prints: ilya Grishman age=24


		//Declare a new variable with the name p (of type Person)
		Person p;
		p = new Person();		//now 'p' is a new Person (it can only be a new Person or an existing one);
	}

}
