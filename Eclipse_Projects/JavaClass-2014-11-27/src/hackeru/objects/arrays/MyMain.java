package hackeru.objects.arrays;

import java.util.Scanner;

public class MyMain {

	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Person p;
		p = new Person();
		System.out.println(p.name + " " + p.age);
		
		//Array of Persons
		Person[] array1 = new Person[3];	//create an array of 3 cells of Person type (just declared)
		for (int i=0; i<array1.length; i++){
			System.out.println(array1[i]);
		}		
		//prints "null null null"
		
		
		array1[0] = p;		//p->array1[0]
		for (int i=0; i<array1.length; i++){
			System.out.println(array1[i]);
		}
		//prints "hackeru.objects.arrays.Person@626f144 null null"
		
		
		System.out.println();
		//Let's fill up the array and objects
		for (int i = 1; i < array1.length; i++) {
			System.out.println("Enter first name");
			String name = scan.nextLine();
			System.out.println("Enter Age:");
			int age = scan.nextInt();
			scan.nextLine();	//Clean buffer
			
			array1[i] = new Person();	//Create a new Person into the current cell
			array1[i].name = name;
			array1[i].age = age;
		}
		
		//Print the array of Objects
		for (int i = 0; i < array1.length; i++) {
			System.out.println(array1[i].name + " " + array1[i].age);
		}
		
	}

}
