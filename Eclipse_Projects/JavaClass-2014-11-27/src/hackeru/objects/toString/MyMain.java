package hackeru.objects.toString;

public class MyMain {

	public static void main(String[] args) {

		Car car1 = new Car();
		car1.model = "Mazda";
		car1.color = "black";
		car1.year = 1800;
		
		System.out.println(car1); 			 	//same thing
		System.out.println(car1.toString());	//same thing 
		
		//with default .toString()
		//hackeru.objects.toString.Car@3b7c680
		//With manual change of .toString()
		//Mazda black 1800
	}

}
