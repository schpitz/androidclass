package hackeru.objects.toString;

public class Car {

	String model;
	int year;
	String color;

	
	
	// tos+ctrl+space+enter
//	@Override					//Default
//	public String toString() {
//		return super.toString();	
//	}
	
//	@Override					//Manual change
//	public String toString() {
//		return (model+"\n"+color+" "+year);
//	}
	
	//Auto-generate toString() -> alt+shift+s+s
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + ", color=" + color
				+ "]";
	}
	
}
