package hackeru.arraylists;

import java.util.ArrayList;
import java.util.Scanner;

public class MyClass {

	static ArrayList<String> arr = new ArrayList<>();
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter players count");		
		int count = scan.nextInt();	
		scan.nextLine();	//clean the buffer
		
		//Get the "count" variable, and call the function to add new player for "count" times
		for (int i = 0; i < count; i++) {
			addName(i);
		}
		
		//Print the team
		for (int i = 0; i <arr.size(); i++) {
			System.out.print(arr.get(i) + " ");
		}
	}
	
	
	static void addName(int playerNumber){
		System.out.println("Enter name for player" + playerNumber);
		arr.add(scan.nextLine());
		
	}

}
