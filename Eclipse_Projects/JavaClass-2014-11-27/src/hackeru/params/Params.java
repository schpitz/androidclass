package hackeru.params;

public class Params {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] array = {1,7,4};
		printArray(array);			//call the function which gets array
		
		printParameters(1,2,3,45,1);		//call the function with int...
		
	}
	
	//Function gets parameters (unknown amount)
	//It automatically creates an array from "i"
	//The compiler create the array
	static void printParameters(int... i){
		//check the created array
		if(i.length>1)
			System.out.println(i[0]);
		else
			System.out.println(i.length);
	}
	
	//function gets 2 arrays
	static void printParameters(int... i, String... str){
		
	}

	
	//Function gets ARRAY
	//The developer create the array
	static void printArray(int[] i){
	}

}
