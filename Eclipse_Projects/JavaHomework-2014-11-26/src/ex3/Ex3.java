package ex3;

public class Ex3 {

	
	public static void main(String[] args) {
		
		int[] array1 = {10, 80, 50 , 70, 9};
		int[] array2 = {100, 90, 80, 70, 60, 50};

		System.out.println(findBiggestNumber(array1));			//Find largest number
		System.out.println(findBiggestNumber(array2));
		System.out.println();									//Space
		System.out.println(findLargestPosition(array1));		//Find largest number position
		System.out.println(findLargestPosition(array2));

		
	}
	
	static int findBiggestNumber(int[] arr)
	{
		int x=arr[0];					//reset x to the first value
		for (int i=1; i<arr.length; i++)
		{
			if (x<arr[i])				//check if the current value is larger than x
				x=arr[i];				//if so -> HASAMA (save it into x)
		}
		return x;
	}
	
	static int findLargestPosition(int[] arr)
	{
		int largestX=findBiggestNumber(arr);
		int i = 0;
		for (int j=0; j<arr.length; j++)
		{
			if (arr[j]==largestX)
			{
				i=j;
				return i;
			}
		}
		
		return i;
		
	}

}
