package ex1;

import java.util.ArrayList;

public class Ex1 {

	//static ArrayList<Integer> intArray = new ArrayList<>();
	
	//Alternate way to set up an array
	static int[] intArray = {1,7,9,90,2,-5};
	
	
	public static void main(String[] args) {
		printList(intArray);				//call the function

	}
	
	
	static void printList (int[] arr){
		for (int i=0; i<arr.length; i++)
		{
			System.out.print(arr[i]);		//print the number
			if (i==(arr.length-1))			//check if to print "," (not in the last letter)
				return;
			else
				System.out.print(" , ");
		}
	}

}
