package ex2;

public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("1st Array");
		int[] testInt = getIntArray(10);
		System.out.println(testInt[5]);
		for (int i=0; i<testInt.length; i++)			//Print array of "0"
			System.out.println(testInt[i]);
		
		System.out.println();
		System.out.println("2nd Array");
		int[] testInt2 = getIntArray(150,false);
		for (int i=0; i<testInt2.length; i++)
			System.out.println(testInt2[i]);			//Print array (of odd/even numbers)
		
	}
	
	
	static int[] getIntArray (int i)
	{
		if (i<0 || i>1000)							//filter
			return null;
		
		int[] newArray = new int[i];		//on new array all cells = 0
		return newArray;
	}
	
	
	static int[] getIntArray(int x ,  boolean isTrue)
	{
		if (x<0 || x>1000)
			return null;
		
		if (x>100){
			int[] newArray = new int[x];
			return newArray;
		}
		
		if (isTrue){
			int[] newArray = new int[x];
			newArray[0] = 2;					//reset the first number to even
			for (int i=1; i<x; i++){
				newArray[i]=newArray[i-1]+2;	//add 2 to the last number
			}
			return newArray;
		} else {
			int[] newArray = new int[x];
			newArray[0] = 1;					//reset the first number to odd
			for (int i=1; i<x; i++){
				newArray[i]=newArray[i-1]+2;	//always add to the last number
			}
			return newArray;		
		}
		
	}

}
