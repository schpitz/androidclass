package ex4;

import java.util.Scanner;

public class Ex4 {

	/**
	 * @param args
	 */
	static Scanner scan = new Scanner(System.in);

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arrCheck = makeArr();
		for (int i=0; i<arrCheck.length; i++)
		{
			System.out.println(i + " - "+ arrCheck[i]);
		}
		
	}
	
	static int[] makeArr()
	{
		int[] arr = new int[10];
		
		for (int i=0; i<arr.length; i++)
		{
			System.out.println((i+1 + "Enter a new number"));
			int tempX = scan.nextInt();
			//scan.nextLine();

			
			while (tempX>10)
			{
				System.out.println("The number is larger than 10. Enter another");
				tempX = scan.nextInt();
				scan.nextLine();
			}
			
			arr[i] = tempX;
		}
		
		return arr;
	}

}
