package serializable;

import java.io.Serializable;

public class Point {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int x, y;

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
	

}
