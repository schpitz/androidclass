package hackeru.examp;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class MyClass {
	
	public static void main(String[] args) {
		
		try {
			OutputStream outputStream = new FileOutputStream("C:\\Users\\hackeru.HACKERU3\\Desktop\\writer.txt");
			
			Writer w = new OutputStreamWriter(outputStream, "UTF-8");
		
			w.write("abcdef,אב");
			w.close();
			
			FileInputStream fis = new FileInputStream("C:\\Users\\hackeru.HACKERU3\\Desktop\\writer.txt");
			Reader reader = new InputStreamReader(fis, "UTF-8");
			int counter = 0 ;
			char[] array = new char[2];
			while((counter = reader.read(array)) != -1){
				System.out.println(new String(array, 0, counter));
			}
			reader.close();
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
