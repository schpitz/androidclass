package simple;

import java.io.*;

public class MyClass {
	
	public static void main(String[] args) {
		//File desktop = new File("C:/Users/hackeru.HACKERU3/Desktop");
		
		File hackeru = new File("C:/Users/hackeru.HACKERU3/Desktop/hackeru" );
		if(!hackeru.exists())
			hackeru.mkdir();
		File hackeru2 = new File("C:/Users/hackeru.HACKERU3/Desktop/hackeru", "hackery file.txt");
		if(!hackeru2.exists())
			try {
				hackeru2.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		File hackeru3 = new File(hackeru, "hackeru file 2.txt");
		try {
			System.out.println(hackeru3.createNewFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		System.out.println("hackeru3 parent is: "+hackeru3.getParentFile().getName());
	}

}
