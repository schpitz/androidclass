package hackeru.superclass2;

public class Sub extends Base{
	
	//if Base object has default constructors -> OK
	//if Base object doesn't have default constructor -> Create it here
	
	
	static String defString = "My String";	//won't work without static -> which will be created regardless to constructors
	
	public Sub(){
		super(defString);
		//defString is initialized after the constructors are called by the CPU
	}

}
