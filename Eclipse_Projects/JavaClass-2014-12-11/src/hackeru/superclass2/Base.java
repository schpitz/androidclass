package hackeru.superclass2;

public class Base {

	//Default constructor
	public Base() {
	}
	
	//Manual constructor
	public Base(String str)
	{
		System.out.println(str);
	}
	
	

}
