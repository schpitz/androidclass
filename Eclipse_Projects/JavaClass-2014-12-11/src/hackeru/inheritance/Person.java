package hackeru.inheritance;

public class Person {
	
	private String name;
	protected String lastName;
	public static int counter;		//counter is static -> created only once!
	protected int age;
	
	
	//Constructors
	public Person() {
		counter++;	//with every new construtor -> increase "counter" static int
		name = "Person " +  counter;
	}
	
	
	
	//Getters & Setters
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public String getLastName() {
		return lastName;
	}



	@Override	//ToString()
	public String toString() {
		return "Person [name=" + name + ", lastName=" + lastName + ", age="
				+ age + "]";
	}
	
	
	
	
}
