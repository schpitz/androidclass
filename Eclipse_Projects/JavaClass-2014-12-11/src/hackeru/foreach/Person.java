package hackeru.foreach;

public class Person {
	
	//private variable
	private String name;
	
	//Create a new object (type Test)
	Test t = new Test("Person global variable");
	
	
	//2 Constructors
	public Person() {
		System.out.println("Constructor Person");
	}
	
	public Person(String name)
	{
		this.name = name;
		System.out.println("Constructor Person");
	}

	
	
	@Override
	public String toString() {
		return "Person [name=" + name + "]";
	}
}
