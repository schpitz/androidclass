package hackeru.foreach;

import java.util.ArrayList;

public class MyMain {

	public static void main(String[] args) {
		
		String[] arr = {"a","b","c"};	//create array
		PrintArray(arr);				//print array
		
		Test t = new Test("Main Function");
		
		System.out.println();
		System.out.println("Create Person");
		
		Person p1 = new Person();		//default constructor
		Person p2 = new Person("Parameters used");	//Manual constructor

		ArrayList<Person> list = new ArrayList<>();
		list.add(p1);
		list.add(p2);
		list.add(new Person("Person 3"));
		list.add(new Person("Person 4"));
		
		
		//Print array with FOREACH loop
		System.out.println("\n ---Print Persons---"); //SYSO
		for (Person itemX:list)		//foreach loop
		{
			System.out.println(itemX);
		}
		
	}
	
	
	//Print array with Loop and function
	public static void PrintArray(String[] arrX)
	{
		for (int i=0; i<arrX.length; i++)
		{
			String item = arrX[i];
			System.out.println(item);
		}
	}

}
