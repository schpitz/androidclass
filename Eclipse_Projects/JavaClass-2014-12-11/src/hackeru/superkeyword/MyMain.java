package hackeru.superkeyword;

public class MyMain {
	
	public static void main(String[] args) {
		
	//	Person p1 = new Person();	//Won't work without parameters (doesn't have default constructor)
		Person p2 = new Person("p2","Last 1");
		Person p3 = new Person("p3","Last 2");
		System.out.println(p2);
		System.out.println(p3);
		
		//Use Worker() constructor that will use Person() constructor
		Worker w1 = new Worker();
		Worker w2 = new Worker("Lior","Cohen",29);
		System.out.println(w1);
		System.out.println(w2);
	}

}
