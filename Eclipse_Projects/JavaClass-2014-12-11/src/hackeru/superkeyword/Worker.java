package hackeru.superkeyword;

//Worker don't have a default constructor, so we need to create it ourselve
public class Worker extends Person{

	//Must be the 1st thing else it won't work
	public Worker()
	{
		super("Default Worker","d last!");
	}
	//Once we created the father object...


	
	//new constructor (alt+shift+s+o)
	//Use the Person class private/protected vars with SUPER
	public Worker(String name, String last, int age) {
		super(name, last, age);
	}
	public Worker(String name, String last) {
		super(name, last);
	}
	
	
	
	
	
	
	
}
