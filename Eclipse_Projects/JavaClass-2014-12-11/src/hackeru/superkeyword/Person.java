package hackeru.superkeyword;

public class Person {
	
	
	String name;
	String last;
	int age;
	
	
	//Constructors (alt+shift+s+o)
	//Once we create a custom costrutctor, it won't have a default empty one
	public Person(String name, String last, int age) {
		//super();
		this.name = name;
		this.last = last;
		this.age = age;
	}


	public Person(String name, String last) {
		//super();
		this.name = name;
		this.last = last;
	}


	//ToString (alt+shift+s+s)
	@Override
	public String toString() {
		return "Person [First Name: " + name + ", Last name: " + last + ", Age: " + age + "]";
	}
	
	
	
	
	
	

}
