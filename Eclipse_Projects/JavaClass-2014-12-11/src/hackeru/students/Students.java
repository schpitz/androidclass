package hackeru.students;

import hackeru.inheritance.Person;

public class Students extends Person{

	public Students(String last){
		this.lastName = last;
		super.setAge(15);   //SUPER -> call the father class (Person)
	}
	
	
	//Happens only if the "super.setAge()" is not activated
	@Override
	public void setAge(int ageX){
		if(ageX>17 && age<35)
			super.setAge(ageX); //SUPER -> call the father class
	}
}
