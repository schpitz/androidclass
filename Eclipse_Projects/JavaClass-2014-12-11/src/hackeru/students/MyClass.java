package hackeru.students;

import hackeru.inheritance.Person;

public class MyClass {

	public static void main(String[] args) {

		Person p1 = new Person();
//		p.lastName -> lastName is protected value
		p1.setAge(12); 		//Set age of p1 to 12 (it's the only setter we have)
		System.out.println(p1);
		
		Person p2 = new Person();
		System.out.println(p2);
		
		
		//using Super
		//When creating a new student it changes also the protected name
		Students student1 = new Students("HackerU 1");
		student1.setAge(10);		//is not between 17 to 35 -> won't change
		System.out.println(student1);
		
		Students student2 = new Students("HackerU 22");
		student2.setAge(30);
		System.out.println(student2);
		
		
		System.out.println("Person counter is: " + Person.counter);
		
		
	}

}
