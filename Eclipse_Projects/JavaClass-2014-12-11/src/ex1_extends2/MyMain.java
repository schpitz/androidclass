package ex1_extends2;

import ex1.Animal;
import ex1.Cow;
import ex1.Dog;
import ex1_extends1.Cat;

public class MyMain {
	
	public static void main(String[] args) {
		
		Animal animal1 = new Animal();
		Cow animal2 = new Cow();
		Dog animal3 = new Dog();
		Cat animal4 = new Cat();
		
		//Check the new animals
		System.out.println(animal1);
		System.out.println(animal2);
		System.out.println(animal3);
		System.out.println(animal4);
		
		//Set ages
		animal1.setAge(5);
		animal2.setAge(12);
		animal3.setAge(20);
		animal4.setAge(-10);
		
		animal1.move();
		animal2.move();
		animal3.move();
		animal4.move();
		
		System.out.println(animal1);
		System.out.println(animal2);
		System.out.println(animal3);
		System.out.println(animal4);
		
			
		//Dog (animal3) can't access to "title" variable -> because it's private at "Animal"
		
		
	}

}
