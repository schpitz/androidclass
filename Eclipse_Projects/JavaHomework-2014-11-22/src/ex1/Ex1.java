package ex1;

import java.util.Scanner;

public class Ex1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		boolean keepLoop = true;
		
		while (keepLoop)
		{
			//Getting a number from the user
			System.out.println("Please enter a number");
			int z = scan.nextInt();
			
			//Stopping point
			if (z<0)
				break;
			
			//Printing the random function
			System.out.println("A random number: " + getRandom(z));			
		}

		

	}
	
	//Creating a return function
	static int getRandom (int max)
	{
		int x = (int)(Math.random()*max);	//get a random number between 0 and "max"
		return x;							//return the new number
	}

}
