package ex5;

import java.util.Scanner;

public class Ex5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter length and width of the RECTANGLE");
		int x = scan.nextInt();
		int y = scan.nextInt();
		System.out.println("Its area is ==> " + (area(x,y)));
		
		System.out.println("Please enter circle Raduis");
		int rad = scan.nextInt();
		System.out.println("Its area is ===> " + (area(rad)));


	}

	//area of a square (a*b)
	static int area (int a, int b)
	{
		return (a*b);
	}
	
	//area of a circle (2piR)
	static double area(int r)
	{
		return ( 2 * r * (Math.PI));
	}
	
}
