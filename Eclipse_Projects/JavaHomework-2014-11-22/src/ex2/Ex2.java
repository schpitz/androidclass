package ex2;

import java.util.Scanner;

public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your full name:");
		String full = scan.nextLine();
		System.out.println("Now please enter your first name");
		String first = scan.next();
		System.out.println("Last name...");
		String last = scan.next();
		
		System.out.println("Now here are the results");
		PrintFullName1(full);
		PrintFullName2(first,last);
		PrintFullName3(first,last,true);
		PrintFullName3(first,last,false);
	}
	
	static void PrintFullName1 (String fullname)
	{
		System.out.println("The name is: " + fullname);
	}
	
	static void PrintFullName2 (String firstName, String lastName)
	{
		System.out.println(firstName +" "+ lastName);
	}
	
	static void PrintFullName3 (String firstName, String lastName, boolean breaking)
	{
		if (breaking)
		{
			System.out.println(firstName +" "+ lastName);
		} else {
			System.out.println(firstName);
			System.out.println(lastName);
		}
	}

}
