package ex6;

import java.util.Scanner;

public class Ex6 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		boolean keepLoop = true;
		while (keepLoop)
		{
			System.out.println("Please enter an integer number");
			int x123 = scan.nextInt();		//get a new number (into x123)
			int nums[] = summi(x123);		//get the array from the number (via function)
		}		
	}
	
	static int[] summi(int x)
	{
		int[] arr;
		
		if ((x>999) || (x<0))	//if number too low or too high - abort
			return null;
		
		//create new array (by different scenarios)
		if (x<10)
		{
			arr = new int[1];		//0<x<10
			arr[0] = x % 10;
		} else if (x>=100) {
			arr = new int[3];		//100<x<999
			arr[0] = x % 10;
			arr[1] = (x/10)%10;
			arr[2] = x/100;
		} else {
			arr = new int[2];		//10<x<100
			arr[0] = x % 10;
			arr[1] = x / 10;
		}
		
		return arr;			
	}
}
