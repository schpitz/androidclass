package ex2;

public class Worker {
	
	String name;
	int id;
	int salary;
	
	public Worker(String nameX, int idX, int salaryX)
	{
		this.name = nameX;
		this.id = idX;
		this.salary = salaryX;
	}
	
	public void Print()
	{
		System.out.println("Worker's name: " + name + " | Salary: " + salary +"$");
	}
	
}
