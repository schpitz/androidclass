package ex2;

import java.util.Random;

public class MyMain {
	
	static Random rand = new Random();		//create a random 
	static String[] names = {"Lior","Betty","Rocks","Noa","Yoela","Yossi","Shalom","Michael","Becki","Kobi","Alex"};		//potential names array
	static int counter = 0;
	
	public static void main(String[] args) {
		Manager[] managersList = new Manager[4];
		
		for (int j=0; j<managersList.length; j++)
		{
			int WorkerArrayLength = rand.nextInt(10)+1;				//random workers list size
			Worker[] workersArray = new Worker[WorkerArrayLength];	//random workers list size
			
			for (int i=0; i<WorkerArrayLength; i++)
			{
				Worker tempWorker = new Worker(
							names[rand.nextInt(names.length)],	//random name from the array
							rand.nextInt(99999),				//random id
							rand.nextInt(999)					//random salary
							);
				workersArray[i] = tempWorker;
			}
			
			//Manager() constructor -> String name, int salary, int id, Worker[] arr
			managersList[j] = new Manager(("Manager"+(counter+1)),rand.nextInt(9999),rand.nextInt(9999),workersArray);
			counter++;
		}

		//Create a new CEO instance/object
		CEO newCEO = new CEO("Lior the BOSS",731,19875,managersList);

		//Print all data
		newCEO.Print();
		//newCEO.PrintManager();		
	}
}
