package ex2;

public class Manager extends Worker  {

	//Empty array of workers
	Worker[] workersList;;
	
	public Manager(String name, int salary, int id, Worker[] arr)
	{
		super(name,id,salary);		//use the father -> its constructor (with "super")
		this.workersList = arr;
	}
	
	public void Print()
	{
		super.Print();				//user the father -> its Print() function
		System.out.println("Workers beneath : " + workersList.length);
		
		
		//Usint "For" loop
//		for (int i=0; i<workersList.length; i++)
//		{
//			workersList[i].Print();			//use Print() function of every worker type in the array
//		}
		
		System.out.println(); 		//break-line

		//using "Foreach" loop
		for (Worker workerX : workersList)
		{
			workerX.Print();		
		}
		
		System.out.println();  		//adds a break-line
	}

}
