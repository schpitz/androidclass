package ex2;

public class CEO extends Manager{
	
	static Manager[] managersList;			//empty Managers type array
	
	public CEO(String name , int id, int salary, Manager[] managersList)
	{
		super(name, id, salary,managersList);
	}
	
	
// Doesn't need to to be re-written!!!	the normal manager.print() works fine!
//
//		//Print everything beneath it
//	public void Print()
//	{
//		for (Manager x : managersList)
//		{
//			//x.sup.Print();		//print the managers print function (which also print the workers inside)
//			super.Print();
//		}	
//	}
	
	
	//print only the managers' details
	public void PrintManager()
	{
//		for (Manager x : managersList)
//		{
//			System.out.println("Manager: " + x.name + " |Salary: " + x.salary + " |id: " + x.id);
//		}
		
		for (int i=0; i<managersList.length; i++)
		{
			System.out.println("Manager: " + managersList[i].name);
		}
	}

}
