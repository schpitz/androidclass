package ex1;

public class Animal {

	private String title = "base: Animal";
	protected int age;
	
	
	//Filter all ages below 0 + update protected "age"
	public void setAge(int newAge) {
		if (newAge>0)
		{
			this.age = newAge;
		}
	}
	
	//print the word "animal"
	public void move()
	{
		System.out.println("Animal");
	}

	
	@Override
	public String toString() {
		return "Animal [title=" + title + ", age=" + age + "]";
	}
		
	
}
