package ex1_extends1;

import ex1.Animal;
import ex1.Cow;
import ex1.Dog;

public class MyMain2 {
	
	public static void main(String[] args) {
		
		Animal animal1 = new Animal();
		Cow animal2 = new Cow();
		Dog animal3 = new Dog();
		Cat animal4 = new Cat();
		
		animal3.setAge(-30);
		animal4.setAge(-30);
		
		System.out.println(animal3);
		System.out.println(animal4);
	}

}
