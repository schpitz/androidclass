package ex1_extends1;

import ex1.Animal;

public class Cat extends Animal{
	

	public void move(){
		System.out.println("cat is doing mewoooo");
	}
	
	//Making a constructor will overwrite the default limitation of "0"
	public Cat()
	{
		age = -12;
	}

}
