package ex2;

import java.util.Random;

public class Ex2 {

	//New random object
	static Random rand = new Random();
	
	public static void main(String[] args) {
		System.out.println("1st random number (500-1500): " + getRandom());
		System.out.println("2nd random number (1500-2500): " + getRandom(1500,2500));

	}
	
	//Get a random number between 500 and 1500
	static int getRandom(){
		//int x = (int)(Math.random()*1000) + 500;
		int x = rand.nextInt(1000) + 500;
		return x;
	}
	
	static int getRandom(int min, int max){
		int x = rand.nextInt(max-min) + min;
		return x;
		
	}

}
