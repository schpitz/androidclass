package ex1;

import java.util.Scanner;

public class Ex1 {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a sentence");
		String newString = scan.nextLine();
		//scan.nextLine();		//clean buffer
		
		//First function
		print(newString);
		print((newString.toCharArray()),true);
		print((newString.split(" ")));
		print((newString.toCharArray()),false);
		
				
	}
	
		
	
	//Default PRINT
	static void print(String str)
	{
		System.out.println();
		System.out.println("The string: " +  str);
	}
	
	
	//Array PRINT
	static void print(String[] str)
	{
		System.out.println();
		for (int i = 0; i < str.length; i++) {
			System.out.println(str[i]);
		}
	}
	
	
	//Char Array PRINT
	static void print(char[] arr, boolean isTrue)
	{
		System.out.println();
		if (isTrue)
		{
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + ",");
			}
		} else {
			for (int i = 0; i < arr.length; i++) {
				System.out.println(arr[i]);
			}
		}
	}

}
