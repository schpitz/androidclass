package ex4;

public class Student {

	String firstName;
	String lastName;
	String id;			//because it might start with 0 (example '007')
	long privateID;
	
	
	//Change "toString()" function
	@Override
	public String toString() {
		return ("FirstName: " + firstName + ", LastName: " + lastName
				+ ", ID: " + id);
	}
	
	

}
