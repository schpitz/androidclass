package simple;

import java.io.File;
import java.io.IOException;

public class MyClass {
	
	public static void main(String[] args) {
		String path = "C:/Users/hackeru.HACKERU3/Desktop";
		File desktop = new File(path);
		
		if(desktop.exists())
			System.out.println(desktop.getName());
		File newFile = new File(desktop, "t.txt");
		if(newFile.exists())
			System.out.println("file exists");
		else
			newFile.mkdir();
		System.out.println(newFile.mkdir());
		System.out.println("desktop size is "+getSize(desktop)+" MB");
			
	}
	
	static long getSize(File f){
		long size = 0;
		size+=f.length();
		if(f.isDirectory()){
			File[] inners = f.listFiles();
			for (File file : inners) {
				size += file.length();
				if(file.isDirectory())
					size += getSize(file);
			}
		}
			
		return size;
	}

}
