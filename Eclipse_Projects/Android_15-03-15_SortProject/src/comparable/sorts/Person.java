package comparable.sorts;

public class Person implements Comparable<Person>{
	
	int children;
	int age;
	String name;
	
	public Person(int children, int age, String name) {
		this.children = children;
		this.age = age;
		this.name = name;
	}

	@Override
	public String toString() {
		return "[children=" + children + ", age=" + age + ", name="
				+ name + "]";
	}

	@Override
	public int compareTo(Person o) {
		return o.children - this.children;
//		System.out.println("name: "+name+", other name: "+o.name);
//		if(this.age < o.age)
//			return -1;
//		else if(this.age > o.age)
//			return 1;
//		return 0;
	}
	
	
	

}
