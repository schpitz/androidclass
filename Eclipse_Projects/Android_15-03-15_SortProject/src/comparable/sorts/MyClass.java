package comparable.sorts;

import java.util.Arrays;

public class MyClass {
	
	public static void main(String[] args) {
		
		Person[] persons = {new Person(2, 2, "a"),
							new Person(5, 1, "d"),
							new Person(7, 7, "c")};
		System.out.println(Arrays.toString(persons));
		Arrays.sort(persons);
		System.out.println("sort array:");
		System.out.println(Arrays.toString(persons));
		
	}

}
