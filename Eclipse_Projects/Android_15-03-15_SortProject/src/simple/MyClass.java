package simple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MyClass {
	
	public static void main(String[] args) {
		String[] arr1 = {"z", "a", "b", "k"};
		System.out.println(Arrays.toString(arr1));
		//sort
		System.out.println("sort Array");
		Arrays.sort(arr1);
		System.out.println(Arrays.toString(arr1));
		
		System.out.println();
		//ArrayList
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(2);
		System.out.println(list);
		System.out.println("sort list");
		//sort
		Collections.sort(list);
		System.out.println(list);
		
	}

}
