package compare.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class MyClass {
	
	public static void main(String[] args) {
		
		ArrayList<Person> persons = new ArrayList<>(); 
		persons.add(new Person(2, 2, "a"));
		persons.add(new Person(5, 1, "d"));
		persons.add(new Person(7, 7, "c"));
		System.out.println(persons);
		
		System.out.println("sort by a - z");
		Collections.sort(persons, new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				return o2.name.compareTo(o1.name);
			}
		});
		System.out.println(persons);
	}

}
