package compare.arraylist;

public class Person {
	
	int children;
	int age;
	String name;
	
	public Person(int children, int age, String name) {
		this.children = children;
		this.age = age;
		this.name = name;
	}

	@Override
	public String toString() {
		return "[children=" + children + ", age=" + age + ", name="
				+ name + "]";
	}
	

}
