package comparator;

import java.util.Arrays;
import java.util.Comparator;

public class MyClass {
	
	public static void main(String[] args) {
		
		Person[] persons = {new Person(2, 2, "a"),
							new Person(5, 1, "d"),
							new Person(7, 7, "c")};
		
		System.out.println(Arrays.toString(persons));
		//sort age persons
		Arrays.sort(persons, new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				
				return o1.age - o2.age;
			}
		});
		System.out.println("sort by age");
		System.out.println(Arrays.toString(persons));
		
	}

}
