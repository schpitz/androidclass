package ex2;

import java.util.Scanner;

public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);	//Enable input
		int[] numbers = new int[5];		//new array of 5 places
		double sum = 0;					//final summary of all numbers
		
		for (int i=0; i<numbers.length; i++){
			System.out.println((i+1) +". Enter a number");
			numbers[i] = scan.nextInt();
			sum = (sum + (double)numbers[i]);
		}
		
		sum = (sum/numbers.length);
		System.out.println("The avarage of all numbers is: " + sum);
		
		
		
	}

}
