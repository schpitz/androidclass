package ex12;

import java.util.Scanner;

public class Ex12 {



	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		int[][] table = new int[11][11];
		
		//Set values into the table (Multi-Board)
		for (int i=1; i<=10; i++)
		{
			for (int j=1; j<=10; j++)
			{
				table[i][j]=(i*j);
			}
		}
		
		//Print the table
		for (int i=1; i<=10; i++)
		{
			for (int j=1; j<=10; j++)
			{
				System.out.print(table[i][j]);
			}
			System.out.println();
		}
		
		boolean keepLooping = true;
		
		while (keepLooping)
		{
			System.out.println("Please enter index #1");
			int x = scan.nextInt();
			System.out.println("Please enter index #2");
			int y = scan.nextInt();
			
			//Filter
			if ((x>11) || (x<1) || (y<1) || (y>11))
			{
				System.out.println("ERROR, please write other numbers (between 1-10)");
				continue;
			}
			
			System.out.println("The value of array1 index #" +x+ " and array2 index #" +y+ " ===>" + table[x][y]);
		
		}
	}

}
