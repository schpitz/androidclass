package ex10;

public class Ex10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arr = new int[10];
		boolean isUpOrder = true;
		
		for (int i=0; i<arr.length; i++)
		{
			arr[i] = (int)(Math.random()*1000);
			System.out.println("#" +i+ " number is: " + arr[i]);
		}
		
		//Check if the array cells are larger than their previous
		for (int i=1; i<arr.length; i++)		//start check from i=1 and not i=0
		{
			if (arr[i]>arr[i-1])
			{
				System.out.println("The numbers are going up! :)");
				continue;
			} else {
				System.out.println("The numbers are not going up :(");
				break;
			}
		}

	}

}
