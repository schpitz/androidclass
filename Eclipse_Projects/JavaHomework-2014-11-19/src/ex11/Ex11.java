package ex11;

import java.util.Scanner;

public class Ex11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		
		String[] arrNames = new String[5];
		String[] arrLastName = new String[5];
		
		for (int i=0; i<arrNames.length; i++)
		{
			System.out.println((i+1) + ". Please enter the student FIRST name");
			arrNames[i] = scan.nextLine();
			System.out.println((i+1) + ". Please enter the student LAST name");
			arrLastName[i] = scan.nextLine();
		}
		
		for (int i=0; i<arrNames.length; i++)
		{
			System.out.println(arrNames[i] +" "+arrLastName[i]);
		}
		
	}

}
