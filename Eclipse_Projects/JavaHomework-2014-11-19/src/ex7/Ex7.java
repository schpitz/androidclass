package ex7;

import java.util.Scanner;

public class Ex7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char[] myArr = new char[5];
		
		Scanner scan = new Scanner(System.in);
		
		for (int i=0; i<myArr.length; i++)
		{
			System.out.println((i+1) + "//5 - Enter a char");
			myArr[i] = scan.next().charAt(0);
		}
		
		System.out.println("These are all the chars: ");
		for (int i=0; i<myArr.length; i++)
		{
			System.out.print(myArr[i]);
		}
		
	}

}
