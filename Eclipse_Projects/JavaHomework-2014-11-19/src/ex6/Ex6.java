package ex6;

import java.util.Scanner;

public class Ex6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		int[] numbers = new int[5];
		
		for (int i=0; i<numbers.length; i++)
		{
			System.out.println((i+1) + ". Please enter a numbers");
			numbers[i] = scan.nextInt();			
		}
		
		System.out.println("Now the number in the opposite direction:");
		for (int i=numbers.length-1; i>=0; i--)
		{
			System.out.println(numbers[i]);
		}
		
	}

}
