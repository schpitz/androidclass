package hackeru.array1;

public class MyArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] array = new int[5];			//new integer array of 5 places
		byte[] byteArray = new byte[array.length];	//new byte array in the length of the first array
		
		//Print default values of the array
		for (int i=0; i<byteArray.length; i++)
		{
			//print value for index-i
			System.out.println(byteArray[i]);
		}
		//Break Row
		System.out.println();
		//System.out.println("Set Values");
		//Setting values
		array[0]=5;
		array[1] = array[0]*2;
		array[2] = 10;
		array[3] = byteArray[0];	//Hinted Convertion (byte -> int)
		array[4] = 900;
		//array[5] = array[4];		//Error: There's no array[5]. array[4] is the last one
		
		//Copy arrays
		//System.out.println("Copy Array");
		for (int i=0; i<byteArray.length; i++)
		{
			byteArray[i] = (byte)array[i];	//must have (byte) converter
		}
		
		//System.out.println("print arrays");
		//Print arrays
		for (int i=0; i<byteArray.length; i++)
		{
			System.out.println(i + " - array=" +array[i]+ " | byteArray= " + byteArray[i]);
		}
		
		
	}

}
