package hackeru.functions1;

public class Functions1 {

	
	static int count;
	//Possible to create variables in the class, but not functions!
	//The MAIN function is the only one who RUNS other function

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Call function printAdd(a,b)
		printAdd(5,10);
		
		//Call function printMinus(a,b)
		printMinus(6,1);
		
		//Call function returnAdd(a,b)
		System.out.println("Call function returnAdd(a,b)");
		/*
		// 1st way - 2 lines
		int x = returnAdd(2,3);
		System.out.println(x);
		*/
		// 2nd way - 1 line, same thing!
		System.out.println(returnAdd(2,3));
		
		//Call function returnMinus(a,b)
		System.out.println(returnMinus(2,4));
	}
	
	
	//void -> function without return
	static void printAdd(int a, int b)
	{
		System.out.println(a + " + " + b + " = " + (a+b));	//prints: 5 + 10 = 15
//		System.out.println(a + " + " + b + " = " + a+b);	//prints: 5 + 10 = 510
	}
	
	
	//void -> function without return
	static void printMinus(int a, int b)
	{
		if (a>b)				//a	> b
			System.out.println(a-b);
		else if (b>a)			//a < b
			System.out.println(b-a);
		else					//a == b
			System.out.println(":(");
	}
	
	
	//function with RETURN
	static int returnAdd(int a, int b){
		return a+b;
	}
	
	
	//function with RETURN (string for the smily)
	static String returnMinus(int a, int b)
	{
		System.out.println("Return minus!");
		
		if (a>b)				//a>b
			return (""+(a-b));
		else if (a<b)			//a<b
			return (""+(b-a));
		else					//a==b
			return (":^(");		//return a strange string chars
	}
	

}
