package hackeru.simpleFunction;

public class SimpleFunction {

	static char myChar = '*';

	//This is the only function who runs on startup
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		print1();
		
	}
	
	
	static void print1()
	{
		System.out.println(myChar);		//Print 1st line
		print2();						//
		System.out.println(myChar);		//Print 5th line
	}
	
	static void print2()
	{
		System.out.println("" +myChar+myChar);	//Print 2nd line
		print3();								//
		System.out.println("" +myChar+myChar);	//Print 4th line
	}

	static void print3() {
		System.out.println(""+myChar+myChar+myChar);	//Print 3rd line
		
	}


}
