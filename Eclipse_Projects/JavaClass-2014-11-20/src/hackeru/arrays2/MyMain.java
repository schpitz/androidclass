package hackeru.arrays2;

import java.util.Scanner;

public class MyMain {

	static int count = 5;
	static Scanner scan = new Scanner(System.in);
	static int[] array = new int[5];	//array length is a variable
	static int[] copyArray = new int[count];
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for (int i=0; i<array.length; i++)
		{
			System.out.println("Enter number for index: " + i);
			array[i] = scan.nextInt();
		}
		
		
		/*
		//copy array: 1st way - no connection (copying primitive integers)
		for (int i=0; i<array.length; i++)
		{
			copyArray[i] = array[i];
		}
		*/
		
		
		//Copy array: 2nd way - connection between arrays
		System.out.println("Copy array and Change value");
		copyArray = array;	//copy the arrays -> 1 array with 2 names
		array[0] = 9999;
		
		
		//Print the arrays
		for (int i=0; i<copyArray.length; i++)
		{
			System.out.println(i + " - array=" +array[i]+ " | copyArray= " + copyArray[i]);
		}

	}

}
