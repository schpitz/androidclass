package hackeru.io.read;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MyClass {

	public static void main(String[] args) {
		File desktop = new File("C:/Users/hackeru.HACKERU3/Desktop");
		File readFile = new File(desktop, "write.txt");
		
		try {
			
			InputStream inputStream = new FileInputStream(readFile);
			byte[] buffer = new byte[4];
			
			int counter = 0;
			StringBuilder str = new StringBuilder();
			
			while((counter = inputStream.read(buffer)) != -1){
				str.append(new String(buffer, 0 , counter));
			}
			inputStream.close();
			System.out.println(str.toString());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
