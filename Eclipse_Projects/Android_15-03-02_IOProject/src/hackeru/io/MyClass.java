package hackeru.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MyClass {
	
	public static void main(String[] args) {
		File desktop = new File("C:/Users/hackeru.HACKERU3/Desktop");
		File writeFile = new File(desktop, "write.txt");
		try {
//			OutputStream outputStream = new FileOutputStream(writeFile);//default append is false
			OutputStream outputStream = new FileOutputStream(writeFile, true);
			String str = "first write\r\nnew line\r\n";
			byte[] bytes = str.getBytes();
			outputStream.write(bytes);
			outputStream.write(bytes);
			outputStream.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
