package com.example.schpitz.app_15_04_02_sqliteexample;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            rootView.findViewById(R.id.write).setOnClickListener(this);
            rootView.findViewById(R.id.read).setOnClickListener(this);
            return rootView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.write:
                    createAndWrite();
                    break;
                case R.id.read:
                    readData();
                    break;
            }
        }

        private void readData() {

            SQLiteDatabase database = getActivity().openOrCreateDatabase("db.sqlite", MODE_PRIVATE, null);
            Cursor cursor = database.rawQuery("SELECT name, age FROM table_name WHERE age > 29", null);

            Log.d("db", "cursor column count is: "+cursor.getColumnCount());
            Log.d("db", "cursor rows count is: "+cursor.getCount());

            if(cursor.moveToFirst()){

                int nameIndex = cursor.getColumnIndex("name");
                do{

                    Log.d("db", "name: "
                            +cursor.getString(nameIndex)+
                            ", age: "+
                            cursor.getInt(cursor.getColumnIndex("age")));

                }while (cursor.moveToNext());

            }
            cursor.close();
            database.close();


        }

        private void createAndWrite() {
            SQLiteDatabase database;
            database = getActivity().openOrCreateDatabase("db.sqlite", MODE_PRIVATE, null);
            database.execSQL("CREATE TABLE IF NOT EXISTS table_name (name VARCHAR NOT NULL, last VARCHAR, age INTEGER)");
            database.execSQL("INSERT INTO table_name VALUES ('Hackeru','havkeru' ,80)");
            database.execSQL("INSERT INTO table_name (name, age) VALUES('Izik', 29)");
            database.close();
        }


    }
}
