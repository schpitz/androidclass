package com.example.hackeru.app15_01_29_lifecycle;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class ActivityA extends ActionBarActivity {

    String activityName = "Activity A";     // 1 String
    TextView methodView , stateView;        // 2 TextViews
    static StatusTracker statusTracker = new StatusTracker();      //object type of StatusTracker()



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);


        // init text views
        methodView = (TextView) findViewById(R.id.methodList);
        stateView = (TextView) findViewById(R.id.stateActivity);

        statusTracker.setStates(activityName , "onCreate()");
        statusTracker.printStatus(methodView , stateView);
    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    //We re-write the following methods to print their names
    @Override
    protected void onStart() {
        super.onStart();        //a must have!
        //We add the following:
        statusTracker.setStates(activityName , "onCreate()");
        statusTracker.printStatus(methodView , stateView);
    }

    @Override
    protected void onResume() {
        super.onResume();           //a must have!
        //We add the following:
        statusTracker.setStates(activityName , "onResume()");
        statusTracker.printStatus(methodView , stateView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //We add the following:
        statusTracker.setStates(activityName , "onPause()");
        statusTracker.printStatus(methodView , stateView);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //We add the following:
        statusTracker.setStates(activityName , "onStop()");
        statusTracker.printStatus(methodView , stateView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //We add the following:
        statusTracker.setStates(activityName , "onDestroy()");
        statusTracker.printStatus(methodView , stateView);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //We add the following:
        statusTracker.setStates(activityName , "onRestart()");
        statusTracker.printStatus(methodView , stateView);
    }




    public void startA(View view) {
    }

    public void startB(View view) {
        startActivity(new Intent(getApplicationContext() , ActivityB.class));
    }

    public void StartC(View view) {
        startActivity(new Intent(getApplicationContext() , ActivityC.class));
    }

    public void StartD(View view) {
    }

    public void refreshState(View view)
    {
        statusTracker.printStatus(methodView , stateView);
    }


}
