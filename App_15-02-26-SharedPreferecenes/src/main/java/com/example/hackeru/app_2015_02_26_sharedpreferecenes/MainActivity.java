package com.example.hackeru.app_2015_02_26_sharedpreferecenes;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //
    public void click(View view) {
        // open or create file "first.xml"
        SharedPreferences pref = getSharedPreferences("first", MODE_PRIVATE);
        // If the file exits -> we could extract data from it
        // If the file doesn't exist -> it will be created

        // Enable to edit
        SharedPreferences.Editor editor = pref.edit();  // import the Editor class from "prefs"
        editor.putString("strKey", "string value example");    //set a string and a key
        editor.putFloat("floatKey", 1.5f);
        editor.putBoolean("boolKey", true);
        editor.putInt("intKey", 15300);

        //save the key values
        editor.commit();
    }


    public void StartSecond(View view)
    {
        startActivity(new Intent(getApplicationContext() , SecondActivity.class));
    }
}
