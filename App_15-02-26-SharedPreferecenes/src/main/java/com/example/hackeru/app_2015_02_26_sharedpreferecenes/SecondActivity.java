package com.example.hackeru.app_2015_02_26_sharedpreferecenes;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Instead of XML, it's on the root view
        TextView tv = new TextView(this);
        setContentView(tv);
        tv.setTextColor(Color.RED);

        // We get the data from "first" XML file ("first.xml")
        SharedPreferences prefs = getSharedPreferences("first" , MODE_PRIVATE);

        // Read from XML
        //get the key, and also a default value in case it doesn't exists (instead of null)
        String str = prefs.getString("srtKey" , "");
        int num = prefs.getInt("intKey" , -1000);
        float f = prefs.getFloat("floatKey" , -95.5f);
        boolean b = prefs.getBoolean("bKey" , false);

        // Let's present it in the textview
        tv.setText(str + "\n" + "i" + "\n" + f + "\n" + b);



        setActivityPrefs();

    }


    // New 2 objects-variables for dealing with the shared-preferences
    // We must put it in a function because they will be created before the app start to run (ERROR)
    SharedPreferences activityPrefs;
    SharedPreferences.Editor editorPrefs;

    //We create new function
    private void setActivityPrefs()
    {
        activityPrefs = getPreferences(MODE_PRIVATE);
        editorPrefs = activityPrefs.edit();
        editorPrefs.putString("strKey" , "string test 1");

        editorPrefs.commit();
    }
}
