package com.example.hackeru.app_15_03_09_streams;

import android.app.Activity;
import android.os.Environment;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scanFile();

        try {
            FileOutputStream out = openFileOutput("output.file", MODE_APPEND);
            out.write("text".getBytes());
            out.close();

            File parent = new File(getFilesDir(), "Folder");
            File file = new File(parent, "output.file");
            FileOutputStream fos = new FileOutputStream(file, true);
            fos.write("inner folder".getBytes());
            fos.close();

            FileInputStream fis = openFileInput("output.file");

            int counter = 0;
            StringBuilder builder = new StringBuilder();
            byte[] buffer = new byte[1024];


            while((counter = fis.read(buffer)) != -1){
                builder.append(new String(buffer, 0 , counter));
            }
         //   Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();
            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void scanFile() {
        File file = new File(getFilesDir(), "writer.txt");
        try {
            Scanner scanner = new Scanner(file);
            scanner.nextLine();
            Toast.makeText(getApplicationContext(), scanner.nextLine(), Toast.LENGTH_LONG).show();
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public File getInternalRoot(){
        //data/data/<package name>/files
        return  getFilesDir();
    }

    public File getExternalRoot(){
        //mnt/sdcard
        return Environment.getExternalStorageDirectory();
    }

    public  File getExternalDataRoot(){
        //sdcard/android/data/<package name>/files/Picture
//        getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        //sdcard/android/data/<package name>/files

        return getExternalFilesDir(null);
    }


}
