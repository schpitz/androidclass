package com.example.app_2015_01_05;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {

	//Array of strings
	String[] array = {"a" , "b" , "c"};
	ArrayAdapter<String> adapter; 	//makes View object for every cell in the array
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		
		//Filters at "LogCat" (doesn't work?!?)
		System.out.println("List View");
		Log.e("main activity" , "first log message");	//tag, message
		
		
		//Create a spinner object from XML
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		
		//Create adapter (takes a cell from array and export it to view (simple_expan
		adapter = new ArrayAdapter<>(this,	//context
									android.R.layout.simple_list_item_1,	//Row layout
									array);
		
		//Execute adapter into spinner
		spinner.setAdapter(adapter);
	
	}
	
	
}
