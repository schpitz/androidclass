package com.example.app_2015_01_05;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FourthActivity extends Activity implements OnItemClickListener {

	static ArrayList<String> list = new ArrayList<>();		//will be static even if we finish() this class (destroyed)
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fourth);
		
		
		ListView listView = (ListView) findViewById(R.id.listView1);	//connect to XML listView
		list.add(ListenerActivity.lastString);	//remember the last string of this activity
		
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
														android.R.layout.simple_list_item_1,
														list);
		listView.setAdapter(adapter);	//Set adapter into ListView
		listView.setOnItemClickListener(this);	//add listener to ListView object
	}


	//Implements an interface function (onItemClick)
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ListView lv = (ListView) parent; //(make sure the parent is ListView object)
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) parent.getAdapter(); 	//use casting because we are searching for toSting() (and its string)
		
		//System notification
		Toast.makeText(this, "" + position +" : "+ adapter.getItem(position) , Toast.LENGTH_SHORT).show();
	}
	
	
	
	
}
