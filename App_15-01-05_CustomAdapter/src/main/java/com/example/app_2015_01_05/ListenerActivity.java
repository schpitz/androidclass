package com.example.app_2015_01_05;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ListenerActivity extends Activity implements OnClickListener {

	public static String lastString;
	EditText name;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listener);
		
		//Get the button object from XML
		Button button = (Button) findViewById(R.id.button1);
		
		//Listen to OnClick() activity inside the button object
		button.setOnClickListener(this);
		
		//Get editText object
		name = (EditText) findViewById(R.id.editText1);
	}
	
	
	//Count "back" or "ESC" actions
	int counter=0;
	@Override
	public void onBackPressed() {
		Log.d("back pressed", "count = " + counter++);	//logs still doesn't work
		if (counter==5)
			super.onBackPressed();	//actual "back" action			
	}
	
	
	//Added because we implements abstract/interface class onClickLisener
	@Override
	public void onClick(View v) {
		//YAAAAA!!!
		lastString = name.getText().toString();	//saving the editText value
		startActivity(new Intent(this, FourthActivity.class)); 	//Forward to new activity+xml
	}
	
}
