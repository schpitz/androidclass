package com.example.schpitz.homework_2014_12_29_calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends Activity {

    EditText txtBox;        //Textbox screen
    float NumberBf;         //Save screen before pressing button operation
    String Operation;       //Operation name

    private ButtonClickListener btnClick;      //add a new class (ButtonClickListener) which has a listener

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtBox = (EditText) findViewById(R.id.editText);        //connect to editText in XML
        txtBox.setEnabled(false);                       //Not sure...

        btnClick = new ButtonClickListener();                   //Initialize new object -> for future listeners


        // Array (of ints) of all the buttons' IDs
        int idList[] = {R.id.button0,
                R.id.button1,
                R.id.button2,
                R.id.button3,
                R.id.button4,
                R.id.button5,
                R.id.button6,
                R.id.button7,
                R.id.button8,
                R.id.button9,
                R.id.buttonDOT,
                R.id.buttonEqual,
                R.id.buttonClear,
                R.id.buttonMin,
                R.id.buttonAdd,
                R.id.buttonMult,
                R.id.buttonDiv
        };

        // Add listeners to all views (by ID)
        for (int id:idList)
        {
            View v = (View) findViewById(id);
            v.setOnClickListener(btnClick);         //any "v" will run "btnClick" class when clicking on it
        }
    }



    //Inner functions (methods) that will help us
    //Saving the operations + old numbers
    public void mMath(String str)
    {
        NumberBf = Float.parseFloat(txtBox.getText().toString());    //Save the number from textbox into variable
        Operation = str;                //Save the action/operation into string
        txtBox.setText("0");            //Clear screen -> in order to reset the textbox
    }


    //Letting the user to type a long number, add it to a string
    public void getKeyboard(String str)
    {
        String ScreenCurrent = txtBox.getText().toString();     //save current txtbox value as a string

        if(ScreenCurrent.equals("0"))                       //Special scenario
            ScreenCurrent="";                               //0 + 1 = 1 -> so don't remember 0 if it's the current number

        ScreenCurrent += str;                               //add given string (char) into current string (chars?)
        txtBox.setText(ScreenCurrent);                      //Print the new string
    }



    //Results
    // Logic: saved number (action) new number -> textbox
    public void mResult()
    {
        float NumAFTER = Float.parseFloat(txtBox.getText().toString());     //Save the textbox as a float number

        float result = 0;                                                       // New float variable

        //Now compare the operation string into the buttons (.getText())
        if (Operation.equals("+")){
            result = NumAFTER + NumberBf;
        }
        if (Operation.equals("-")){
            result = NumAFTER - NumberBf;
        }
        if (Operation.equals("*")){
            result = NumAFTER * NumberBf;
        }
        if (Operation.equals("/")){
            result = NumAFTER / NumberBf;
        }

//      txtBox.setText(result.toString());          //won't work (won't cast)
        txtBox.setText(String.valueOf(result));     // Turn the result (float) -> string type -> into textbox
    }




    //New inner class - ButtonClickListener.class
    //Will run with every click on a button, and be managed by its content
    class ButtonClickListener implements View.OnClickListener {
//        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.buttonClear:      //Clear screen
                    txtBox.setText("0");
                    NumberBf = 0;
                    Operation = "";
                    break;
                case R.id.buttonAdd:        //Add function
                    mMath("+");
                    break;
                case R.id.buttonMin:        //Minus function
                    mMath("-");
                    break;
                case R.id.buttonMult:       //Multiply function
                    mMath("*");
                    break;
                case R.id.buttonDiv:        //Divide function
                    mMath("/");
                    break;
                case R.id.buttonEqual:      //Equal button
                    mResult();
                    break;
                default:                    //All other buttons (numbers + dot)
                    String num = ((Button)v).getText().toString();  //get the number from the button content
                    getKeyboard(num);
                    break;
            }
        }
    }

}
