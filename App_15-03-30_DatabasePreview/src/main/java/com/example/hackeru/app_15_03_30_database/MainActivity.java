package com.example.hackeru.app_15_03_30_database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }


        createFirstDatabaseFile();              // 1st phase   - enable this if app doesn't run!
//        writeAllData2();                        // 2nd phase
//        writeUdi();                               // 3rd phase

    }

    private void writeUdi() {

        SQLiteDatabase database = openOrCreateDatabase("first.sqlite", MODE_PRIVATE, null);
        Cursor cursor = database.rawQuery("SELECT name FROM table_name WHERE age > 20",null);

        Log.d("db", "cursor row count is: "+cursor.getCount());
        Log.d("db", "cursor column count is: "+cursor.getColumnCount());

        if(cursor.moveToFirst()){
            Log.d("db", "move to first!");
            String[] columnNames = cursor.getColumnNames();
            do{

                StringBuilder builder = new StringBuilder();
                for (String columnName : columnNames) {
                    builder.append(" "+columnName +": "+cursor.getString(cursor.getColumnIndex(columnName)));

                }

                Log.d("db", builder.toString());
            }while (cursor.moveToNext());
            cursor.close();
            database.close();

        }

    }

    private void writeAllData2() {

        SQLiteDatabase database = openOrCreateDatabase("first.sqlite", MODE_PRIVATE, null);

        Cursor cursor = database.rawQuery("SELECT * FROM table_name", null);
        Log.d("db", "cursor row count is: "+cursor.getCount());
        Log.d("db", "cursor column count is: "+cursor.getColumnCount());

        if(cursor.moveToFirst()){
            Log.d("db", "move to first!");
            String[] columnNames = cursor.getColumnNames();
            do{

                StringBuilder builder = new StringBuilder();
                for (String columnName : columnNames) {
                    builder.append(" "+columnName +": "+cursor.getString(cursor.getColumnIndex(columnName)));

                }

                Log.d("db", builder.toString());
            }while (cursor.moveToNext());
            cursor.close();
            database.close();

        }

    }


    private void writeAllData() {

        SQLiteDatabase database = openOrCreateDatabase("first.sqlite", MODE_PRIVATE, null);

        Cursor cursor = database.rawQuery("SELECT * FROM table_name", null);
        Log.d("db", "cursor row count is: "+cursor.getCount());
        Log.d("db", "cursor column count is: "+cursor.getColumnCount());

        if(cursor.moveToFirst()){
            Log.d("db", "move to first!");

            int nameIndex = cursor.getColumnIndex("name");

            do{
                String name = cursor.getString(nameIndex);
                String lastName  = cursor.getString(cursor.getColumnIndex("last_name"));
                int age = cursor.getInt(cursor.getColumnIndex("age"));
                boolean isOld = cursor.getInt(cursor.getColumnIndex("is_old")) == 1;
                Log.d("db", "name: "+name+" last: "+lastName+" age: "+age+" isOld: "+isOld);
            }while (cursor.moveToNext());
        }

    }

    private void createFirstDatabaseFile() {
        SQLiteDatabase database = openOrCreateDatabase("first.sqlite", MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS table_name (name VARCHAR, last_name VARCHAR, age INTEGER, is_old INTEGER)");
        database.execSQL("INSERT INTO table_name VALUES ('Koby', 'Mimon', 20, 0)");
        database.execSQL("INSERT INTO table_name(name, age) VALUES('Udi', 27);");

        //Primary key
        database.execSQL("CREATE TABLE IF NOT EXISTS table_2 " +
                "(name VARCHAR, " +
                "last_name VARCHAR," +
                " age INTEGER PRIMARY KEY," +
                " is_old INTEGER)");

//        database.execSQL("INSERT INTO table_2 VALUES ('Koby', 'Mimon', 20, 0)");
//        database.execSQL("INSERT INTO table_2(name, age) VALUES('Udi', 27);");
        database.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
