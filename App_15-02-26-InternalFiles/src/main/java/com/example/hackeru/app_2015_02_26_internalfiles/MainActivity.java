package com.example.hackeru.app_2015_02_26_internalfiles;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import java.io.File;
import java.io.IOException;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // Root internal storage (get all the dir content)
        // getFilesDir() is a context method, Activity inherit
        // <root>/data/data/<package>/files/
        File internalRoot = getFilesDir();
        File folder = createDir(internalRoot , "New Folder");

        createFile(folder , "first.txt");               //      files/New Folder/first.txt
        createFile(internalRoot , "second.txt");        //      files/second.txt


        // External storage root file
        // <root>/storage/sdcard/
        File externalRootD = Environment.getExternalStorageDirectory();
        createDir(externalRootD , "My New Directory");
        createFile(externalRootD , "First external file.png");


    }


    // Create the actual file
    private File createFile(File parent, String name)
    {
        File fileX = new File(parent , name);

        if (!fileX.exists())
            try{
                fileX.createNewFile();
            } catch (IOException e){
                e.printStackTrace();
            }

        return fileX;
    }


    // New function
    private File createDir(File root , String name)
    {
        // Get a new dir or file into a variable
        File dir = new File(root , name);

        //Check if the variable exists (if it's a dir)
        if (!dir.exists())
        {
            dir.mkdir();
        }

        return  dir;
    }
}
