package com.example.hackeru.app15_01_26_rotation;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    static int counter = 0;         //counter that will be shared to all activities


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //On 1st runtime -> Bundle savedInstanceState = null
        // but when the activity recreated (like with Rotate), it has data
        // so -> if savedInstanceState==null -> first runtime (we can't get it)

        setContentView(R.layout.activity_main);

        //this int is ID for a string (from XML)
        int res = R.string.hello_world;

        //The toast can present only string (or id of string)
        if (savedInstanceState == null)
        {
            Toast.makeText(getApplicationContext() ,
                    "Initial Run" ,
                    Toast.LENGTH_LONG).show();
        } else {
            //If it's not intial run, count numbers
            Toast.makeText(getApplicationContext() ,
                    ""+counter++ ,
                    Toast.LENGTH_LONG).show();
        }




        //Rotation - (ctrl+F11) will force the activity to close
        //It will destruct the current activity, and build a new one
        // It will save the data of the activity, as long they have IDs
        //This is why the EditText with ID will save the data, while the other one will be forgotten

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("lalala" , "babababa");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
