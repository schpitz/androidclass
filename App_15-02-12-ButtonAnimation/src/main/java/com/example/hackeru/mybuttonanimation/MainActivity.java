package com.example.hackeru.mybuttonanimation;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    Handler handler = new Handler();
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        //threadTest();
        replace();
    }



    public  void replace(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                button.setVisibility(button.isShown() ? View.INVISIBLE : View.VISIBLE);
                replace();
            }
        }, 500);
    }

    public void  threadTest(){

        new Thread() {

            @Override
            public void run() {

                while (true) {
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            button.setVisibility(button.isShown() ? View.INVISIBLE : View.VISIBLE);
                        }
                    });
                }
            }
        }.start();

    }



    public void test(){

        for (int i = 0; i < 100; i++) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("","");
                }
            }, 500);
        }
    }
}
