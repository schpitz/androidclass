package com.example.schpitz.homework_2014_12_29_calculator2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView textdigits;


    Button equ, next, prev;
    boolean lastSymbolClicked = false;
    boolean firsState= true;
    boolean equalState = false;
    boolean isPrev;
    boolean isNext;
    boolean isOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textdigits = (TextView) findViewById(R.id.textView);
        equ = (Button) findViewById(R.id.equal);
        next = (Button) findViewById(R.id.next);
        prev = (Button) findViewById(R.id.prev);

    }


    double lastNumberSum;
    String currentNumber="";
    String symbol;
    int currentIndex =0;

    //called when one of number buttons clicked
    public void numberClick(View v){
        String sNumber = (String) v.getTag();
        if( addToCurrentNumber(sNumber)){
            if(lastSymbolClicked)
                currentIndex++;
            lastSymbolClicked = false;
            updateTextDigit();
            updateUI();
        }

    }

    private void updateUI() {

        equalState = false;
        isPrev = false;

        if(currentIndex > 0 ) {
            equalState = lastSymbolClicked ? false : true;
            isPrev = true;
        }

        equ.setEnabled(equalState);
        next.setEnabled(isNext);
        prev.setEnabled(isPrev);


    }

    private void updateTextDigit() {
        textdigits.setText(currentNumber);
    }

    private boolean addToCurrentNumber(String newDigit) {
        if(isOut)
            return false;
        if(newDigit.equals("0") && currentNumber.length() == 0)//first and second digit can't be 0. e.x 00001 is 1
            return false;
        if(currentNumber.length() <= 6) {
            currentNumber = currentNumber + newDigit;
            return  true;
        }
        return  false;
    }

    public void symbolClick(View v){

        if(currentNumber.isEmpty()){
            symbol = (String) v.getTag();
            return;
        }
        if(!lastSymbolClicked)
            calculate();

        symbol = (String) v.getTag();
        lastSymbolClicked = true;
        updateUI();
    }

    private void calculate() {
        if(symbol == null){
            lastNumberSum = Double.parseDouble(currentNumber);
            currentNumber = "";
            return;
        }else{
            int number = Integer.parseInt(currentNumber);
            switch (symbol){
                case "/":
                    if(number != 0)
                        lastNumberSum /= number;

                    break;
                case "+":
                    lastNumberSum += number;
                    break;
                case "-":
                    lastNumberSum -= number;
                    break;
                case "*":
                    lastNumberSum *= number;
                    break;
            }
        }
        setBounds();
        updateTextDigit();
        currentNumber = "";

    }

    public void setBounds(){
        int dot;
        if(lastNumberSum > 9999999)
            isOut = true;
        else if((dot = (""+lastNumberSum).indexOf("."))!= -1){
            int integer = (int) lastNumberSum;
            int integerCount = (""+integer).length();
            int doubleCount = (""+lastNumberSum).substring(dot+1).length();
            String dobString;
            if(6-integerCount <= doubleCount)
                dobString = (""+lastNumberSum).substring(dot ,dot+(6-integerCount)+1);
            else
                dobString =(""+lastNumberSum).substring(dot ,dot+doubleCount);
            lastNumberSum = Double.parseDouble(integer+dobString);
        }

    }

    public void  eqClicked (View v){
        if(currentNumber.isEmpty())
            return;
        lastSymbolClicked = false;
        calculate();
        symbol = null;
        currentNumber = ""+lastNumberSum;
        textdigits.setText(""+lastNumberSum);
    }


    public void reset(View v){
        isOut = false;
        isPrev = false;
        isNext = false;
        equalState = false;
        lastNumberSum = 0;
        currentNumber = "";
        currentIndex = 0;
        lastSymbolClicked = false;
        symbol = null;
        updateUI();
        textdigits.setText(currentNumber);
    }
}

