package com.example.hackeru.filesexplorerproject;

import java.io.File;
import java.util.ArrayList;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewParent;


/**
 * Created by Schpitz on 15/03/2015.
 */
public class DirectoryInfo extends FileInfo{


    // Integers which are the number of files and folders inside this current directory
    public int innerFilesNumber = 0;
    public int innerDirectoriesNumber = 0;



    // Constructors
    public DirectoryInfo(File file, DirectoryInfo parent) {
        super(file, parent , true);
    }

    public DirectoryInfo(String path) {
        super(path , true);
//        if(! getPath().equals(rootDirectory.getPath()))
//            parent = new DirectoryInfo(getParentFile());
    }

    public DirectoryInfo(File file) {
        super(file , true);
//        if(! file.getPath().equals(rootDirectory.getPath()))
//            parent = new DirectoryInfo(getParentFile());

    }


    // Return arraylist with the files in the directory
    public  ArrayList<FileInfo> getInnerFiles()
    {
        ArrayList<FileInfo> listX = new ArrayList();

        for (File fileX : listFiles())                  // Loop over the file.listFiles (directory of "file")
        {
            if (fileX.isDirectory())
                listX.add(new DirectoryInfo( fileX , this));
            else
                listX.add( new FileInfo( fileX , this));
        }
        return listX;

    }

//    public int getInnerFilesNumber(DirectoryInfo arrayX){
//        innerFilesNumber=getInnerFilesNumber(arrayX);
//
//            if (isDirectory()) {
//                return innerFilesNumber;
//            }
//
//
//        return 0;
//    }

    // here we created a function that says:
    //here is a new array list
    //we created a new Directory info called current - and we put in it the Directory info parent we
    //created un file info class
    //than we said: add the current directoy to the new arraylist(called list)
    //also: we used while and if conditions to say:
    //if there is a parent directory info
    //ad the "current" directory to the arraylist
    //than return the content of that arrayList

    public ArrayList<DirectoryInfo>parents(){
        ArrayList<DirectoryInfo>list=new ArrayList<>();
        DirectoryInfo current = parent;
        list.add(current);
        while (parent.getParentDirectory()!=null){
            list.add(current=parent.getParentDirectory());
        }
        return list;
    }




    public int getInnerFilesNumber(){

        ArrayList<FileInfo> listOfInnerFiles = getInnerFiles();
       int Number=listOfInnerFiles.size();
       int counter=0;

       for (int i =0; i<Number;i++){
           if(listOfInnerFiles.get(i).getClass() == DirectoryInfo.class)
           {
               // If the object is Directory
               counter = counter + ((DirectoryInfo)(listOfInnerFiles.get(i))).getInnerFilesNumber();

           } else {
               // If the object is FileInfo
               counter = counter + 1;
           }
       }

        return counter;
    }


}
