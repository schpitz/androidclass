package com.example.hackeru.filesexplorerproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//////////////////////
    // The answer will be found in the ex of 15.1.2015
    //////////////////


public class CustomAdapter extends ArrayAdapter<FileInfo> {


    ArrayList<FileInfo> items;                                     // Every array of items will be of type "DirectoryInfo"
    DirectoryInfo dir;
    Boolean isCheck;


    //Constructor -> gets the context + arraylist (of FileInfo)
    public CustomAdapter(Context context, ArrayList<FileInfo> objects) {
        super (context, R.layout.row , objects);
        items = objects;                                            // update the "items" arraylist
    }




    // Part of the "CustomAdapter()" original functions
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view;
        if (convertView == null)
        {
            view = LayoutInflater.from(getContext()).inflate(R.layout.row , null);
        } else {
            view = convertView;
        }


        // Set the name of the file
        TextView tv_name = (TextView) view.findViewById(R.id.textView_title);

        // Check the text length and set the size according to it
        String tempName = items.get(position).getName();                // return a string of the name of file
        tv_name.setText(tempName);                                      // set the text content into TextView
        if (tempName.length() < 16)                                     // check the string length
            tv_name.setTextSize(16);
        else
            tv_name.setTextSize(12);


        // Set the icon
        ImageView icon = (ImageView) view.findViewById(R.id.imageView);

        // Set the number of files inside the folder
        TextView tv_itemsCounter = (TextView) view.findViewById(R.id.textView_itemCounter);
        tv_itemsCounter.setTextSize(12);                                                    // tweak font size

        // Set Date - last modified
        TextView tv_date = (TextView) view.findViewById(R.id.textView_date);
        tv_date.setTextSize(12);                                                            // tweak font size

        // Get the Checkbox (possible to copy/cut/delete)
        CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox);
        isCheck = cb.isChecked();

        ///////////////////////////////////////
        /// Check Object -> Folder or File ///
        /////////////////////////////////////
        if (items.get(position).getClass() == DirectoryInfo.class)                  // Compare object to the class
        {
            DirectoryInfo newDir;
            newDir = (DirectoryInfo) items.get(position);

            int tempNumber;
            tempNumber = newDir.getInnerFilesNumber();

            tv_itemsCounter.setText(tempNumber + " items");

            if (tempNumber>0)
                icon.setImageResource(R.mipmap.folder_full);                        // show full folder icon
            else
                icon.setImageResource(R.mipmap.folder_empty);                       // show empty folder icon
        } else {
            icon.setImageResource(R.drawable.ic_launcher);                          // show file icon (general)
            tv_itemsCounter.setText(items.get(position).FileLength(items.get(position)));       // returns the string of file size
        }

        // Updating the date
        Date lastModified = new Date(items.get(position).lastModified());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedDateString = formatter.format(lastModified);
        tv_date.setText(formattedDateString);                                       // set the string into date




//        TextView tv_itemsCounter = (TextView) view.findViewById(R.id.textView_itemCounter);
//        tv_itemsCounter.setText(items.get(position).parent.innerDirectoriesNumber);


        return view;
    }
}
