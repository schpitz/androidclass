package com.example.hackeru.filesexplorerproject;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import java.io.File;
import android.graphics.Bitmap;
import android.os.Environment;
import android.renderscript.Allocation;

import java.io.File;

/**
 * Created by Schpitz on 15/03/2015.
 */
public class FileInfo extends File {


    public DirectoryInfo parent;
    public Bitmap icon;

    protected  static  DirectoryInfo rootDirectory = new DirectoryInfo(Environment.getExternalStorageDirectory(),null);




    // Constructors

    public FileInfo(File file) {
        this(file.getPath());
        parent = new DirectoryInfo(file.getParentFile());
        if(isDirectory())
            throw new IllegalArgumentException();
    }

    public FileInfo(File file, DirectoryInfo parent) {
        this(file.getPath());
        this.parent = parent;
        if(isDirectory())
            throw new IllegalArgumentException();
    }


    public FileInfo(String path) {
        super(path);
        parent = new DirectoryInfo(getParentFile());
        if(isDirectory())
            throw new IllegalArgumentException();
    }

    public DirectoryInfo getParentDirectory(){// function that returns directory info object
       //and return the parent of this fileInfo
        return parent;
    }


    // new ToString()
    @Override
    public String toString()
    {
        return getName();
    }



    // Constructors with booleans
    protected FileInfo(File file, DirectoryInfo parent, boolean dir) {
        this(file.getPath(), true);
        this.parent = parent;
    }
    protected FileInfo(String dirPath, boolean directory) {
        super(dirPath);
    }
    protected FileInfo(File file, boolean directory) {
        super(file.getPath());
    }




    // Returns the file size
    public String FileLength(File file)
    {
        String finalSize = null;
        long size = 0;
        size = file.length();                //it's a very long number...

        if (size <= 1024)
            finalSize = size + "Bytes";
        else if (size>1024 && size<1000000)
            finalSize = size/1024 + "Kb";
        else if (size > 1000000)
            finalSize = size/1000000 + "Mb";

        return finalSize;
    }
}
