package com.example.hackeru.filesexplorerproject;

import android.app.Activity;
import android.app.ListActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


public class MainActivity extends ActionBarActivity{

    // 3 private variables
//    ArrayList<FileInfo> list;
    static public DirectoryInfo currentDir;
    ArrayAdapter adapter;
    ListView lv;
    TextView tv_path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_path = (TextView) findViewById(R.id.textView_path);
        tv_path.setTextColor(Color.WHITE);
        lv = (ListView) findViewById(R.id.listView1);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position) instanceof DirectoryInfo) {

                     currentDir = (DirectoryInfo) adapter.getItem(position);
                     adapter = new CustomAdapter(MainActivity.this , currentDir.getInnerFiles());

                     lv.setAdapter(adapter);

                    tv_path.setText(currentDir.getPath());

                 }
            }
       });

        File file = Environment.getExternalStorageDirectory();
        currentDir = new DirectoryInfo(file);

        // Alternative #1
//        adapter = new ArrayAdapter<FileInfo>(
//                this,android.R.layout.simple_list_item_1,
//                currentDir.getInnerFiles()
//        );

        // Alternative #2
//        adapter = new ArrayAdapter<FileInfo>(
//                this,
//                R.layout.row,
//                R.id.textView_title,
//                currentDir.getInnerFiles()
//        );

        adapter = new CustomAdapter(this , currentDir.getInnerFiles());

        lv.setAdapter(adapter);
        tv_path.setText(currentDir.getPath());
//        setListAdapter(adapter);
    }



    //////////////////////////////////
    ///// BACK Button ///////////////
    ////////////////////////////////
    public void clickBack(View view)
    {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        if(currentDir.getParentDirectory()==null){
            return;
        }else{
            currentDir = currentDir.getParentDirectory();
            adapter = new CustomAdapter(this , currentDir.getInnerFiles());

            lv.setAdapter(adapter);
            tv_path.setText(currentDir.getPath());
         }

    }







    ////////////////////////////////////////////
    /////// ACTION BAR ////////////////////////
    //////////////////////////////////////////
    // Action-Bar -> synced with menu_main.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // get the menu_main.xml and attach to the menu
        getMenuInflater().inflate(R.menu.menu_main , menu);

        // Example for something you could do inside the function
//        if ( copyState)
//            menu.add("paste");

        return true;
    }


    // When selecting an item -> View object -> popup menu -> menu_item.xml
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case (R.id.action_settings):
                Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_SHORT).show();
//                copyState = true;
                invalidateOptionsMenu();
                break;

            case (R.id.button_sort1):
                Toast.makeText(getApplicationContext() , "Sorting files..." , Toast.LENGTH_SHORT).show();
                break;
        }

//        return true;
        return super.onOptionsItemSelected(item);
    }


}
