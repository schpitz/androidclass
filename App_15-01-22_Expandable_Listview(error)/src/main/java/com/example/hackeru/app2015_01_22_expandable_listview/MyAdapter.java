package com.example.hackeru.app2015_01_22_expandable_listview;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by hackeru on 22/01/2015.
 */

public class MyAdapter implements ExpandableListAdapter {

    String[] str = new String[20];      //Array of 20 strings
    Context context;

    //New constructor
    public MyAdapter(Context context) {
        this.context = context;
        for (int i=0 ; i<str.length ; i++)
        {
            str[i] = ("Group " + i);
        }
    }



    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    //How many
    @Override
    public int getGroupCount() {
        return 0;
    }

    //How many children in a certain group
    @Override
    public int getChildrenCount(int groupPosition) {
        return groupPosition % 2 == 0 ? 2 : 3;
        //if we have 2 children -> 2
        // else -> 3
    }

    //We can't chang input of function, but we can change its output (return)
    @Override
    public String getGroup(int groupPosition) {
        return null;
    }

// Return string
    @Override
    public String getChild(int groupPosition, int childPosition) {
        return ("Group" + groupPosition + ", child " + childPosition);
    }

    //if we want to assign ID to groups
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    //get the current group which is open
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_expandable_list_item_1 , null);
        }

        TextView tv = (TextView) convertView;
        tv.setText(getGroup(groupPosition));            //use getGroup()

        return convertView;
    }

    //Almost the same as getGroupView()
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_1 , null);
        }

        TextView tv = (TextView) convertView;
        tv.setText(getChild(groupPosition , childPosition));    //use getChild()

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }
}
