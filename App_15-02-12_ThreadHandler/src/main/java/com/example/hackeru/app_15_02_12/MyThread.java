package com.example.hackeru.app_15_02_12;

import android.os.Handler;
import android.view.View;
import android.widget.Button;


/**
 * Created by hackeru on 12/02/2015.
 */
public class MyThread extends  Thread {
    Handler handler;
    Button button;

    public MyThread(Handler handler, Button b) {
        this.handler = handler;
        button = b;
    }

    @Override
    public void run() {
        try {
            sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
               button.setVisibility(View.INVISIBLE);
            }
        });
    }
}
