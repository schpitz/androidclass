package com.example.hackeru.app_15_02_12;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    Button button;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), "Runnable run", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        handler.postDelayed(runnable, 3000);

       // button.setVisibility(View.INVISIBLE);

    }


    public void click(View view) {
//        try {
//            Thread.sleep(3000);//ui thread run
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        Toast.makeText(getApplicationContext(), "button clicked", Toast.LENGTH_SHORT).show();


        /////////////////////////////
        //     second//////////////
        ////////////////////////
//        Thread newThread = new Thread(){
//            @Override
//            public void run() {
//                try {
//                    sleep(7000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                //Toast.makeText(getApplicationContext(), "button clicked", Toast.LENGTH_SHORT).show();
//                //button.setVisibility(View.INVISIBLE);
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        button.setVisibility(View.INVISIBLE);
//                    }
//                });
//            }
//        };
//        newThread.start();
        new MyThread(handler, button).start();
    }
}
