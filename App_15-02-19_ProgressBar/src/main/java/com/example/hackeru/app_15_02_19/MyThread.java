package com.example.hackeru.app_15_02_19;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


/**
 * Created by hackeru on 19/02/2015.
 */
public class MyThread extends  Thread {

    Handler handler;

    public MyThread(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        Bundle data = new Bundle();
        for (int i = 0; i <= 100; i++) {
            try {
                sleep(35);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            data.putInt("progress", i);
            Message message = new Message();
            message.setData(data);
            handler.sendMessage(message);//SECOND THREAD
        }

    }
}
