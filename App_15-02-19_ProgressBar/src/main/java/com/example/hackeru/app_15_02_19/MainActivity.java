package com.example.hackeru.app_15_02_19;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;


public class MainActivity extends ActionBarActivity {

    ProgressBar progressBar;//null
    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {//UI THREAD
            Bundle data;
            if(msg != null && (data = msg.getData()) != null ){
                if(data.getInt("progress", -1) != -1){
                    progressBar.setProgress(data.getInt("progress"));
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //updateProgress(progressBar);

        updateProgress2(progressBar);
    }

    private void updateProgress2(ProgressBar progressBar) {

        new MyThread(handler).start();

    }






    private void updateProgress(final ProgressBar progressBar) {
        new Thread(){
            @Override
            public void run() {
                for (int i = 1; i <= 5; i++) {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    //progressBar.setProgress(i*20);
                    final int c = i;
                    handler.post(new Runnable() {//UI THREAD
                        @Override
                        public void run() {
                            progressBar.setProgress(c*20);
                        }
                    });
                }
            }
        }.start();
    }

}
