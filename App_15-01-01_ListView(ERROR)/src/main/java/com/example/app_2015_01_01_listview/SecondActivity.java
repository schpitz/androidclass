package com.example.app_2015_01_01_listview;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		//Get an object array (of TextViews? of Person?)
		Person[] persons = getArrayPerson(); //call a function that returns array length of 15-30 cells
		
		//Create ADAPTER
		ArrayAdapter<Person> adapter = new ArrayAdapter<>(this ,		//context
														android.R.layout.simple_list_item_1,	//View row, use layout from the android device
														persons);		//array (content)
		//Link between ListView to the Adapter
		ListView myListView = (ListView) findViewById(R.id.listView1);
		myListView.setAdapter(adapter);
	}
	
	
	Random rand = new Random();		//load random class
	
	//New function that returns an array of person
	public Person[] getArrayPerson(){
		
		Person[] array = new Person[rand.nextInt(15)+15];	//new array between 15-30
		for (int i = 0; i < array.length; i++) {
			Person newPerson = new Person();		//new empty Person object
			newPerson.name = "Person " + (i+1);		//set name to "Person 1"/"Person 2"/etc...
			newPerson.age = rand.nextInt(120)+1;	//set age to 1-120;
			array[i] = newPerson;					//save newPerson into array[i]
		}
		return array;		
	}
}
