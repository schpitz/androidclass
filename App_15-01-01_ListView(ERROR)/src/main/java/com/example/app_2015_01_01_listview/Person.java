package com.example.app_2015_01_01_listview;

public class Person {
	
	String name;
	int age;
	
	
	//Default Constructor
	public Person() {
	}

	//Constructor with the parameters
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	
	@Override
	public String toString() {
		return (name + ", age: " + age); //example: Lior, age:29
	}
	
	
	

}
