package com.example.app_2015_01_01_listview;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MyListActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_list);
	
		//create new array
		String[] country = getResources().getStringArray(R.array.countryArray);
		
		//Create Adapter
		ArrayAdapter<String> adapterX = new ArrayAdapter<>(getApplicationContext(),	//context
															R.layout.draw,		//row layout
															R.id.textView1,		//specific id of textView
															country);			//content
		
		ListView lv = (ListView) findViewById(R.id.listView1);
		lv.setAdapter(adapterX);
					
	}
}
