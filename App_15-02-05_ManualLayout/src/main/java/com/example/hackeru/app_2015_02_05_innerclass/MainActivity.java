package com.example.hackeru.app_2015_02_05_innerclass;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Let's create our own Linear layout!
        LinearLayout root = new LinearLayout(getApplicationContext());
        root.setOrientation(LinearLayout.VERTICAL);             //set to Vertical
        root.setBackgroundColor(Color.RED);                     //set color to red

        //Let's create object in the layout
        TextView myTextView = new TextView(getApplicationContext());    //every object need context
        Button button = new Button(getApplicationContext());

        myTextView.setText("Hello Android");
        button.setText("Button from CODE");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1f;
        myTextView.setLayoutParams(params);

        button.setLayoutParams(new LinearLayout.LayoutParams( -2 , -2));
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) button.getLayoutParams();   //don't forget to cast
        params2.weight = 1f;
        button.setLayoutParams(params2);


        //Add the objects into the linear layout
        root.addView(button);
        root.addView(myTextView);

        setDialog(button);

        //We need to load the XML alternative
        setContentView(root);
    }


    public void setDialog(Button button)
    {
        //Dialog class -> dialog object
            ///  AlertDialog dialog = new AlertDialog.Builder(this).create();
        // Alert dialog is not an activity, it's a window that can have layout (this is why we have "this" instead of getApplication...
        // AlertDialog.Builder -> access to private variable we can set (but we couldn't see from default AlertDialog class)
        // Builder is static
        // .create() -> new dialog object

        final AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);  //it's still an inner class
        builder.setIcon(R.drawable.ic_launcher).setTitle(" :-) ");
        builder.setPositiveButton("OK" , null).setNegativeButton("cancel" , null);

        dialog = builder.create();          //not it's actually created

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }

}
