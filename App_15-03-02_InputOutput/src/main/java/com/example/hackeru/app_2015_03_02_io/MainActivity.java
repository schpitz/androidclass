package com.example.hackeru.app_2015_03_02_io;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

///////////////////////////////////////////
/// Don't forget to commit a "write.txt" file to the application folder int the Android Device Monitor
///////////////////////////////////////////


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // write something...

        readFile();
        copyFile();

    }



    private void readFile()
    {
        FileInputStream inputStream = null;

        try {
            inputStream = openFileInput("write.txt");           // force having exceptions
            byte[] buffer = new byte[10];
            int counter = 0;

            StringBuilder builder = new StringBuilder();

            while ((counter = inputStream.read(buffer)) != -1){
                builder.append(new String(buffer,0,counter));
            }
            if (builder.length()>0) {
                Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();
            }

            inputStream.close();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {                                         // Check also if the inputStram is NULL
            if (inputStream != null) {                      // close it if the stream is not closed (then close it)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    // Copy the file into a new one
    // Move the bytes, don't present them (so we don't neet to use strings or string builder)
    private void copyFile(){
        FileInputStream inputStream;
        FileOutputStream outputStream;

        try{
            inputStream = openFileInput("write.txt");
            outputStream = openFileOutput("copyWrite.txt" , MODE_PRIVATE);


            byte[] buffer = new byte[1024];
            int counter;


            while ((counter = inputStream.read(buffer)) != -1){
               outputStream.write(buffer , 0 , counter);
            }

            inputStream.close();
            outputStream.close();

            Toast.makeText(getApplicationContext() , "SUCCESS" , Toast.LENGTH_LONG).show();


        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
