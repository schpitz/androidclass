package com.example.hackeru.app_2015_03_26_titlefragment;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by hackeru on 26/03/2015.
 */
public class TitleFragment extends ListFragment {


    // Create an interface
    // (whoever which will execute the interface must use the function)
    public interface OnTitleSelectedListener {
        void onTitleSelect (int position);
    }



    // Variables
    OnTitleSelectedListener listener;       // =null


    // Getters & Setters
    // It will automatically create a set function for 'listener'
    // + execute the interface as well
    public void setListener(OnTitleSelectedListener listener) {
        this.listener = listener;               //
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnTitleSelectedListener) activity;      // we assume it executes the interface
    }



    // Load the content from TITLE object (in the XML)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setListAdapter( new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1,
                Util.TITLES));
    }



    // When we click on an item
    // any item is activity with "OnTitleSelectedListener" functions
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        if (listener != null)
            listener.onTitleSelect(position);
    }



    // Detach -> :(
    @Override
    public void onDetach() {
        super.onDetach();               // detaching from the activity
        listener = null;                // now the listener is empty
    }
}
