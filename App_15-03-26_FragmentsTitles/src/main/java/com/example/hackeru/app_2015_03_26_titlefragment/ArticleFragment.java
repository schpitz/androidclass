package com.example.hackeru.app_2015_03_26_titlefragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by hackeru on 26/03/2015.
 */
public class ArticleFragment extends Fragment {

    private int position = -1;


    // Setters & Getters
    public void setPosition(int position) {
        this.position = position;
    }



// OnCreated
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null)
        {
            position = savedInstanceState.getInt("position");
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
//        return (inflater.inflate(R.layout.article_layout , null));
                return  inflater.inflate(R.layout.article_layout, null);

    }




    @Override
    public void onResume() {
        super.onResume();
        if (position != -1) {
            setTextPosition();
        }
    }



    // New original function
    private void setTextPosition() {

        if (getView() != null){
            TextView tv = (TextView) getView().findViewById(R.id.textArticle);
            tv.setText(Util.ARTICLES[position]);
        }
    }


    // Showing articles
    public void showArticle(int position)
    {
        setPosition(position);
        setTextPosition();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("position", position);
    }

}
