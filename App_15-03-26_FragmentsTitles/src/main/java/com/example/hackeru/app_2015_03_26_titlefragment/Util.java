package com.example.hackeru.app_2015_03_26_titlefragment;

/**
 * Created by hackeru on 26/03/2015.
 */
public class Util {

    // Titles of articles
    final static String[] TITLES = {"Title1" , "Title2"};
    // Content of articles
    final static String[] ARTICLES = {"Article 1 Article 1\n Article 1\n" +
            "Article 1 Article 1\n Article 1\n" ,
            "Article 2 Article 2\n Article 2\nArticle 2 Article 2\n" +
                    " Article 2\nArticle 2 Article 2\n" +
                    " Article 2\n"};



}
