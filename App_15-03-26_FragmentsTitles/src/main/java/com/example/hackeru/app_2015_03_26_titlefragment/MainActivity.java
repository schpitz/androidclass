package com.example.hackeru.app_2015_03_26_titlefragment;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;


public class MainActivity extends Activity implements TitleFragment.OnTitleSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // How to check what device we're using (large display or normal)
//        findViewById(R.id.container);
        getFragmentManager().findFragmentById(R.id.articleFragment);


        // Make sure it's the 1st time, and the device is small (only happens on the 1st XML which has ID)
        if ((savedInstanceState == null) && (findViewById(R.id.container) != null))
        {
            getFragmentManager().beginTransaction().add(R.id.container , new TitleFragment()).commit();
        }


        //Check for landscape mode (orientation)
        // Check if the current configuration is landscape (return YES or NO)
        boolean isLand = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);



    }



    // Implements the INTERFACE
    // With every click we create a new transaction
    @Override
    public void onTitleSelect(int position) {

//        ArticleFragment articleFragment = new ArticleFragment();
//        articleFragment.setPosition(position);
//        getFragmentManager().beginTransaction().replace(R.id.container , articleFragment).addToBackStack("a").commit();

        ArticleFragment articleFragment;    // null
        if (findViewById(R.id.container) != null)   // -> small screen
        {
            articleFragment = new ArticleFragment();
            articleFragment.setPosition(position);
            getFragmentManager().beginTransaction().replace(R.id.container ,
                    articleFragment).addToBackStack("a").commit();
        } else {                                    // -> large screen
            articleFragment = (ArticleFragment) getFragmentManager().findFragmentById(R.id.articleFragment);
            articleFragment.showArticle(position);
        }
    }
}
