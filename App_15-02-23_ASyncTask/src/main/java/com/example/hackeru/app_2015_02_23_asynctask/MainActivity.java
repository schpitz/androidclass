package com.example.hackeru.app_2015_02_23_asynctask;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ProgressBar;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);     // link the ProgressBar from XML

        MyAsync task = new MyAsync(progressBar);                // The constructor will get the progress-bar
        task.execute("Some message" , "Another string...");
    }



}
