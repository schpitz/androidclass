package com.example.hackeru.app_2015_02_23_asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ProgressBar;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        // Dealing with the 2nd XML
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar2);


        MyAsync task = new MyAsync(progressBar)
        {
            @Override
            protected void onProgressUpdate(Integer... values) {
                progressBar.setProgress(values[0]);
            }
        };

        //Async task will make sure all tasks will run one after each other
        // Thread-Pool will make sure they will run with the free thread

        // 1st version
        // Only the main thread run the 2nd activity
//      task.execute("SecondActivity message");

        //Alternative
        // We could use the thread-pool with "executeOnExecuter()"
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR , "some message" , "stam message");

    }


}
