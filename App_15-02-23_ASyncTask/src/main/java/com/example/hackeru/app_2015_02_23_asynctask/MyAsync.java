package com.example.hackeru.app_2015_02_23_asynctask;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by hackeru on 23/02/2015.
 */
//public class MyAsync extends AsyncTask <Object,Object,Object>{
// AsyncTask<DoInBackground object, OnProgressUpdate object , OnPostExecute object>
// Can change the type of the inputs (params)

public class MyAsync extends AsyncTask <String ,Integer ,Boolean>{
    // Must implement "doInBackground()"
    // Get object list, and return an object


    // Variables
    ProgressBar progressBar;
    String message;
    Random random = new Random();


    //Constructor
    public MyAsync(ProgressBar progressBarX)
    {
        this.progressBar = progressBarX;
    }



    // Now the doInBackground() will use String array
    @Override
    protected Boolean doInBackground(String[] params)                      // Background Thread
    {
        if (params != null && params.length>0)
            message = params[0];        //get the 1st cell of the array params

        for (int i=1; i<=100; i++)
        {
            try {
                Thread.sleep(35);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(i);
        }

        return random.nextBoolean();                                // Return a boolean random (random true/false)
    }


    // The onProgressUpdate() will use Integers array
    @Override
    protected void onProgressUpdate(Integer... values)
    {
        if (values[0] == 70)                                    // Only after at the i=70 will start the Second-Activity
            progressBar.getContext().startActivity(new Intent(progressBar.getContext() , SecondActivity.class));               //will run the 2nd activity after the 1st is over
        progressBar.setProgress(values[0]);
    }


    // the onPostEecute() will use Booleans
    @Override
    protected void onPostExecute(Boolean aBoolean)                      // UI Thread
    {
       if (aBoolean)
       {
           Toast.makeText(progressBar.getContext() , message , Toast.LENGTH_SHORT).show();
       }
    }
}
