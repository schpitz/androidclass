package com.example.schpitz.homework_2015_01_12_painter;

import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class Shape
{
        Paint paint;                        //Create a Paint object (part of
        Point pointStart,pointEnd;          //Create 2 points (using a class we created)
                                            //With 2 points we will create lines

        //Every class that will use "Shape" class - must implement this abstract function!
        public abstract void DrawShape(Canvas canvas);


        // Why protected?
        public Point[] points(){

            // Filter out if any of the points is empty/null
            if (pointStart==null || pointEnd==null)
                return null;


            // In every click there will be 2 values for every point (x,y) -> and 2 points to create a basic shape (and this will be abstract)
            pointStart = new Point(pointStart.x , pointStart.y);         // Making a new start-point
            pointEnd = new Point(pointEnd.x , pointEnd.y);              // Making a new end-point
            Point[] returned = {pointStart , pointEnd};                 // The {} -> because it's an array

            // If the end point is smaller than the start point -> flip between them
            // (Example: draw from down to up, or right to left)
            if (pointStart.x > pointEnd.x)                                // fix X points
            {
                returned[0].x = pointEnd.x;
                returned[1].x = pointStart.x;
            }
            if (pointStart.y > pointEnd.y)                              // fix Y points
            {
                returned[0].y = pointEnd.y;
                returned[1].y = pointStart.y;
            }

            return returned;                                            //return the array with the 2 points
        }
}
