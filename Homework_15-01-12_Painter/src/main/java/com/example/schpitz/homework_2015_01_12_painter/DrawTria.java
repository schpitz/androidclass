package com.example.schpitz.homework_2015_01_12_painter;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Schpitz on 14/02/2015.
 */
public class DrawTria extends Shape
{

    // Constructor
    public DrawTria(Point start , Point end , Paint paintTemp)
    {
        this.paint = paintTemp;
        this.pointStart = start;
        this.pointEnd = end;
    }


    // Implement of Shape abstract function
    @Override
    public void DrawShape(Canvas canvas)
    {
        float x1 = pointEnd.x;
        float y1 = pointEnd.y;
        float x2 = pointStart.x;
        float y2 = pointStart.y;
        float z = (int) Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));         //not sure why...

        //Every triangle has 3 different line
        // each line has (start.x , start.y , end.x , end.y , context)
        canvas.drawLine(x2+z/2 , y2 , x2-z/2 , y2 , this.paint);
        canvas.drawLine(x1 , y1 , x2+z/2 , y2 , this.paint);
        canvas.drawLine(x1 , y1 , x2-z/2 , y2 , this.paint);

    }
}
