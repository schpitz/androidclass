package com.example.schpitz.homework_2015_01_12_painter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



///////////////////////////////////////////////////
/// Not sure why the rectangle doesn't work :( ///
/////////////////////////////////////////////////

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    MyPaint block;                              // get the object of MyPaint
    ImageView erase;                            // get the eraser object from ImageView (of the XML)

    String[] colors = { "Red" , "Blue" , "Green" , "Yellow" , "Black"};         //new strings array for the spinners
    String[] shapes = {"Line" , "Rectangle" , "Circle" , "Triangle"};

    ArrayAdapter<String> spin1;                             // new array-adapter for spinner
    ArrayAdapter<String> spin2;                             // new array-adapter for spinner

    Spinner spinner1;                                       // create 2 spinner objects
    Spinner spinner2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        block = (MyPaint) findViewById(R.id.myPaint1);          //get the objects from the XML
        spinner1 = (Spinner) findViewById(R.id.spinner1);       // get the spinners from XML
        spinner2 = (Spinner) findViewById(R.id.spinner2);

        // Create array-adapters from the spin1 + spin2
        spin1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                colors);
        spin2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                shapes);

        spinner1.setAdapter(spin1);                             // apply the adapter on the spinner
        spinner2.setAdapter(spin2);

        spinner1.setOnItemSelectedListener(this);               //add event listener (and get its position as well)
        spinner2.setOnItemSelectedListener(this);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        TextView tv = (TextView) view;

        // Apply the "Shapes" spinner
        if (tv.getText() == "Line")
            MyPaint.setSHAPE(0);
        if (tv.getText() == "Circle")
            MyPaint.setSHAPE(1);
        if (tv.getText() == "Rectangle")
            MyPaint.setSHAPE(2);
        if (tv.getText() == "Triangle")
            MyPaint.setSHAPE(3);

        //Apply the "Colors" spinner
        if (tv.getText() == "Red")
            MyPaint.setCOLOR(Color.RED);
        if (tv.getText() == "Green")
            MyPaint.setCOLOR(Color.GREEN);
        if (tv.getText() == "Blue")
            MyPaint.setCOLOR(Color.BLUE);
        if (tv.getText() == "Yellow")
            MyPaint.setCOLOR(Color.YELLOW);
        if (tv.getText() == "Black")
            MyPaint.setCOLOR(Color.BLACK);
    }





    // Function that handles the buttons from XML
    public void click(View view)
    {
        if (view.getId() == R.id.imageView1)                        // Clear everything
        {
            block.shapesList.clear();
            block.invalidate();
        }

        if (view.getId() == R.id.imageView2)                        // Clear last shape
        {
            if (block.shapesList.size()>0)                          //make sure there is a shape in the block/shape list
            {
                int lastShape = block.shapesList.size()-1;          // get the last shape index in the arraylist
                block.shapesList.remove(lastShape);                 // remove the last shape from array
                block.invalidate();
            } else {
                Toast.makeText(this,                                // else -> show a toast
                        "No shapes left on the screen" ,
                        Toast.LENGTH_LONG).show();
            };
        }
    }





    //All the others that were existed before... nothing interesting...
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
