package com.example.schpitz.homework_2015_01_12_painter;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Schpitz on 14/02/2015.
 */

public class DrawRectangle extends Shape{

    // Constructor (gets start and end points + paint canvas
    public DrawRectangle(Point start , Point end, Paint paintTemp)
    {
        this.paint = paintTemp;
        pointStart = start;
        pointEnd = end;
    }



    // Must have -> implementation of Shape class
    @Override
    public void DrawShape(Canvas canvas)
    {
        Point[] points = points();          // "points()" functions existed in Shape class // gets the start/end points into array

        //Draw rectangle on the canvas (start.x , start y , end x, end y , context)
        canvas.drawRect(points[0].x,points[0].y,points[1].x,points[1].y,this.paint);

    }
}
