package com.example.schpitz.homework_2015_01_12_painter;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Schpitz on 14/02/2015.
 */
public class DrawCircle extends Shape {

    float Radius;           //every circle has a radius & center value
    float center;


    // Constructor
    public DrawCircle(Point start , Point end , Paint paintTemp)
    {
        this.paint = paintTemp;
        this.pointEnd = end;
        this.pointStart = start;
    }

    @Override
    public void DrawShape(Canvas canvas)
    {
        float x = Math.abs(pointStart.x - pointEnd.x);      //get distance between start-end X points
        float y = Math.abs(pointStart.y - pointEnd.y);     //get distance between start-end Y points

        if (x>y)
            Radius = x;
        else
            Radius = y;

        //To draw a circle -> (start.x , start.y , radius , context)
        canvas.drawCircle(pointStart.x , pointStart.y , Radius , this.paint);

    }
}
