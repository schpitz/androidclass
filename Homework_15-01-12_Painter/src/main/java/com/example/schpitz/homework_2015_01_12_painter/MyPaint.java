package com.example.schpitz.homework_2015_01_12_painter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by Schpitz on 14/02/2015.
 */

public class MyPaint extends View {

    private static int SHAPE;                       //static var -> shared between all classes / index for shapes
    private static int COLOR = Color.RED;           // index for colors

    Point start = new Point();                      // 2 empty points
    Point end = new Point();
    Paint paint = new Paint();                      // new "paint" object

    // Create a list of shapes + one shape apart
    ArrayList<Shape> shapesList = new ArrayList<>();
    Shape tempShapes;



    // implementation of View class
    public MyPaint(Context context) {
        super(context);
        initial();
    }
    public MyPaint(Context context, AttributeSet attrs) {
        super(context, attrs);
        initial();
    }
    public MyPaint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initial();
    }


    // Set the initial properties when drawing a new shape/line
    private void initial() {
        paint = new Paint();                    // new paint object
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        paint.setColor(COLOR);
    }


    // Getters & Setters (for the private shape and color variables)
    public static int getSHAPE() {
        return SHAPE;
    }
    public static void setSHAPE(int tempShape) {
        MyPaint.SHAPE = tempShape;
    }
    public static int getCOLOR() {
        return COLOR;
    }
    public static void setCOLOR(int tempColor) {
        MyPaint.COLOR = tempColor;
    }



    //Draw the fucking shape on the canvas :)
    @Override
    protected void onDraw(Canvas canvas) {
        for (Shape s : shapesList)
        {
            s.DrawShape(canvas);                // draw all shapes from the arraylist (general instruction)?!?!?
        }
        // now it was drawn
        if (tempShapes != null)
        {
            tempShapes.DrawShape(canvas);       // create a shape on the canvas
        }
    }


    //Adding a touch-event-listener implementation
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            initial();
            start = new Point();        // new empty point
            start.x = event.getX();     //get the x + y from the event listener
            start.y = event.getY();
            end = new Point(start.x , start.y);     // the new shape's end point = start point (just at the beginning)

            switch (SHAPE){
                case 0:
                    DrawLine line = new DrawLine(start , end , paint);
                    tempShapes = line;
                    invalidate();                       // -???-
                    break;
                case 1:
                    DrawCircle circle = new DrawCircle(start , end , paint);
                    tempShapes = circle;
                    invalidate();
                    break;
                case 2:
                    DrawRectangle rect = new DrawRectangle(start , end , paint);
                    tempShapes = rect;
                    invalidate();
                    break;
                case 3:
                    DrawTria tria = new DrawTria(start , end , paint);
                    tempShapes = tria;
                    invalidate();
                    break;
                default:
                    break;
            }
        }


        // change the end (x,y) as the mouse is moving/drag
        if (event.getAction() == MotionEvent.ACTION_MOVE)
        {
            end.x = event.getX();
            end.y = event.getY();
            invalidate();
        }

        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            shapesList.add(tempShapes);                     // add the current shape object to the shape-list
            // reset the tempShape object
            tempShapes = null;
            start = null;
            end = null;
        }

        return true;
    }
}
