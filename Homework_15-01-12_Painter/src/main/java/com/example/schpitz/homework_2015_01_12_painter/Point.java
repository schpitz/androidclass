package com.example.schpitz.homework_2015_01_12_painter;


public class Point
{
    float x,y;                          //Create 2 floats variables

    //Constructor -
    //Gets 2 inputs, and create an object that contains 2 float numbers
    public Point(float xx, float yy) {
        this.x = xx;
        this.y = yy;
    }

    // Alternative constructor (will do nothing)
    public Point() {
    }

//    // Make a new ToString() that prints the X,Y
//    // Not sure why, maybe just to make sure the toString() won't return memory index (which means nothing...)
//    @Override
//    public String toString()
//    {
//        return "Point [x=" + x + ", y=" + y + "]";
//    }

}
