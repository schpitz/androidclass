package com.example.schpitz.homework_2015_01_12_painter;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Schpitz on 14/02/2015.
 */

public class DrawLine extends Shape{

    // Get start/end points when calling a constructor
    public DrawLine(Point start , Point end, Paint paintX)
    {
        this.paint = paintX;
        this.pointEnd = end;
        this.pointStart = start;
    }


    // Implement an abstract function of "Shape" class
    @Override
    public void DrawShape(Canvas canvas) {
        canvas.drawLine(
                pointEnd.x,
                pointEnd.y,
                pointStart.x,
                pointStart.y,
                this.paint
        );
    }

    @Override
    public String toString() {
        return "DrawLine [PaintOnMouseUp=" + pointStart + ", PointOnMouseDown=" + pointEnd +"]";
    }
}
