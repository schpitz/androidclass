package com.example.hackeru.fragmentproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            Fragment fragment1 = new MyFragment();
            MyFragment fragment2 = new MyFragment();

            fragment1.setRetainInstance(true);


            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().
                    add(R.id.myContainer, fragment1).
                    add(R.id.myContainer, fragment2).commit();
        }
    }
}
