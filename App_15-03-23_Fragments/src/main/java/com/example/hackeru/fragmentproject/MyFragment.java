package com.example.hackeru.fragmentproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by hackeru on 23/03/2015.
 */
public class MyFragment extends Fragment implements View.OnClickListener {

    static  int counter = 1;
    final int count = counter++;
    final  String TAG = "MY Fragment "+count;

    public MyFragment() {
//        getActivity();  null
    }

    //always call super
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Log.d(TAG, "onAttach");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_layout, null);
        TextView tv = (TextView) v.findViewById(R.id.textView);
        tv.setText("Hello fragment "+count);
        tv.setOnClickListener(this);

        Log.d(TAG, "onCreateView");
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity().getApplicationContext(), TAG, Toast.LENGTH_SHORT).show();
    }

}
