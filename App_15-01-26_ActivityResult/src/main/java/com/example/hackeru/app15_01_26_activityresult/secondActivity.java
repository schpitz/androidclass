package com.example.hackeru.app15_01_26_activityresult;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class secondActivity extends ActionBarActivity {

    final static int CODE = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }


    public void resultClick(View v)
    {
//           setResult(RESULT_OK);
//           //Will be seen only after finishing the activity

        //Unless we put data in the intent, its result will be NULL
        //so...
        Intent iData = new Intent();
        iData.putExtra("result" , "Test result data (2nd activity");
        setResult(RESULT_CANCELED , iData);       //send this if the activity is cancelled


    }

    public void finishActivity(View v)
    {
            finish();       //Finish to the current activity
    }



}
