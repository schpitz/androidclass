package com.example.hackeru.app15_01_26_activityresult;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;


public class ActivityC extends ActionBarActivity {

    final static int CODE = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_c);

        Intent myStartIntent = getIntent();     //get the Intent object from last activity
        //Let's get the data from that intent

        //Empty string
        String fName="";
        String lName="";

        if (myStartIntent.hasExtra("firstName"))
            fName = myStartIntent.getStringExtra("firstName");
        if (myStartIntent.hasExtra("lastName"))
            lName = myStartIntent.getStringExtra("lastName");

        //Now we will present it on screen (via TextView object)
        TextView tv = (TextView) findViewById(R.id.textView1);
        tv.setText(fName + " -> " + lName);

    }



}
