package com.example.hackeru.app15_01_26_activityresult;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    static int test = 9;
    String firstName = "Izik";
    String lastName = "Algrisi";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }




    public void click(View view) {

        Intent intent = new Intent();
        intent.setClass(getApplicationContext() , secondActivity.class);
        startActivity(intent);
        //Doesn't wait for result
    }

    public void startSecond(View view) {

        Intent intent = new Intent(getApplicationContext() , secondActivity.class);
        //Returns an object (class)

        startActivityForResult(intent , secondActivity.CODE);
        // Run and wait for an answer
        //Run the secondActivity, and then request CODE

    }

    public void StartC(View view) {
        Intent intent = new Intent(getApplicationContext() , ActivityC.class);

        //we put our data (vars) into intent.data
        intent.putExtra("firstName" , firstName);   //putExtra(String,data)
        intent.putExtra("lastName" , lastName);

        startActivityForResult(intent , ActivityC.CODE);    //send also the data
        //Run and wait for return
    }


    //This function will present from which Activity we are back from
    //1st button - not showing anything
    //2nd button - will return with CODE=4 -> Toast 2nd activity
    //3rd button - will return, but won't find "CODE" var -> show Toast 3rd activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        String activityResult = (requestCode == 4 ? "Second Activity" :  "Activity C");
        Toast.makeText(getApplicationContext() ,
                "Result from " + activityResult,
                Toast.LENGTH_LONG
                ).show();

        //Will print ResultCode from Activity2 (works only after finish())
        if (resultCode == RESULT_OK)
        {
            Toast.makeText(getApplicationContext() , "-> Result OK -> " , Toast.LENGTH_SHORT).show();
        }

        //if the return data has info/data and a "result" key, then...
        if ((data != null) && data.hasExtra("result"))
        {
            Toast.makeText(getApplicationContext(),
                    data.getStringExtra("result"),
                    Toast.LENGTH_SHORT).show();
        }

    }
}
