package com.example.hackeru.myserializable;

import android.graphics.Point;
import android.graphics.PointF;

import java.io.Serializable;

/**
 * Created by hackeru on 09/03/2015.
 */
public class MyPoint implements Serializable {
    float x,y;
    public MyPoint() {
    }

    public MyPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

}
