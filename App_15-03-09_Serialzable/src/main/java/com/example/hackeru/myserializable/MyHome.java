package com.example.hackeru.myserializable;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by hackeru on 09/03/2015.
 */
public class MyHome extends View {

    ArrayList<MyPoint> points = new ArrayList<>();
    public MyHome(Context context) {
        super(context);
    }

    public MyHome(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyHome(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyHome(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    Paint paint = new Paint();

    @Override
    protected void onDraw(Canvas canvas) {
        if(points.size()>1){
            for (int i = 1; i < points.size(); i++) {
                MyPoint start = points.get(i-1);
                MyPoint end = points.get(i);
                canvas.drawLine(start.x, start.y, end.x, end.y, paint);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP){
            MyPoint point = new MyPoint(event.getX(), event.getY());
            points.add(point);
            invalidate();
        }

        return true;
    }
}
