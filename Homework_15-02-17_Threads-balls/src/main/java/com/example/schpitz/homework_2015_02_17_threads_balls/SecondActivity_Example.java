package com.example.schpitz.homework_2015_02_17_threads_balls;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;


public class SecondActivity_Example extends Activity {

    TextView ball1, ball2;
    boolean b1Right, b2Right, b1Bottom, b2Bottom;
    Handler handler = new Handler();
    View table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_activity__example);

        ball1 = (TextView) findViewById(R.id.ball1);
        ball2 = (TextView) findViewById(R.id.ball2);
        table = findViewById(R.id.table);


        //updateBall();

        //second way
        new Thread(){
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if(! moveX(ball1,ball2,3)) {
                        b1Right = !b1Right;
                    }
                    if(!moveY(ball1, ball2,3)){
                        b1Bottom = !b1Bottom;
                    }
                    if(! moveX(ball2,ball1,3)) {
                        b2Right = !b2Right;
                    }
                    if(!moveY(ball2, ball1,3)){
                        b2Bottom = !b2Bottom;
                    }

                }
            };
            @Override
            public void run() {
                while (true){
                    handler.post(runnable);
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }.start();

    }

    public void updateBall(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(! moveX(ball1,ball2,3)) {
                    b1Right = !b1Right;
                }
                if(!moveY(ball1, ball2,3)){
                    b1Bottom = !b1Bottom;
                }
                if(! moveX(ball2,ball1,3)) {
                    b2Right = !b2Right;
                }
                if(!moveY(ball2, ball1,3)){
                    b2Bottom = !b2Bottom;
                }

                updateBall();
            }
        },1);

    }



    public boolean moveX(View move, View other, int num) {

        if(move == ball1 && ! b1Right ||move == ball2 && ! b2Right)
            num *= -1;

        float height = move.getHeight();
        float endY = move.getY()+height;
        float startX = move.getX();
        float endX = startX + move.getWidth();

        //check is move through other
        float top = Math.min(other.getY(), move.getY());//x top
        float bottom = Math.max(other.getY(), move.getY());//x bottom

        if(top + height > bottom && top < bottom+height){
            boolean right = move.getX() < other.getX();
            boolean rightAfter = endX + num < other.getX();
            boolean leftAfter = startX + num > other.getX()+other.getWidth();

            if(! ((right && rightAfter)||(! right && leftAfter)))
                return false;
        }

        if(startX+num < 0 || endX + num > table.getWidth())
            return false;
        move.setX(startX + num);
        return true;

    }

    public boolean moveY(View move, View other, int num) {

        if(move == ball1 && ! b1Bottom ||move == ball2 && ! b2Bottom)
            num *= -1;
        float width = move.getWidth();
        float endX = move.getX()+width;
        float startY = move.getY();
        float height = move.getHeight();
        float endY = startY+height;


        float left = Math.min(other.getX(), move.getX());
        float right = Math.max(other.getX(), move.getX());
        if(left + width > right && left < right+width) {
            boolean top = move.getY() < other.getY();
            boolean topAfter = endY + num < other.getY();
            boolean bottomAfter = startY + num > other.getY() + other.getHeight();

            if (!((top && topAfter) || (!top && bottomAfter)))
                return false;
        }
        if(startY+num < 0 || endY + num > table.getHeight())
            return false;
        move.setY(startY + num);

        return true;
    }
}
