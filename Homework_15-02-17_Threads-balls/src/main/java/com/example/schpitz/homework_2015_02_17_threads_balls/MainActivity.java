package com.example.schpitz.homework_2015_02_17_threads_balls;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;




/*///////////////////////
In this app we have 2 ball (images or textviews)
inside of a vertical linear-layout (stretched all over the device screen).
Using threads we going to move them seperately
*////////////////////////


public class MainActivity extends Activity {

//    ImageView ball1;                                        // get the balls & table from XML
//    ImageView ball2;
    TextView ball1 , ball2;
    View table;
    boolean b1Right , b2Right , b1Bottom , b2Bottom;        //group of booleans of states
    Handler handler = new Handler();                         // Remember choosing "android.os.Handler"


    // Main function (run at start)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ball1 = (TextView) findViewById(R.id.ball3);       //link the ball from XML
        ball2 = (TextView) findViewById(R.id.ball4);
        table = findViewById(R.id.table);



        //Update the ball position using threads
        new Thread()
        {
            // Using runnable force us to ovveride some functions (as run() )
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (!moveX(ball1 , ball2 , 3))              // if ball_! doesn't hit ball_1 at X position
                        b1Right = !b1Right;
                    if (!moveY(ball1, ball2, 3))
                        b1Bottom = !b1Bottom;
                    if (!moveX(ball2 , ball1 , 3))              // if ball2 doesn't hit ball_1 at X position
                        b2Right = !b2Right;
                    if (!moveY(ball2, ball1, 3))                //if ball2 doesn't hit ball_1 at Y position
                        b2Bottom = !b2Bottom;
                }
            };

//            // Not a must have!
//            // Use this function to catch exceptions every 5 mili-seconds
//            @Override
//            public void run() {
//                while (true)
//                {
//                    try {
//                        Thread.sleep(5);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
        }.start();                      // start the new thread! :)
    }



    public void UpdateBalls()
    {
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run() {
                if (!moveX(ball1 , ball2 , 3))
                    b1Right = !b2Right;
                if (!moveY(ball1 , ball2 , 3))
                    b1Bottom = !b1Bottom;
                if (!moveX(ball2 , ball1 , 3))
                    b2Right = !b2Right;
                if (!moveY(ball2 , ball1 , 3))
                    b2Bottom = !b2Bottom;

                UpdateBalls();                      // call to itself with every 1ms
            }
        } , 1);                                     // every 1-millisecond
    }


    // Update the balls X position
    public boolean moveX (View mover , View other , int number)
    {

        // If the moving ball doesn't hit its right (b1 or b2) -> multiply the number with *(-1)
        if (((mover==ball1) && !b1Right) || ((mover==ball2) && !b2Right))
        {
            number = number*(-1);
        }

        // Get some basic info about the ball (mover)
        float height = mover.getHeight();                       // get the ball height
        float endY = mover.getY()+height;                       // get the screen max height
        float startX = mover.getX();                            // get the ball X position
        float endX = startX + mover.getWidth();                 // get the screen max width
        // more advanced stuff
        float top = Math.min( other.getY() , mover.getY());     // get Y top between 2 objects
        float bottom = Math.max( other.getY() , mover.getY());  // get the Y bottom between 2 objects


        // If the ball (mover) hit the bottom or top of the screen
        if (((top+height)>bottom) && (top<(bottom+height)))
        {
            // Return a boolean from the >< statements
            boolean right = (mover.getX() < other.getX());
            boolean rightAfter = ((endX+number) <  other.getX());
            boolean leftAfter = ((startX+number) > (other.getX()+other.getWidth()));

            if (!((right && rightAfter) || (!right && leftAfter)))
                return false;
        }

       // If the ball (mover) hit the side-walls of the screen
       if (((startX+number)<0) || ((endX+number)>table.getWidth()))
            return false;

       // If there was no hit, keep moving the ball (mover) with the same direction (number=3)
       mover.setX(startX+number);
       return true;                                            // Must return a boolean!
    }



    // Update the balls Y position
    public boolean moveY(View move , View other , int num)
    {
        if(move == ball1 && ! b1Bottom ||move == ball2 && ! b2Bottom)
            num *= -1;
        float width = move.getWidth();
        float endX = move.getX()+width;
        float startY = move.getY();
        float height = move.getHeight();
        float endY = startY+height;


        float left = Math.min(other.getX(), move.getX());
        float right = Math.max(other.getX(), move.getX());
        if(left + width > right && left < right+width) {
            boolean top = move.getY() < other.getY();
            boolean topAfter = endY + num < other.getY();
            boolean bottomAfter = startY + num > other.getY() + other.getHeight();

            if (!((top && topAfter) || (!top && bottomAfter)))
                return false;
        }
        if(startY+num < 0 || endY + num > table.getHeight())
            return false;
        move.setY(startY + num);


        return true;                                            // must return something
    }


}
