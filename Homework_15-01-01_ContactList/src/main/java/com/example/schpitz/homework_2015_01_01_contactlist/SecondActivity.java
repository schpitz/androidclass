package com.example.schpitz.homework_2015_01_01_contactlist;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class SecondActivity extends Activity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        //Import the listView from XML (and cast it to ListView from View object type)
        ListView lv = (ListView) findViewById(R.id.listView);

        //New adapter (using Person objects)
        ArrayAdapter<Person> adapter = new ArrayAdapter<Person>(
                this,                                           //apply on this activity
                android.R.layout.simple_list_item_1,            //style - taken from Android OS (.R class)
                MainActivity.contactList                        //Context - the arraylist
        );

        lv.setAdapter(adapter);                                 //Apply the adapter into listview

        //Add a listener to the listview -> to delete cells
        lv.setOnItemClickListener(this);    //Force to implement OnItemClick() function
    }


    //Implements an interface function (onItemClick())
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //use casting because we are searching for toSting() (and its string)
        ArrayAdapter<Person> adapterY = (ArrayAdapter<Person>) parent.getAdapter();
        Person personToRemove = adapterY.getItem(position);         //Get the object on this position
        adapterY.remove(personToRemove);                            //remove this object from the ArrayAdapter
        adapterY.notifyDataSetChanged();                            //Update the ArrayAdapter

        //You will never have 2 objects similar

    }
}
