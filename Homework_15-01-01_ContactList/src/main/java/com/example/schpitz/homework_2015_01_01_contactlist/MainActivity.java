package com.example.schpitz.homework_2015_01_01_contactlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends Activity implements View.OnClickListener {

    // Create a common static arraylist (based on Person objects)
    static  public ArrayList<Person> contactList = new ArrayList<>();

    Random rand = new Random();

    //Variables for XML -> Person object
    EditText firstNAME;
    EditText lastNAME;
    EditText ADDRESS;
    int ID;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the "add" button from XML into an object
        Button btn = (Button) findViewById(R.id.buttonAdd);
        // Add a listener to the button -> force to implements View.OnClickListener
        btn.setOnClickListener(this);

        firstNAME = (EditText) findViewById(R.id.editText1);
        lastNAME = (EditText) findViewById(R.id.editText2);
        ADDRESS = (EditText) findViewById(R.id.editText3);
        ID = rand.nextInt(10000)+1;     //Random number between 1-10,000
    }

    //Must implement onClick() function -> because the setOnClickListener() activity
    public void onClick(View v)
    {
        String fn = firstNAME.getText().toString();      //Get the actual strings from EditViews
        String ln = lastNAME.getText().toString();
        String ad = ADDRESS.getText().toString();

        Person personX = new Person(fn , ln , ad);       //Construct a new Person object , with the information from Editviews
        personX.id = ID;                                 //Set the ID as well, outside the constructor

        contactList.add(personX);                       // Add personX into the contact list array

        // Load the next scene/activity
        startActivity(new Intent(this , SecondActivity.class));
    }
}
