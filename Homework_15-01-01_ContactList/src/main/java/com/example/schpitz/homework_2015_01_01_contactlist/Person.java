package com.example.schpitz.homework_2015_01_01_contactlist;

public class Person {

    String firstName;
    String lastName;
    String address;
    int id;

    public Person(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    @Override
    public String toString() {
        return ("Name: " + firstName + " " + lastName + " " + address + "   ID: " + id);
    }
}
