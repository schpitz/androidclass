package com.example.hackeru.app_2015_04_20_viewanimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    ///////////////////////////
    // View Animation ////////
    /////////////////////////
    public void move(View view) {

        // create a new animation ("translate.xml in 'anim' folder)
        // Load the animation as generic animation object
        Animation translateAnim = AnimationUtils.loadAnimation(
                getApplicationContext(),
                R.anim.translate);              // load from 'translate.xml'

        translateAnim.setInterpolator(new BounceInterpolator());            // add 'ease' (interpulator) to animation
        view.startAnimation(translateAnim);                                 // apply and start animation
    }



    // Rotate animation
    public void rotate(View view) {
        Animation animation = AnimationUtils.loadAnimation(
                getApplicationContext(),
                R.anim.rotate);              // load from 'translate.xml'

        view.startAnimation(animation);

    }
}
