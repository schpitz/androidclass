package com.example.app_2014_12_22;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_new_layout);		//change into new layout XML
	}
	
	public void buttonClick(View v)
	{
		int button2visibility = View.VISIBLE;		//reset invisibility index/variable
		
		switch (v.getId()) {
		
		case R.id.button1:		//when we click on "button1"->switch "button2" visibility
			
			//We can get object from XML with FIND
			View vButton2 = findViewById(R.id.button2);
			Button bu2 = (Button) vButton2;		//convert View-Object-button into Button
			
			if (bu2.getVisibility() == View.VISIBLE)	//when button is visible
			{
				bu2.setVisibility(View.INVISIBLE);	//set it invisible
			} else {			//when button is not shown
				bu2.setVisibility(View.VISIBLE);	//make it visible
			}			
			break;
			
		case R.id.button2:
		case R.id.button4:		//when we click on "button4"->"button3" switch from GONE to visible
			View vButton3 = findViewById(R.id.button3);
			//Button bu3 = (Button) vButton3;		//we don't really need to cast/convert, as both are View extensions
			
			if (vButton3.getVisibility() == View.GONE)	
			{
				vButton3.setVisibility(View.VISIBLE);	
			} else {	
				vButton3.setVisibility(View.GONE);	
			}
			break;
			
			//something doesn't work right...
		case R.id.button3:		//when we click on "button4"->"button3" switch from GONE to visible
			View vButton2_1 = findViewById(R.id.button2);
			//button2visibility = vButton2_1.getVisibility();
			
			if (!(vButton2_1.getVisibility() == View.GONE))	//check if it's invisible, then save visibility	
			{
				button2visibility = vButton2_1.getVisibility();		//get current visibility
				vButton2_1.setVisibility(View.GONE);	
			} else {	
				vButton2_1.setVisibility(button2visibility);	//return saved visibility
			}
			break;
		default:
			break;
		}
	}
}
