package com.example.hackeru.app15_01_29_lifecycle;

import android.widget.TextView;
import java.util.ArrayList;



public class StatusTracker {

    //Set SingelTon class (alternative version to cycle-activity)

    //Private constructor
    private StatusTracker()
    {
    }
    // static & private instance - we create it only ONCE on startup!
    private static StatusTracker instance = new StatusTracker();
    public static StatusTracker getInstance()
    {
        return instance;        //we only return the instance
    }




    //new arraylist of methods
    //[Activity A.onCreate()]
    ArrayList<String> methods = new ArrayList<>();
    //the name of activity (filtered)
    //[Activity A]
    ArrayList<String> names = new ArrayList<>();
    //The state/status of the activity (filtered)
    //[onCreate()]
    ArrayList<String> states = new ArrayList<>();


    public void setStates (String activityName, String status)
    {
        if (status.endsWith("onCreate()"))
        {
            methods.add("");        //every onCreate() will append empty line
        }
        methods.add(activityName + " . " + status); //append activity-name and its status

        // we want to have only one activity of the same one
        if (names.contains(activityName))           //if the name list containt the current activity name
        {
            int index = names.indexOf(activityName);
            names.remove(index);
            states.remove(index);
        }
        //Now add the current activity and its status
        names.add(activityName);
        states.add(status);
    }



    public void printStatus(TextView methodView , TextView stateView)
    {
        StringBuilder builder = new StringBuilder();    //String builder!

        for (String method : methods) {
            builder.insert(0, method + "\n");
            // "/n" -> adds a break-line for every end of cell in the array
            //  0 -> insert the cell in the first position, so we put new string on the start of the string
            //Now we have one string that contains all the methods string
        }

        //Now if the methodView is not empty -> display it
        if (methodView != null)
        {
            methodView.setText(builder.toString()); //we print the whole string
        }


        builder = new StringBuilder();      //creating a new builder into the current one
        for (int i=0 ; i<names.size(); i++)
        {
            builder.insert(0 , (names.get(i) + "." + states.get(i) + "\n"));
        }

        if (stateView != null)                          //filter
            stateView.setText(builder.toString());      //set into the textview





    }
}
