package com.example.hackeru.paint;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by hackeru on 12/01/2015.
 */
public class PaintHome extends View {


    ArrayList<PointF> points = new ArrayList<>();   //New arraylist (dynamic array)
    //PointF -> system object having (float x,float y) positions
    Paint paint = new Paint();      //using the same paint for the whole project, and save resources

    public PaintHome(Context context) {
        super(context);
    }

    public PaintHome(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaintHome(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PaintHome(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    //With new touch event -> add new point to the array list
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            PointF point = new PointF(event.getX() , event.getY());
            points.add(point);
            invalidate();           //refreshing the screen
        }
        return true;        //means we (this script) is handling touch-events
    }

    //Drawing lines on the canvas
    @Override
    protected void onDraw(Canvas canvas)
    {
        paint.setColor(Color.RED);          //setting the paint color to red

        if (points.size()<2)
            return;             //if array length < 2 -> don't draw anything

        //else is wasted, because "return" breaks the function anyway

        for (int i=1; i<points.size(); i++)
        {
            //get the previous point, and draw a line between it and the current
            //for 10 dots, we will have 9 lines
            PointF start = points.get(i-1);
            PointF end = points.get(i);
            canvas.drawLine(start.x , start.y , end.x, end.y , paint);
        }

        paint.setStyle(Paint.Style.FILL);
    }
}
